��          �      ,      �  $   �     �  #   �  $   �  "     !   8  #   Z  "   ~  $   �  .   �  -   �  *   #  0   N  ,     -   �  '   �       7     3   ;  3   o  ;   �  F   �  7   &  9   ^  7   �  T   �  a   %  g   �  b   �  i   R	  e   �	  g   "
  b   �
                                         
                           	          The network specified already exists The specified IP is not valid The specified network doesn't exist The specified protocol doesn't exist The specified proxy already exists The specified proxy doesn't exist The specified server already exists The specified server doesn't exist You must provide the IP as parameter You must provide the network name as parameter You must provide the port number as parameter You must provide the protocol as parameter You must provide the proxy hostname as parameter You must provide the proxy name as parameter You must provide the server name as parameter You must provide the value as parameter Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Alexey Sokolov <alexey+transifex@asokolov.org>
Language-Team: Russian (http://www.transifex.com/kvirc/KVIrc/language/ru/)
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 Указанная сеть уже существует Указанный IP не действителен Заданная сеть не существует Заданный протокол не существует Указанный адрес прокси уже существует Заданный прокси не существует Заданный сервер уже существует Заданный сервер не существует Вы должны предоставить IP в качестве параметра Вы должны предоставить имя сети в качестве параметра Вы должны предоставить номер порта в качестве параметра Вы должны предоставить протокол в качестве параметра Вы должны предоставить адрес прокси в качестве параметра Вы должны предоставить имя прокси в качестве параметра Вы должны предоставить имя сервера в качестве параметра Вы должны предоставить значение в качестве параметра 