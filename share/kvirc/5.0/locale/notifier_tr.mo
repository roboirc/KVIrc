��          �            h     i     p  
   y  
   �  	   �     �     �  &   �     �  *   �  .     #   <     `      y  f  �            	     	        %     .     <  &   B     i  :   �  1   �     �  !     $   -                     
                                                       	    1 Hour 1 Minute 15 Minutes 30 Minutes 5 Minutes Disable Hide Permanently (Until Explicitly Enabled) Show/Hide input line The -t switch expects a timeout in seconds The specified timeout is not valid, assuming 0 The specified window does not exist Until KVIrc is Restarted Write text or commands to window Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: ctrlaltca <ctrlaltca@gmail.com>
Language-Team: Turkish (http://www.transifex.com/kvirc/KVIrc/language/tr/)
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 1 Saat 1 Dakika 15 Dakika 30 Dakika 5 Dakika Devre dışı Gizle Kalıcı (Açıkça Etkin Olana Kadar) Göster / Gizle giriş hattı -t anahtarı bir kaç saniyede bir zaman aşımı bekliyor Belirtilen zaman geçerli değil, 0 varsayıyorum Belirtilen pencere yok KVIrc Yeniden başlatılana kadar Pencereye metin veya komut yazınız 