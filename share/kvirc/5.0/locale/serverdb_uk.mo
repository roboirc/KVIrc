��          �      �       0  $   1     V  "   t  #   �  $   �  .   �  -     *   =  0   h  ,   �  -   �  '   �  �    -   �  *   �  /   )  /   Y  ;   �  P   �  N     I   e  X   �  M     O   V  I   �                                    
         	             The network specified already exists The specified IP is not valid The specified proxy already exists The specified server already exists You must provide the IP as parameter You must provide the network name as parameter You must provide the port number as parameter You must provide the protocol as parameter You must provide the proxy hostname as parameter You must provide the proxy name as parameter You must provide the server name as parameter You must provide the value as parameter Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: ctrlaltca <ctrlaltca@gmail.com>
Language-Team: Ukrainian (http://www.transifex.com/kvirc/KVIrc/language/uk/)
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Вказана мережа вже існує Вказаний IP некоректний Вказаний проксі вже існує Вказаний сервер вже існує Ви повинні вказати IP як параметр Ви повинні вказати назву мережі як параметр Ви повинні вказати номер порту як параметр Ви повинні вказати протокол як параметр Ви повинні вказати ім'я хосту проксі як параметр Ви повинні вказати ім'я проксі як параметр Ви повинні вказати ім'я сервера як параметр Ви повинні вказати значення як параметр 