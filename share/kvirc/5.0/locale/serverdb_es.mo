��          �      �       0  $   1     V  "   t  #   �  $   �  .   �  -     *   =  0   h  ,   �  -   �  '   �  g       �      �     �  "   �  "     0   )  1   Z  &   �  .   �  0   �  3     %   G                                    
         	             The network specified already exists The specified IP is not valid The specified proxy already exists The specified server already exists You must provide the IP as parameter You must provide the network name as parameter You must provide the port number as parameter You must provide the protocol as parameter You must provide the proxy hostname as parameter You must provide the proxy name as parameter You must provide the server name as parameter You must provide the value as parameter Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: ctrlaltca <ctrlaltca@gmail.com>
Language-Team: Spanish (http://www.transifex.com/kvirc/KVIrc/language/es/)
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 La red especificada ya existe La IP especificada no es válida El proxy especificado ya existe El servidor especificado ya existe Debes dar la IP como un parámetro Debes dar el nombre de la red como un parámetro Debes dar el número de puerto como un parámetro Debes dar el puerto como un parámetro Debes dar el host del proxy como un parámetro Debes dar el nombre del proxy como un parámetro Debes dar el nombre del servidor como un parámetro Debes dar el valor como un parámetro 