��          T      �       �   2   �      �   C     \   H  H   �  >   �     -  h   .  )   �  q   �  r   3  C   �  s   �                                        Internal error: Python interpreter not initialized Python execution error: The pythoncore module can't be loaded: Python support not available The pythoncore module failed to execute the code: something is wrong with the Python support This KVIrc executable has been compiled without Python scripting support To see more details about loading failure try /pythoncore.load Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Alexey Sokolov <alexey+transifex@asokolov.org>
Language-Team: Russian (http://www.transifex.com/kvirc/KVIrc/language/ru/)
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 Внутренняя ошибка: интерпретатор python не инициализирован Ошибка выполнения python: Не возможно загрузить модуль pythoncore: поддержка python не доступна Модуль pythoncore не может выполнить код: проблема с поддержкой python Эта сборка KVIrc не имеет поддержки python Более подробная информация об ошибке загрузки модуля /pythoncore.load 