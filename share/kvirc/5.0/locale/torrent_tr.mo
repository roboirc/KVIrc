��    
      l      �       �   &   �           7     N  9   b  <   �  N   �     (     7  g  T  &   �  !   �            I   :  ?   �  U   �       #   +                                
      	       Choosing torrent client interface "%Q" Invalid client interface "%Q"! Last interface error:  No client selected! No torrent client interface selected. Try /torrent.detect Seems that there is no usable torrent client on this machine The selected torrent client interface failed to execute the requested function Torrent Client Using client interface "%Q". Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Robert Förster <Dessa@gmake.de>
Language-Team: Turkish (http://www.transifex.com/kvirc/KVIrc/language/tr/)
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Torrent istemcisi arayüz seçimi "%Q" Geçersiz istemci arayüzü "%Q"! Son arabirimi hatası: Hiçbir istemci seçilmemiş! Hiçbir torrent istemcisi arayüzü seçmedik. Deneyiniz; /torrent.detect Hiç kullanışlı torrent istemcisi bu makinede görünmüyor. Seçilen torrent istemci arayüzü istenen fonksiyonu gerçekleştirmede başarısız Torrent İstemci Kullanılan istemci arayüzü "%Q". 