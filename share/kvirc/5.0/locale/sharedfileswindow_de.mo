��    #      4  /   L           	  
             "     &     -  
   H     S  )   [  
   �     �     �     �     �     �  #   �     "     '     0     5     ;  7   U     �  %   �     �     �  !   �     �  �   �     z  C   �  0   �       
   "  g  -     �     �     �     �  	   �     �     �  	   �  3     
   9     D  	   Y     c     �  #   �  +   �     �  	   �      	     	     		  Q   )	     {	  :   ~	  	   �	     �	  +   �	     �	  �   
  #   �
  C   �
  8        >     ^                                #                          !   
                               "                                                             	    &Add... &Browse... &Edit &OK Cancel Error Opening File - KVIrc Expire at: Expires Expires in %d hours %d minutes %d seconds File path: File: %s (%u bytes) Filename Invalid Expiry Time - KVIrc Invalid Share Name - KVIrc Invalid timeout, ignoring Invalid visible name: using default Mask Mask: %s Name Never No active file sharedfile No sharedfile with visible name '%Q' and user mask '%Q' OK Oops! Failed to add the sharedfile... Re&move Share name: Shared File Configuration - KVIrc Shared Files The expiry date/time is in the past: please either remove the "Expires at"check mark or specify a expiry date/time in the future The file '%Q' is not readable The file doesn't exist or it is not readable, please check the path The share name can't be empty, please correct it Total: %d sharedfile User mask: Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Robert Förster <Dessa@gmake.de>
Language-Team: German (http://www.transifex.com/kvirc/KVIrc/language/de/)
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 &Hinzufügen... &Auswählen... &Bearbeiten &OK Abbrechen Öffnen fehlgeschlagen - KVIrc Läuft ab am: Läuft ab Läuft in %d Stunden, %d Minuten und %d Sekunden ab Dateipfad: Datei: %s (%u bytes) Dateiname Ungültige Ablaufzeit- KVIrc Ungültiger Freigabname - KVIrc Ungültiger Timeout, wird ignoriert Ungültiger sichtbarer Name: nutze Standard Maske Maske: %s Name Nie Keine aktive freigegebene Datei Keine freigegebene Datei mit sichtbarem Namen ›%Q‹ und Benutzermaske ›%Q‹ OK Ups! Hinzufügen der freigegebenen Datei fehlgeschlagen... &Löschen Freigabename: Konfiguration freigegebener Dateien - KVIrc Freigegebene Dateien Das Ablaufdatum liegt in der Vergangenheit: Bitte deselektieren Sie entweder »Läuft ab am«, oder geben Sie ein Datum in der Zukunft an. Die Datei ›%Q‹ ist nicht lesbar Die Datei existiert nicht oder ist nicht lesbar, bitte Pfad prüfen Der Freigabename kann nicht leer sein, bitte korrigieren Gesamt: %d freigegebene Dateien Benutzermaske: 