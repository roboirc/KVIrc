��          �      �       0  $   1     V  "   t  #   �  $   �  .   �  -     *   =  0   h  ,   �  -   �  '   �  g       �     �     �  !   �        .   &  .   U  )   �  0   �  -   �  .     %   <                                    
         	             The network specified already exists The specified IP is not valid The specified proxy already exists The specified server already exists You must provide the IP as parameter You must provide the network name as parameter You must provide the port number as parameter You must provide the protocol as parameter You must provide the proxy hostname as parameter You must provide the proxy name as parameter You must provide the server name as parameter You must provide the value as parameter Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: ctrlaltca <ctrlaltca@gmail.com>
Language-Team: Italian (http://www.transifex.com/kvirc/KVIrc/language/it/)
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 La rete specificata esiste già L'IP specificato non è valido Il proxyspecificato esiste già Il server specificato esiste già Devi fornire l'IP come parametro Devi fornire il nome della rete come parametro Devi fornire il numero di porta come parametro Devi fornire il protocollo come parametro Devi fornire l'hostname del proxy come parametro Devi fornire il nome del proxy come parametro Devi fornire il nome del server come parametro Devi fornire il valore come parametro 