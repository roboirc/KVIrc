��          T      �       �   2   �      �   C     \   H  H   �  >   �  r  -  =   �     �  I   �  a   B  M   �  F   �                                        Internal error: Python interpreter not initialized Python execution error: The pythoncore module can't be loaded: Python support not available The pythoncore module failed to execute the code: something is wrong with the Python support This KVIrc executable has been compiled without Python scripting support To see more details about loading failure try /pythoncore.load Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-01-11 02:07+0000
Last-Translator: David Martí <neikokz+transifex@gmail.com>
Language-Team: Spanish (http://www.transifex.com/kvirc/KVIrc/language/es/)
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Error interno: el intérprete de Python no está inicializado Error al ejecutar Python: El módulo pythoncore no se puede cargar: soporte de Python no disponible El módulo pythoncore ha fallado al ejecutar el código: algo no funciona en el soporte de Python Este ejecutable de KVIrc ha sido compilado sin soporte de scripting en Python Para ver más detalles sobre el error de carga prueba /pythoncore.load 