��            )   �      �     �     �     �     �     �     �     �     �  ,   �  &        D     W  '   u     �     �     �  
   �     �     �     �     �     �     �          #     3  '   C     k  i  �  	   �  "   �                '     8     @     O  0   \  -   �     �  '   �  4   �     3     :     B     T     g     �     �     �     �     �     �     �       /   3     c                                                             
                                                            	                 %1 on %2 Can't log to file '%1' Cancel Channel Channel %1 on %2 Console Console on %1 DCC Chat Do you really wish to delete all these logs? Do you really wish to delete this log? Export Log - KVIrc Failed to export the log '%1' Failed to load logview module, aborting Filter Index Log File Log Viewer Log contents mask: Log name mask: Other Query Show DCC chat logs Show channel logs Show console logs Show other logs Show query logs This window has no logging capabilities Window '%1' not found Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: ctrlaltca <ctrlaltca@gmail.com>
Language-Team: Esperanto (http://www.transifex.com/kvirc/KVIrc/language/eo/)
Language: eo
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 %1 sur %2 Ne povas protokoli al dosiero '%1' Rezigni Kanalo Kanalo %1 sur %2 Konzolo Konzolo sur %1 DCC Babilado Ĉu vi vere volas forigi ĉi ĉiujn protokolojn? Ĉu vi vere volas forigi ĉi tiun protokolon? Eksporta Protokolo - KVIrc Malsukcesis eksporti la protokolon '%1' Malsukcesis ŝargi protokolrigardan modulon, abortas Filtri Indekso Protokola Dosiero Protokola Vidigilo Protokoli enhavan maskon: Protokola noma masko Alia Demando Montri DCC babilajn protokolojn Montri kanalajn protokolojn Montri konzolajn protokolojn Montri aliajn protokolojn Montri demandajn protokolojn Ĉi tiu fenestro ne havas protokolajn kapablojn Fenestro '%1' ne trovita 