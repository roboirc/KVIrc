��          �   %   �      P  A   Q  O   �  P   �  C   4  J   x  �   �  �   S  `   �  �   M  e     V   �    �  K   �  "   )     L  "   g  $   �     �     �  :   �  K   	  :   f	  U   �	  L   �	  ,   D
     q
  B   r  P   �  R     D   Y  L   �  �   �  �     \     �   {  g   T  X   �      G     "   _  $   �  2   �  6   �  %        7  P   R  T   �  b   �  Z   [  ]   �  A                                                         
                                                               	        An interface for Amarok2.
Download it from http://amarok.kde.org
 An interface for BMPx.
Download it from http://sourceforge.net/projects/beepmp
 An interface for Clementine.
Download it from http://www.clementine-player.org/
 An interface for Qmmp.
Download it from http://qmmp.ylsoftware.com
 An interface for Totem.
Download it from http://projects.gnome.org/totem/
 An interface for VLC.
Download it from http://www.videolan.org/
You need to manually enable the D-Bus control
interface in the VLC preferences
 An interface for the AMIP plugin.
You can download it from http://amip.tools-for.net
To use this interface you must install AMIP plugin for your player. An interface for the Audacious media player.
Download it from http://audacious-media-player.org
 An interface for the Mozilla Songbird media player.
Download it from http://www.getsongbird.com.
To use it you have to install also the MPRIS addon available at http://addons.songbirdnest.com/addon/1626.
 An interface for the UNIX Audacious media player.
Download it from http://audacious-media-player.org
 An interface for the UNIX XMMS media player.
Download it from http://legacy.xmms2.org
 An interface for the Winamp media player.
You can download it from http://www.winamp.com.
To use all the features of this interface you must copy the gen_kvirc.dll plugin found in the KVIrc distribution directory to the Winamp plugins folder and restart winamp. An interface for the XMMS2 media player.
Download it from http://xmms2.org
 Can't find a running Winamp window Can't find symbol %1 in %2 Can't load the player library (%1) Choosing media player interface "%Q" Function not implemented Last interface error:  No mediaplayer interface selected. Try /mediaplayer.detect Not sure about the results, trying a second, more aggressive detection pass Seems that there is no usable media player on this machine The Winamp plugin has not been installed properly. Check /help mediaplayer.nowplaying The selected media player interface failed to execute the requested function Trying media player interface "%1": score %2 Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Sölve Svartskogen <solaizen@gmail.com>
Language-Team: Polish (http://www.transifex.com/kvirc/KVIrc/language/pl/)
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);
 Interfejs dla Amarok2.
Możesz pobrać go z http://amarok.kde.org
 Interfejs dla BMPx.
Możesz pobrać go z http://sourceforge.net/projects/beepmp
 Interfejs dla Clementine.
Możesz ją pobrać z http://www.clementine-player.org/
 Interfejs dla Qmmp.
Możesz pobrać go z http://qmmp.ylsoftware.com
 Interfejs dla Totema.
Możesz pobrać go z http://projects.gnome.org/totem/
 Interfejs dla VLC.
Możesz pobrać go z http://www.videolan.org/
Musisz ręcznie włączyć obsługę interfejsu kontroli D-Bus
w ustawieniach VLC
 Interfejs dla wtyczki AMIP.
Możesz pobrać go z http://amip.tools-for.net
By z niego skorzystać, musisz zainstalować wtyczkę AMIP dla Twojego odtwarzacza. Interfejs dla odtwarzacza Audacious.
Możesz pobrać go z http://audacious-media-player.org
 Interfejs dla odtwarzacza Mozilla Songbird.
Możesz pobrać go z http://www.getsongbird.com.
By go użyć, musisz zainstalować też dodatek MPRIS, który jest dostępny na http://addons.songbirdnest.com/addon/1626.
 Interfejs dla uniksowego odtwarzacza Audacious.
Możesz pobrać go z http://audacious-media-player.org
 Interfejs dla uniksowego odtwarzacza XMMS.
Możesz pobrać go z http://legacy.xmms2.org
 Interfejs dla odtwarzacza Winamp.
Możesz pobrać go z http://www.winamp.com.
By skorzystać ze wszystkich funkcjonalności tego interfejsu, musisz skopiować wtyczkę gen_kvirc.dll z katalogu KVIrc do folderu z wtyczkami Winampa i zrestartować odtwarzacz. Interfejs dla odtwarzacza XMMS2.
Możesz pobrać go z http://xmms2.org
 Nie można odnaleźć okna Winampa Nie można znaleźć symbolu %1 w %2 Nie można załadować biblioteki odtwarzacza (%1) Wybieranie interfejsu odtwarzacza multimedialnego "%Q" Funkcja nie została zaimplementowana Ostatni błąd interfejsu: Nie wybrano interfejsu odtwarzacza multimedialnego. Spróbuj /mediaplayer.detect Wykrywanie nie dało pewnych wyników, próbowanie agresywniejszej metody wykrywania Wygląda na to, że nie ma nadającego się do użytku odtwarzacza multimedialnego na tej maszynie Wtyczka Winampa nie została prawidłowo zainstalowana. Wpisz /help mediaplayer.nowplaying Wybranemu interfejsowi odtwarzacza multimedialnego nie udało się wykonać żądanej funkcji Próbowanie interfejsu odtwarzacza multimedialnego "%1": wynik %2 