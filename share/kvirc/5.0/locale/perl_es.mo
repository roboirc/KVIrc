��          T      �       �   0   �      �   ?      X   @  F   �  <   �  r    ;   �     �  E   �  W   ,  F   �  R   �                                        Internal error: Perl interpreter not initialized Perl execution error: The perlcore module can't be loaded: Perl support not available The perlcore module failed to execute the code: something is wrong with the Perl support This KVIrc executable has been compiled without Perl scripting support To see more details about loading failure try /perlcore.load Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-01-11 02:06+0000
Last-Translator: David Martí <neikokz+transifex@gmail.com>
Language-Team: Spanish (http://www.transifex.com/kvirc/KVIrc/language/es/)
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Error interno: el intérprete de Perl no está inicializado Error de ejecución Perl: El módulo perlcore no se puede cargar: soporte de Perl no disponible El módulo perlcore no pudo ejecutar el código: algo no funciona en el soporte de Perl Este ejecutable de KVIrc ha sido compilado sin soporte de scripts Perl Para ver más detalles acerca del fallo durante la carga prueba con /perlcore.load 