��          �      |      �     �            	   !  
   +  /   6  )   f     �     �     �     �     �     �     �     �  
                        2     7  e  ;      �  "   �     �  
   �     �  6     1   K  "   }     �     �     �     �     �     �     �     	          "     /     E     J                                                   
                                       	       &Clear Terminated &Copy Path to Clipboard &Open &Other... Clear &All Clear all transfers, including any in progress? Do you really want to delete the file %1? Failed to remove the file File Transfers Information Local &File MS-DOS Prompt at Location No OK Open &Location Open &With Progress Size: %1 Terminal at Location Type Yes Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-23 21:28+0000
Last-Translator: ctrlaltca <ctrlaltca@gmail.com>
Language-Team: French (http://www.transifex.com/kvirc/KVIrc/language/fr/)
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 &Retirer les transfers Terminés &Copier Chemin dans Presse-papiers &Ouvrir &Autres... Retirer &Tous les transfers Effacer tous les tranferts, y compris ceux en cours ? Désirez-vous vraiment supprimer le fichier %1 ? Impossible de supprimer le fichier Transferts de fichiers Information &Fichier Local Prompt MS-DOS à l'Adresse Non Valider Ouvrir A&dresse Ou&vrir avec... Progrès Taille : %1 Terminal à l'Adresse Type Oui 