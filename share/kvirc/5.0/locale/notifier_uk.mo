��          �            h     i     p  
   y  
   �  	   �     �     �  &   �     �  *   �  .     #   <     `      y  �  �     N     ]     n     ~     �     �     �  7   �  5   �  [   +  O   �  '   �  7   �  =   7                     
                                                       	    1 Hour 1 Minute 15 Minutes 30 Minutes 5 Minutes Disable Hide Permanently (Until Explicitly Enabled) Show/Hide input line The -t switch expects a timeout in seconds The specified timeout is not valid, assuming 0 The specified window does not exist Until KVIrc is Restarted Write text or commands to window Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: ctrlaltca <ctrlaltca@gmail.com>
Language-Team: Ukrainian (http://www.transifex.com/kvirc/KVIrc/language/uk/)
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 1 Година 1 Хвилина 15 Хвилин 30 Хвилин 5 Хвилин Вимкнути Сховати Назавжди (До явного включення) Показати/Сховати рядок вводу Використайте ключ -t щоб задати таймаут у секундах Заданий таймаут недійсний, приймається за 0 Задане вікно не існує Поки KVIrc не буде перезапущений Введіть текст або команду у вікні 