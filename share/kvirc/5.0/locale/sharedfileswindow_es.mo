��          �   %   �      P     Q  
   Y     d     j     n  
   u     �  
   �     �     �  #   �     �     �     �     �  7        <     ?     G     S     `  C   ~  0   �     �  
     g       {     �     �     �     �  
   �     �     �     �     �  8        :     C     J      P  J   q     �  	   �     �     �     �  E     M   Z     �     �                                              
              	                                                                 &Add... &Browse... &Edit &OK Cancel Expire at: Expires File path: Filename Invalid timeout, ignoring Invalid visible name: using default Mask Name Never No active file sharedfile No sharedfile with visible name '%Q' and user mask '%Q' OK Re&move Share name: Shared Files The file '%Q' is not readable The file doesn't exist or it is not readable, please check the path The share name can't be empty, please correct it Total: %d sharedfile User mask: Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: ctrlaltca <ctrlaltca@gmail.com>
Language-Team: Spanish (http://www.transifex.com/kvirc/KVIrc/language/es/)
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 &Añadir... &Navegar... &Editar &Aceptar Cancelar Expira el: Expira Ruta del archivo: Nombre del archivo Timeout inválido, ignorando Nombre visible no válido: usando el que hay por defecto Máscara Nombre Nunca No hay archivo compartido activo No hay archivo compartido con el nombre '%Q' y la máscara de usuario '%Q' Aceptar Eli&minar Nombre compartido: Archivos compartidos El archivo '%Q' no es legible El archivo no existe o no se puede leer, por favor, comprueba la ruta El nombre del archivo compartido no puede estar vacío; por favor, corrígelo Total: %d archivos compartidos Máscara de usuario: 