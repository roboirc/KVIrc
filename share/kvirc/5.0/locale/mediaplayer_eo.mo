��    
      l      �       �      �   "     $   /     T     m  :   �  :   �  L   �  ,   G  j  t      �  +      &   ,     S     h  >     @   �  I   �  4   I         	                                
    Can't find symbol %1 in %2 Can't load the player library (%1) Choosing media player interface "%Q" Function not implemented Last interface error:  No mediaplayer interface selected. Try /mediaplayer.detect Seems that there is no usable media player on this machine The selected media player interface failed to execute the requested function Trying media player interface "%1": score %2 Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Robert Förster <Dessa@gmake.de>
Language-Team: Esperanto (http://www.transifex.com/kvirc/KVIrc/language/eo/)
Language: eo
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Ne povas trovi simbolon %1 en %2 Ne povas ŝargi la ludilan bibliotekon (%1) Elektas median ludilan interfacon "%Q" Funkcio ne realigita Lasta interfaca eraro: Neniu medialudila interfaco elektita. Penu /mediaplayer.detect Ŝajnas ke estas neniu uzebla media ludilo ĉe ĉi tiu komputilo La elektita media ludila interfaco malsukcesis lanĉi la petitan funkcion Provante median ludilan interfacon "%1": poentaro %2 