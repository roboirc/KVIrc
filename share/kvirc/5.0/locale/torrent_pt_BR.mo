��          |      �          Q   !  &   s     �     �     �  9   �  <     N   [     �  .   �     �  e    R   k  .   �  $   �          .  G   K  D   �  P   �     )  <   9  #   v              
                                         	    An interface for KDE's KTorrent client.
Download it from http://www.ktorrent.org
 Choosing torrent client interface "%Q" Invalid client interface "%Q"! Last interface error:  No client selected! No torrent client interface selected. Try /torrent.detect Seems that there is no usable torrent client on this machine The selected torrent client interface failed to execute the requested function Torrent Client Trying torrent client interface "%Q": score %d Using client interface "%Q". Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Daniel Neves
Language-Team: Portuguese (Brazil) (http://www.transifex.com/kvirc/KVIrc/language/pt_BR/)
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Uma interface para o cliente KDE's KTorrent.
Baixá-lo em http://www.ktorrent.org
 A escolher o interface do cliente torrent "%Q" Interface de cliente inválido "%Q"! Erro do último interface:  Nenhum cliente seleccionado! Nenhum interface de cliente torrent seleccionado. Tente /torrent.detect Parece que não existe um cliente torrent utilizável nesta máquina O interface do cliente torrent seleccionado falhou ao executar a função pedida Cliente Torrent Tentando o interface do cliente torrent "%Q": pontuação %d Usando o interface do cliente "%Q". 