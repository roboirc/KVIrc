��            )   �      �     �     �     �     �     �     �     �     �  ,   �  &        D     W  '   u     �     �     �  
   �     �     �     �     �     �     �          #     3  '   C     k  g  �     �  .   �     !     *     0     ?     G     U  9   ^  1   �     �     �  .   �     .     5     =     Q     d     |     �     �     �     �     �     �       $   ,     Q                                                             
                                                            	                 %1 on %2 Can't log to file '%1' Cancel Channel Channel %1 on %2 Console Console on %1 DCC Chat Do you really wish to delete all these logs? Do you really wish to delete this log? Export Log - KVIrc Failed to export the log '%1' Failed to load logview module, aborting Filter Index Log File Log Viewer Log contents mask: Log name mask: Other Query Show DCC chat logs Show channel logs Show console logs Show other logs Show query logs This window has no logging capabilities Window '%1' not found Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: ctrlaltca <ctrlaltca@gmail.com>
Language-Team: Spanish (http://www.transifex.com/kvirc/KVIrc/language/es/)
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 %1 en %2 No se pudo guardar el log en el archivo «%1» Cancelar Canal Canal %1 el %2 Consola Consola el %1 Chat DCC ¿Estás seguro de que quieres eliminar todos estos logs? ¿Estás seguro de que quieres eliminar este log? Exportar log - KVIrc Error al exportar el log «%1» Error al cargar el módulo logview, cancelando Filtro Índice Archivo de registro Visor del registro Máscara de contenidos: Máscara de nombres: Otro Privado Mostrar registros de chat DCC Mostrar registros de canales Mostrar registros de consola Mostrar otros registros Mostrar registros de privados Esta ventana no puede generar un log Ventana «%1» no encontrada 