��            )   �      �     �     �     �     �     �     �     �     �  ,   �  &        D     W  '   u     �     �     �  
   �     �     �     �     �     �     �          #     3  '   C     k  i  �     �  "   �               %     >  
   E     P  7   b  1   �      �  *   �  C        \     d     j     w     �     �     �     �  "   �     �               4  7   N     �                                                             
                                                            	                 %1 on %2 Can't log to file '%1' Cancel Channel Channel %1 on %2 Console Console on %1 DCC Chat Do you really wish to delete all these logs? Do you really wish to delete this log? Export Log - KVIrc Failed to export the log '%1' Failed to load logview module, aborting Filter Index Log File Log Viewer Log contents mask: Log name mask: Other Query Show DCC chat logs Show channel logs Show console logs Show other logs Show query logs This window has no logging capabilities Window '%1' not found Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-23 21:28+0000
Last-Translator: ctrlaltca <ctrlaltca@gmail.com>
Language-Team: Hungarian (http://www.transifex.com/kvirc/KVIrc/language/hu/)
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 %1 %2 Nem tudok naplózni a fájlba:'%1' Mégsem Csatorna %1 csatorna %2 hálózat Konzol Konzol: %1 DCC beszélgetés Tényleg törölni szeretnéd ezeket a naplófájlokat? Tényleg törölni szeretnéd ezt a naplófájlt? Naplófájl Exportálás - KVIrc Naplófájl exportálása sikertelen: '%1' Naplófájl böngésző modul betöltése sikertelen, megszakítás Szűrő Index Napló Fájl Naplófájl Böngésző Napló tartalom maszk: Napló név maszk: Más Privát DCC beszélgetésnaplók mutatása Csatorna naplók mutatása Konzol naplók mutatása Más naplók mutatása Privát naplók mutatása Ez az ablak nem rendelkezik naplózási képességekkel '%1' ablak nem található 