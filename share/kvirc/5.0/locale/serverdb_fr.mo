��          �      �       0  $   1     V  "   t  #   �  $   �  .   �  -     *   =  0   h  ,   �  -   �  '   �  e    #   �  )   �  )   �  #   �  /     2   N  3   �  -   �  3   �  0     2   H  *   {                                    
         	             The network specified already exists The specified IP is not valid The specified proxy already exists The specified server already exists You must provide the IP as parameter You must provide the network name as parameter You must provide the port number as parameter You must provide the protocol as parameter You must provide the proxy hostname as parameter You must provide the proxy name as parameter You must provide the server name as parameter You must provide the value as parameter Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-23 21:28+0000
Last-Translator: ctrlaltca <ctrlaltca@gmail.com>
Language-Team: French (http://www.transifex.com/kvirc/KVIrc/language/fr/)
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Le réseau spécifié existe déjà L'adresse IP spécifiée n'est pas valide Le serveur proxy spécifié existe déjà Le serveur spécifié existe déjà Vous devez fournir une adresse IP en paramètre Vous devez fournir le nom du réseau en paramètre Vous devez fournir le numéro du port en paramètre Vous devez fournir le protocole en paramètre Vous devez fournir l'adresse du proxy en paramètre Vous devez fournir le nom du proxy en paramètre Vous devez fournir le nom du serveur en paramètre Vous devez fournir la valeur en paramètre 