��          �      �       H     I     P  
   Y  
   d  	   o     y     �  &   �  *   �  .   �  #        +      D  g  e     �     �     �     �     �            &   !  ,   H  >   u     �  !   �  (   �           	                                       
               1 Hour 1 Minute 15 Minutes 30 Minutes 5 Minutes Disable Hide Permanently (Until Explicitly Enabled) The -t switch expects a timeout in seconds The specified timeout is not valid, assuming 0 The specified window does not exist Until KVIrc is Restarted Write text or commands to window Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: ctrlaltca <ctrlaltca@gmail.com>
Language-Team: Finnish (http://www.transifex.com/kvirc/KVIrc/language/fi/)
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Tunti Minuutti 15 minuuttia Puoli tuntia Viisi minuuttia Ota pois päältä Piilota Vakituisesti (ellei toisin säädetä) -t -kytkin olettaa aikakatkaisun sekunneissa Asetettu aikakatkaisu ei ole oikein muotoiltu, oletetaan 0:ksi Tämä ikkuna ei ole olemassa Kunnes KVIrc aloitetaan uudestaan Kirjoita tekstiä tai komentoja ikkunaan 