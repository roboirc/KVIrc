��          �   %   �      P     Q  
   Y     d     j     n  
   u     �  
   �     �     �  #   �     �     �     �     �  7        <     ?     G     S     `  C   ~  0   �     �  
     i       }     �     �     �     �     �     �     �  	   �  $   �  6        ;     A     F     K  O   i     �     �     �     �     �  Q     4   _     �     �                                              
              	                                                                 &Add... &Browse... &Edit &OK Cancel Expire at: Expires File path: Filename Invalid timeout, ignoring Invalid visible name: using default Mask Name Never No active file sharedfile No sharedfile with visible name '%Q' and user mask '%Q' OK Re&move Share name: Shared Files The file '%Q' is not readable The file doesn't exist or it is not readable, please check the path The share name can't be empty, please correct it Total: %d sharedfile User mask: Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-23 21:28+0000
Last-Translator: ctrlaltca <ctrlaltca@gmail.com>
Language-Team: Hungarian (http://www.transifex.com/kvirc/KVIrc/language/hu/)
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Hozzáadás &Böngészés... Szerkesztés &OK Mégsem Lejár: Lejárat Fájl útvonala: Fájlnév Helytelen időtúllépés, mellőzve Helytelen látható név: alapértelmezett használata Maszk Név Soha Nincs aktív megosztott fájl Nincs megosztott fájl ezzal a látható névvel: '%Q' felhasználómaszk: '%Q' OK Eltávolítás Megosztási név: Megosztott Fájlok A fájl nem olvasható: '%Q' A fájl nem létezik, vagy nem olvasható: kérlek ellenőrízd az elérési utat A megosztási név nem lehet üres, kérlek javítsd Összesen: %d megosztott fájl Felhasználó maszk: 