��    
      l      �       �      �   "     $   /     T     m  :   �  :   �  L   �  ,   G  k  t  /   �  6     (   G     p     �  D   �  ;   �  I   (  5   r         	                                
    Can't find symbol %1 in %2 Can't load the player library (%1) Choosing media player interface "%Q" Function not implemented Last interface error:  No mediaplayer interface selected. Try /mediaplayer.detect Seems that there is no usable media player on this machine The selected media player interface failed to execute the requested function Trying media player interface "%1": score %2 Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Robert Förster <Dessa@gmake.de>
Language-Team: Portuguese (http://www.transifex.com/kvirc/KVIrc/language/pt/)
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Não é possível encontrar o símbolo %1 em %2 Não é possível carregar a biblioteca do leitor (%1) A escolher um interface multimédia "%Q" Função não implementada Erro do último interface:  Nenhum interface multimédia seleccionado. Tente /mediaplayer.detect Parece que não existe nesta máquina um leitor multimédia O interface multimédia seleccionado falhou ao executar a função pedida A tentar o interface multimédia "%1": pontuação %2 