��          �   %   �      P     Q     c     {     �  	   �  
   �  /   �  &   �     �       )   3     ]     w     �     �     �     �     �     �  
   �     �     �     �     �       g       p      �     �     �  
   �     �  5   �  ,     "   9  %   \  ,   �  !   �     �     �     �     �          #     &     3     @     L     X     h     l                                                                	             
                                                &Clear Terminated &Copy Path to Clipboard &Delete File &Open &Other... Clear &All Clear all transfers, including any in progress? Confirm Clearing All Transfers - KVIrc Confirm File Delete - KVIrc Deleting File Failed - KVIrc Do you really want to delete the file %1? Failed to remove the file File Transfers Information Local &File MS-DOS Prompt at Location No OK Open &Location Open &With Progress Size: %1 Terminal at Location Type Yes Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Robert Förster <Dessa@gmake.de>
Language-Team: German (http://www.transifex.com/kvirc/KVIrc/language/de/)
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Beendete &löschen &Pfad in Zwischenablage kopieren &Lösche Datei &Öffnen &Andere... &Alle löschen Alle Übertragungen inklusive der laufenden löschen? Löschen aller Transfers bestätigen - KVIrc Dateilöschung bestätigen - KVIrc Dateilöschung fehlgeschlagen - KVIrc Soll die Datei %1 wirklich gelöscht werden? Löschen der Datei fehlgeschlagen Dateiübertragungen Information Lokale &Datei MS-DOS-Prompt an dieser Stelle Nein OK &Ort öffnen Öffnen &mit Fortschritt Größe: %1 Terminal an Ort Typ Ja 