��          T      �       �   0   �      �   ?      X   @  F   �  <   �       C        b  <   z  T   �  >     L   K                                        Internal error: Perl interpreter not initialized Perl execution error: The perlcore module can't be loaded: Perl support not available The perlcore module failed to execute the code: something is wrong with the Perl support This KVIrc executable has been compiled without Perl scripting support To see more details about loading failure try /perlcore.load Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Sölve Svartskogen <solaizen@gmail.com>
Language-Team: Polish (http://www.transifex.com/kvirc/KVIrc/language/pl/)
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);
 Błąd wewnętrzny - interpreter Perla nie został zainicjalizowany Błąd wykonania Perla: Moduł Perl nie może być załadowany - brak wsparcia Perla Modułowi Perla nie udało się wykonać kodu - coś jest nie tak ze wsparciem Perla Plik wykonywalny KVIrc został skompilowany bez wsparcia Perla By zobaczyć szczegóły dotyczące błędu ładowania, wpisz /perlcore.load 