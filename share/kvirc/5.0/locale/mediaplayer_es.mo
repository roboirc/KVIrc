��    
      l      �       �      �   "     $   /     T     m  :   �  :   �  L   �  ,   G  h  t  &   �  0     4   5     j     �  C   �  G   �  _   -  )   �         	                                
    Can't find symbol %1 in %2 Can't load the player library (%1) Choosing media player interface "%Q" Function not implemented Last interface error:  No mediaplayer interface selected. Try /mediaplayer.detect Seems that there is no usable media player on this machine The selected media player interface failed to execute the requested function Trying media player interface "%1": score %2 Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Robert Förster <Dessa@gmake.de>
Language-Team: Spanish (http://www.transifex.com/kvirc/KVIrc/language/es/)
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 No pude encontrar el símbolo %1 en %2 No pude cargar la librería del reproductor (%1) Eligiendo la interfaz de reproductor multimedia "%Q" Función no implementada Último error de la interfaz: No hay ningún reproductor seleccionado. Prueba /mediaplayer.detect Parece que no hay ningún reproductor de medios usable en esta máquina La interfaz de reproductor de medios seleccionado ha fallado al ejecutar la función solicitada Probando la interfaz "%1": puntuación %2 