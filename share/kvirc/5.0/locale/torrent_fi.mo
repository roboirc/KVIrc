��    
      l      �       �   &   �           7     N  9   b  <   �  N   �  .   (     W  h  t  8   �  1     "   H     k  N   �  U   �  _   ,  G   �  6   �                                
      	       Choosing torrent client interface "%Q" Invalid client interface "%Q"! Last interface error:  No client selected! No torrent client interface selected. Try /torrent.detect Seems that there is no usable torrent client on this machine The selected torrent client interface failed to execute the requested function Trying torrent client interface "%Q": score %d Using client interface "%Q". Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Robert Förster <Dessa@gmake.de>
Language-Team: Finnish (http://www.transifex.com/kvirc/KVIrc/language/fi/)
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Valitaan torrenttiasiakasohjelman käyttöliittymä "%Q" Viallinen asiakasohjelman käyttöliittymä "%Q"! Viimeisin käyttöliittymävirhe:  Ei asiakasohjelmaa valittu! Ei torrenttiasiakasohjelman käyttölittymää valittu. Yritä /torrent.detect Näyttää siltä ettei tällä koneella ole käytettävää torrenttiasiakasohjelmaa Valittu torrenttiasiakasohjelman käyttöliittymä ei onnistunut ajamaan pyydettyä tehtävää Yritetään torrenttiasiakasohjelman käyttöliittymää "%Q": tulos %d Käytetään asiakasohjelman käyttöliittymää "%Q". 