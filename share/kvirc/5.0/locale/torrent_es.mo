��          t      �         &        8     W     n  9   �  <   �  N   �     H  .   W     �  h  �  0     &   =     d  $   �  P   �  D   �  Z   >     �  ?   �  #   �              	   
                                           Choosing torrent client interface "%Q" Invalid client interface "%Q"! Last interface error:  No client selected! No torrent client interface selected. Try /torrent.detect Seems that there is no usable torrent client on this machine The selected torrent client interface failed to execute the requested function Torrent Client Trying torrent client interface "%Q": score %d Using client interface "%Q". Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Robert Förster <Dessa@gmake.de>
Language-Team: Spanish (http://www.transifex.com/kvirc/KVIrc/language/es/)
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Eligiendo la interfaz de cliente de torrent "%Q" ¡Interfaz de cliente "%Q" no válida! Último error de la interfaz:  ¡No se seleccionó ningún cliente! No se seleccionó ninguna interfaz de cliente de torrent. Prueba /torrent.detect Parece que no hay ningún cliente de torrent usable en esta máquina La interfaz de cliente de torrent seleccionada falló al ejecutar la función especificada Cliente de torrent Probando la interfaz de cliente de torrent "%Q": puntuación %d Usando la interfaz de cliente "%Q". 