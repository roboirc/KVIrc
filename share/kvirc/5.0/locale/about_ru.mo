��          �      |      �     �     �  
          
        (     4     =     C     R     `  +   w     �     �  x   �  
   3     >     N     [     h     t  �  �     v     �  $   �     �     �     �          %  !   4     V  ;   k  1   �     �     �  �        �     �  8   �  ;   .     j     y               
                                            	                                      About Architecture Build Info Build command Build date Build flags CPU name Close Compiler flags Compiler name Executable Information Forged by the <b>KVIrc Development Team</b> Honor && Glory License Oops! Can't find the license file.
It MUST be included in the distribution...
Please report to <pragma at kvirc dot net> Qt version Revision number Runtime Info Sources date System name System version Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-23 21:28+0000
Last-Translator: Robert Förster <Dessa@gmake.de>
Language-Team: Russian (http://www.transifex.com/kvirc/KVIrc/language/ru/)
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 О программе Архитектура Информация о сборке Команда сборки Дата сборки Флаги сборки Процессор Закрыть Флаги компилятора Компилятор Информация об исполняемом файле Выковано в <b>KVIrc Development Team</b> Слава и почёт Лицензия Ой! Не могу найти файл лицензии...
Он ДОЛЖЕН быть в дистрибутиве...
Пожалуйста сообщите по адресу <pragma at kvirc dot net> Версия Qt Номер ревизии Информация времени выполнения Дата исходных текстов программы Система Версия системы 