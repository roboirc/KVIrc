��          T      �       �   0   �      �   ?      X   @  F   �  <   �  g    5   �     �  T   �  e   )  D   �  Q   �                                        Internal error: Perl interpreter not initialized Perl execution error: The perlcore module can't be loaded: Perl support not available The perlcore module failed to execute the code: something is wrong with the Perl support This KVIrc executable has been compiled without Perl scripting support To see more details about loading failure try /perlcore.load Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Robert Förster <Dessa@gmake.de>
Language-Team: German (http://www.transifex.com/kvirc/KVIrc/language/de/)
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Interner Fehler: Perl-Interpreter nicht initialisiert Perl-Ausführungsfehler: Das Perlcore-Modul konnte nicht geladen werden: Perl-Unterstützung nicht verfügbar Das Perlcore-Modul konnte den Code nicht ausführen: Irgendwas ist falsch mit der Perl-Unterstützung Dieser KVIrc-Build wurde ohne Perl-Scripting-Unterstützung erstellt Um mehr Details über den Fehler beim Laden zu erfahren /perlcore.load ausführen 