��    
      l      �       �      �   "     $   /     T     m  :   �  :   �  L   �  ,   G  �  t  0   )  K   Z  =   �  *   �  3     e   C  `   �  y   
  L   �         	                                
    Can't find symbol %1 in %2 Can't load the player library (%1) Choosing media player interface "%Q" Function not implemented Last interface error:  No mediaplayer interface selected. Try /mediaplayer.detect Seems that there is no usable media player on this machine The selected media player interface failed to execute the requested function Trying media player interface "%1": score %2 Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Robert Förster <Dessa@gmake.de>
Language-Team: Ukrainian (http://www.transifex.com/kvirc/KVIrc/language/uk/)
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Не можу знайти символ %1 в %2 Не можу завантажити бібліотеку плеєра (%1) Обраний інтерфейс медіаплеєра "%Q" Функція не реалізована Остання помилка інтерфейсу: Інтерфейс медіаплеєра не обраний. Спробуйте /mediaplayer.detect Здається на цій машині немає придатних медіаплеєрів Обраний інтерфейс медіаплеєра не зміг виконати необхідну функцію Пробую інтерфейс медіаплеєра "%1": спроба %2 