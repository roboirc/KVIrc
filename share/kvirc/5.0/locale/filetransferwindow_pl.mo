��          �   %   �      P     Q     c     {     �  	   �  
   �  /   �  &   �     �       )   3     ]     w     �     �     �     �     �     �  
   �     �     �     �     �               	           >     J     S     \  1   p  7   �  !   �  )   �  %   &     L     k  
   }     �  ,   �     �     �  +   �     �               !     9     =                                                                	             
                                                &Clear Terminated &Copy Path to Clipboard &Delete File &Open &Other... Clear &All Clear all transfers, including any in progress? Confirm Clearing All Transfers - KVIrc Confirm File Delete - KVIrc Deleting File Failed - KVIrc Do you really want to delete the file %1? Failed to remove the file File Transfers Information Local &File MS-DOS Prompt at Location No OK Open &Location Open &With Progress Size: %1 Terminal at Location Type Yes Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Sölve Svartskogen <solaizen@gmail.com>
Language-Team: Polish (http://www.transifex.com/kvirc/KVIrc/language/pl/)
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);
 &Wyczyść zakończone &Kopiuj ścieżkę do schowka &Usuń plik &Otwórz &Inne... Wyczyś&ć wszystko Wyczyścić wszystkie transfery, nawet te w toku? Potwierdź wyczyszczenie wszystkich transferów - KVIrc Potwierdź usuwanie pliku - KVIrc Usuwanie pliku nie powiodło się - KVIrc Czy na pewno chcesz usunąć plik %1? Nie udało się usunąć pliku Transfery plików Informacja Lokaln&y plik Wiersz poleceń MS-DOS w miejscu lokalizacji Nie OK Otwór&z lokalizację (menedżerem plików) Otwór&z za pomocą... Postęp Rozmiar: %1 Terminal na lokalizacji Typ Tak 