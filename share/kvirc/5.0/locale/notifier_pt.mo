��          �            h     i     p  
   y  
   �  	   �     �     �  &   �     �  *   �  .     #   <     `      y  j  �            
     
      	   +  
   5     @  2   I  !   |  5   �  :   �          -  (   I                     
                                                       	    1 Hour 1 Minute 15 Minutes 30 Minutes 5 Minutes Disable Hide Permanently (Until Explicitly Enabled) Show/Hide input line The -t switch expects a timeout in seconds The specified timeout is not valid, assuming 0 The specified window does not exist Until KVIrc is Restarted Write text or commands to window Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: ctrlaltca <ctrlaltca@gmail.com>
Language-Team: Portuguese (http://www.transifex.com/kvirc/KVIrc/language/pt/)
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 1 Hora 1 Minuto 15 Minutos 30 Minutos 5 Minutos Desactivar Esconder Permanentemente (Até Ser Activado Explicitamente) Mostrar/Esconder linha de entrada A opção -t espera um intervalo de tempo em segundos O intervalo de tempo indicado não é válido, a assumir 0 A janela indicada não existe Até o KVIrc ser Reiniciado Escrever texto ou comandos para a janela 