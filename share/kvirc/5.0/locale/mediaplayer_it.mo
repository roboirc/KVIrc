��          �   %   �      P  A   Q  O   �  P   �  C   4  J   x  �   �  �   S  `   �  �   M  e     V   �    �  K   �  "   )     L  "   g  $   �     �     �  :   �  K   	  :   f	  U   �	  L   �	  ,   D
  h  q
  <   �  J     K   b  >   �  E   �  �   3  �   �  Z   a  �   �  Y     P   �    *  E   @  '   �  '   �  ,   �  %        )      C  S   d  <   �  B   �  ^   8  S   �  (   �                                                      
                                                               	        An interface for Amarok2.
Download it from http://amarok.kde.org
 An interface for BMPx.
Download it from http://sourceforge.net/projects/beepmp
 An interface for Clementine.
Download it from http://www.clementine-player.org/
 An interface for Qmmp.
Download it from http://qmmp.ylsoftware.com
 An interface for Totem.
Download it from http://projects.gnome.org/totem/
 An interface for VLC.
Download it from http://www.videolan.org/
You need to manually enable the D-Bus control
interface in the VLC preferences
 An interface for the AMIP plugin.
You can download it from http://amip.tools-for.net
To use this interface you must install AMIP plugin for your player. An interface for the Audacious media player.
Download it from http://audacious-media-player.org
 An interface for the Mozilla Songbird media player.
Download it from http://www.getsongbird.com.
To use it you have to install also the MPRIS addon available at http://addons.songbirdnest.com/addon/1626.
 An interface for the UNIX Audacious media player.
Download it from http://audacious-media-player.org
 An interface for the UNIX XMMS media player.
Download it from http://legacy.xmms2.org
 An interface for the Winamp media player.
You can download it from http://www.winamp.com.
To use all the features of this interface you must copy the gen_kvirc.dll plugin found in the KVIrc distribution directory to the Winamp plugins folder and restart winamp. An interface for the XMMS2 media player.
Download it from http://xmms2.org
 Can't find a running Winamp window Can't find symbol %1 in %2 Can't load the player library (%1) Choosing media player interface "%Q" Function not implemented Last interface error:  No mediaplayer interface selected. Try /mediaplayer.detect Not sure about the results, trying a second, more aggressive detection pass Seems that there is no usable media player on this machine The Winamp plugin has not been installed properly. Check /help mediaplayer.nowplaying The selected media player interface failed to execute the requested function Trying media player interface "%1": score %2 Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Robert Förster <Dessa@gmake.de>
Language-Team: Italian (http://www.transifex.com/kvirc/KVIrc/language/it/)
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Interfaccia per Amarok2.
Scaricalo da http://amarok.kde.org
 Interfaccia per BMPx.
Scaricalo da http://sourceforge.net/projects/beepmp
 Interfaccia per Clementine.
Scaricalo da http://www.clementine-player.org/
 Interfaccia per Qmmp.
Scaricalo da http://qmmp.ylsoftware.com
 Interfaccia per Totem.
Scaricalo da http://projects.gnome.org/totem/
 Interfaccia per VLC.
Scaricarlo da http://www.videolan.org/
Devi abilitare manualmente l'interfaccia di controllo
D-Bus nelle preferenze di VLC.
 Interfaccia per il plugin AMIP.
Puoi scaricarlo da http://amip.tools-for.net.
Per usare questa interfaccia devi installare il plugin AMIP per il tuo player Interfaccia per il media player Audacious.
Scaricalo da http://audacious-media-player.org
 Interfaccia per il mediaplayer Mozilla Songbird.
Scaricalo da http://www.getsongbird.com.
Per usarlo devi installare anche l'addon MPRIS disponibile a http://addons.songbirdnest.com/addon/1626.
 Interfaccia per il player UNIX Audacious.
Scaricalo da http://audacious-media-player.org
 Interfaccia per il media player UNIX XMMS.
Scaricalo da http://legacy.xmms2.org
 Un'interfaccia per il media player Winamp.
Puoi scaricarlo da http://www.winamp.com.
Per usare tutte le caratteristiche di questa interfaccia devi copiare il plugin gen_kvirc.dll che puoi trovare nella directory di KVIrc nella directory dei plugin di Winamp e riavviare Winamp. Interfaccia per il media player Xmms2.
Scaricalo da http://xmms2.org
 Impossibile trovare una finestra Winamp Impossibile trovare il simbolo %1 in %2 Impossibile caricare la libreria sonora (%1) Interfaccia media player scelta: "%Q" Funzione non implementata Ultimo errore dell'interfaccia:  Non è stata selezionata alcuna interfaccia media player. Prova /mediaplayer.detect I risultati non sono certi, riprovo un'analisi più accurata Sembra non esserci un media player utilizzabile su questa macchina Il plugin Winamp non è stato installato correttamente. Controlla /help mediaplayer.nowplaying L'interfaccia media player scelta non è riuscita ad eseguire la funzione richiesta Provo il media player "%1": punteggio %2 