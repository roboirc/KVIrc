��    -      �  =   �      �     �     �     �                 !   .  "   P     s     {     �     �     �  ,   �  &   �               .  '   L     t     {     �     �  
   �     �     �  &   �     �     �               #     )  '   >     f     v     �     �     �     �     �  '   �       5        R     S	     \	  !   k	     �	     �	     �	  8   �	  8   �	     
     %
     3
     E
     N
  4   ]
  *   �
     �
     �
  -   �
  A   $     f     l     z     �     �     �     �  (   �               ,     1     ?     G  .   ]     �     �     �     �      �          *  %   6     \  :   u     
                   %                  -      !      *            "      #                $      (       	                           '       )            &              ,                      +                    %1 on %2 Apply Filter Can't log to file '%1' Cancel Channel Channel %1 on %2 Confirm Current User Log Deletion Confirm Current User Logs Deletion Console Console on %1 Contents Filter DCC Chat DCC Chat with: %1 Do you really wish to delete all these logs? Do you really wish to delete this log? Export Log - KVIrc Export Log File to Failed to export the log '%1' Failed to load logview module, aborting Filter HTML Archive Index Log File Log Viewer Log contents mask: Log name mask: Missing window ID after the 'w' switch Only newer than: Only older than: Other Plain Text File Query Query with: %1 on %2 Remove All Log Files Within This Folder Remove Log File Show DCC chat logs Show channel logs Show console logs Show other logs Show query logs Something on: %1 This window has no logging capabilities Window '%1' not found Window with ID '%1' not found, returning empty string Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Sölve Svartskogen <solaizen@gmail.com>
Language-Team: Polish (http://www.transifex.com/kvirc/KVIrc/language/pl/)
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);
 %1 na %2 Zastosuj filtr Nie można zapisać do pliku '%1' Anuluj Kanał Kanał %1 na %2 Zatwierdź usunięcie dziennika bieżącego użytkownika Zatwierdź usunięcie dziennika bieżącego użytkownika Konsola Konsola na %1 Filtr zawartości Czat DCC Czat DCC z: %1 Czy na pewno chcesz usunąć wszystkie te dzienniki? Czy na pewno chcesz usunąć ten dziennik? Eksportuj dziennik - KVIrc Wyeksportuj plik dziennika do Nie udało się wyeksportować dziennika '%1' Nie udało się załadować modułu widoku dziennika, przerywanie Filtr Archiwum HTML Indeks Plik dziennika Przeglądarka dziennika Maska zawartości dziennika: Maska nazwy dziennika: Brakujące ID okna po przełączniku 'w' Tylko nowsze niż: Tylko starsze niż: Inne Zwykły tekst Rozmowa Zapytanie z: %1 na %2 Usuń wszystkie pliki dziennika w tym folderze Usuń plik dziennika Pokaż dzienniki czatu DCC Pokaż dzienniki kanałów Pokaż dzienniki konsoli Pokaż dzienniki innych zdarzeń Pokaż dzienniki rozmowy Coś na: %1 To okno nie ma możliwości logowania Nie znaleziono okna '%1' Nie znaleziono okna z ID '%1', zwracam pusty ciąg znaków 