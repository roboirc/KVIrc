��          �            h     i     p  
   y  
   �  	   �     �     �  &   �     �  *   �  .     #   <     `      y  i  �            
     
     	   *  	   4     >     D     d  -     1   �  !   �       (                        
                                                       	    1 Hour 1 Minute 15 Minutes 30 Minutes 5 Minutes Disable Hide Permanently (Until Explicitly Enabled) Show/Hide input line The -t switch expects a timeout in seconds The specified timeout is not valid, assuming 0 The specified window does not exist Until KVIrc is Restarted Write text or commands to window Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: ctrlaltca <ctrlaltca@gmail.com>
Language-Team: Esperanto (http://www.transifex.com/kvirc/KVIrc/language/eo/)
Language: eo
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 1 Horo 1 Minuto 15 Minutoj 30 Minutoj 5 Minutoj Malebligi Kaŝi Daŭre (Ĝis Eksplike Ebligita) Montri/Kaŝi enigan linion La -t ŝaltilo atendas tempolimon en sekundoj La specifita tempolimo ne estas valida, supozas 0 La specifita fenestro ne ekzistas Ĝis KVIrc estas Restartita Skribi tekston aŭ komandojn al fenestro 