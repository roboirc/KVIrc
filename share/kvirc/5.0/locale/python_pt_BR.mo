��          T      �       �   2   �      �   C     \   H  H   �  >   �  e  -  0   �     �  Q   �  [   1  F   �  N   �                                        Internal error: Python interpreter not initialized Python execution error: The pythoncore module can't be loaded: Python support not available The pythoncore module failed to execute the code: something is wrong with the Python support This KVIrc executable has been compiled without Python scripting support To see more details about loading failure try /pythoncore.load Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Daniel Neves
Language-Team: Portuguese (Brazil) (http://www.transifex.com/kvirc/KVIrc/language/pt_BR/)
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Erro interno: Interpretador Python não iniciado Erro de execução python: O módulo pythoncore não pôde ser carregado: Suporte ao Python não disponível O módulo pythoncore falhou ao executar o código: algo está errado com o suporte a Python O executável do KVirc foi compilado sem o suporte a scripts em Python Para ver mais detalhes acerca da falha do carregamento, tente /pythoncore.load 