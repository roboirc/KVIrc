��          T      �       �   $   �      �      �   :     :   I  L   �  h  �  +   :     f  "   �  F   �  J   �  9   6                                        Choosing media player interface "%Q" Function not implemented Last interface error:  No mediaplayer interface selected. Try /mediaplayer.detect Seems that there is no usable media player on this machine The selected media player interface failed to execute the requested function Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Robert Förster <Dessa@gmake.de>
Language-Team: Finnish (http://www.transifex.com/kvirc/KVIrc/language/fi/)
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Valitaan mediasoitin käyttöliittymä "%Q" Funktiota ei ole kehitetty Viimeisin käyttöliittymävirhe:  Ei mediasoitin-käyttöliittymää valittu. Yritä /mediaplayer.detect Näyttää siltä ettei tässä koneessa ole käytettävää mediasoitinta Valittu mediasoitin ei voinut ajaa pyydettyä tehtävää 