��          �   %   �      P     Q     c     {     �  	   �  
   �  /   �  &   �     �       )   3     ]     w     �     �     �     �     �     �  
   �     �     �     �     �       r       {     �     �     �  	   �     �  O   �     )  %   ?  $   e  )   �     �     �     �     �  0        6     9     A  	   O     Y     b     n     �     �                                                                	             
                                                &Clear Terminated &Copy Path to Clipboard &Delete File &Open &Other... Clear &All Clear all transfers, including any in progress? Confirm Clearing All Transfers - KVIrc Confirm File Delete - KVIrc Deleting File Failed - KVIrc Do you really want to delete the file %1? Failed to remove the file File Transfers Information Local &File MS-DOS Prompt at Location No OK Open &Location Open &With Progress Size: %1 Terminal at Location Type Yes Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-01-11 02:04+0000
Last-Translator: David Martí <neikokz+transifex@gmail.com>
Language-Team: Spanish (http://www.transifex.com/kvirc/KVIrc/language/es/)
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 B&orrar terminadas &Copiar ruta al portapapeles &Borrar archivo Abrir &Otros... Borrar &todas ¿Borrar todas las transferencias, incluyendo cualquiera que esté en progreso? Confirmación - KVIrc Confirmar borrado del archivo - KVIrc Error al eliminar el archivo - KVIrc ¿Realmente quieres borrar el archivo %1? No pude borrar el archivo Transferencias de archivos Información &Archivo local Intérprete de comandos MS-DOS en esta posición No Aceptar Abrir destino Abrir con Progreso Tamaño: %1 Terminal en destino Tipo Sí 