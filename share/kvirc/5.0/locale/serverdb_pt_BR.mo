��          �      ,      �  $   �     �  #   �  $   �  "     !   8  #   Z  "   ~  $   �  .   �  -   �  *   #  0   N  ,     -   �  '   �  e       h     �     �  #   �     �          #  #   B  $   f  .   �  2   �  +   �  ;     1   U  2   �  '   �                                         
                           	          The network specified already exists The specified IP is not valid The specified network doesn't exist The specified protocol doesn't exist The specified proxy already exists The specified proxy doesn't exist The specified server already exists The specified server doesn't exist You must provide the IP as parameter You must provide the network name as parameter You must provide the port number as parameter You must provide the protocol as parameter You must provide the proxy hostname as parameter You must provide the proxy name as parameter You must provide the server name as parameter You must provide the value as parameter Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Daniel Neves
Language-Team: Portuguese (Brazil) (http://www.transifex.com/kvirc/KVIrc/language/pt_BR/)
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 A rede indicada já existe O IP indicado não é válido A rede especificada não existe O protocole específico não existe O 'proxy' indicado já existe O proxy específico não existe O servidor indicado já existe O servidor especificado não existe Tem que indicar o IP como parâmetro Tem que indicar o nome da rede como parâmetro Tem que indicar o número do porto como parâmetro Tem que indicar o protocolo como parâmetro Tem que indicar o nome do endereço 'proxy' como parâmetro Tem que indicar o nome do 'proxy' como parâmetro Tem que indicar o nome do servidor como parâmetro Tem que indicar o valor como parâmetro 