��          �      |      �     �            	   !  
   +  /   6  )   f     �     �     �     �     �     �     �     �  
                        2     7  i  ;     �     �  	   �     �     �  ?   �  (   8     a     �     �     �     �     �     �     �     �     �     �     �                                                             
                                       	       &Clear Terminated &Copy Path to Clipboard &Open &Other... Clear &All Clear all transfers, including any in progress? Do you really want to delete the file %1? Failed to remove the file File Transfers Information Local &File MS-DOS Prompt at Location No OK Open &Location Open &With Progress Size: %1 Terminal at Location Type Yes Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: ctrlaltca <ctrlaltca@gmail.com>
Language-Team: Esperanto (http://www.transifex.com/kvirc/KVIrc/language/eo/)
Language: eo
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 &Malplenigu Finigitan &Kopii Padon al Tondejo &Malfermi &Alia... Malplenigi Ĉiujn Malplenigi ĉiujn transirigojn, inkluzivante iujn funkciantajn? Ĉu vi vere volas forigi la dosieron %1? Malsukcesis forigi la dosieron Dosieraj Transirigoj Informo Loka &Dosiero MS-DOS Invito ĉe Loko Ne Bone Malfermi &Lokon Malfermi &Kun Progreso Grandeco: %1 Konzolo ĉe Loko Tipo Jes 