��          T      �       �   0   �      �   ?      X   @  F   �  <   �  e    .   �     �  L   �  X     ?   q  L   �                                        Internal error: Perl interpreter not initialized Perl execution error: The perlcore module can't be loaded: Perl support not available The perlcore module failed to execute the code: something is wrong with the Perl support This KVIrc executable has been compiled without Perl scripting support To see more details about loading failure try /perlcore.load Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Daniel Neves
Language-Team: Portuguese (Brazil) (http://www.transifex.com/kvirc/KVIrc/language/pt_BR/)
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Erro interno: Interpretador Perl não iniciado Erro de execução perl: O módulo perlcore não pôde ser carregado: Suporte a Perl não disponível O módulo perlcore falhou ao executar o código: algo está errado com o suporte ao Perl Este executável KVirc foi compilado sem suporte a scripts Perl Para ver mais detalhes acerca da falha do carregamento, tente /perlcore.load 