��          �      �       0  $   1     V  "   t  #   �  $   �  .   �  -     *   =  0   h  ,   �  -   �  '   �  i       �     �  !   �  !   �  %   	  .   /  1   ^  -   �  7   �  1   �  1   (  )   Z                                    
         	             The network specified already exists The specified IP is not valid The specified proxy already exists The specified server already exists You must provide the IP as parameter You must provide the network name as parameter You must provide the port number as parameter You must provide the protocol as parameter You must provide the proxy hostname as parameter You must provide the proxy name as parameter You must provide the server name as parameter You must provide the value as parameter Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: ctrlaltca <ctrlaltca@gmail.com>
Language-Team: Esperanto (http://www.transifex.com/kvirc/KVIrc/language/eo/)
Language: eo
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 La reto specifita jam ekzistas La specifita IP ne estas valida La specifita prokuro jam ekzistas La specifita servilo jam ekzistas Vi devas provizi la IP kiel parametro Vi devas provizi la retan nomon kiel parametro Vi devas provizi la pordan numeron kiel parametro Vi devas provizi la protokolon kiel parametro Vi devas provizi la prokuran retnodnomon kiel parametro Vi devas provizi la prokuran nomon kiel parametro Vi devas provizi la servilan nomon kiel parametro Vi devas provizi la valuon kiel parametro 