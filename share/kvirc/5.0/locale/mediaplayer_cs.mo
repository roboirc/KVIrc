��          L      |       �   $   �      �   :   �   :      L   [  �  �  +   *     V  J   r  L   �  T   
                                         Choosing media player interface "%Q" Last interface error:  No mediaplayer interface selected. Try /mediaplayer.detect Seems that there is no usable media player on this machine The selected media player interface failed to execute the requested function Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Robert Förster <Dessa@gmake.de>
Language-Team: Czech (http://www.transifex.com/kvirc/KVIrc/language/cs/)
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Zvolen interface přehrávače médií "%Q" Poslední chyba interface:  Nebyl vybrán interface k prehrávači médií. Zkuste /mediaplayer.detect Vypadá to jako, že není použitelný přehrávač médií na tomto stroji Zvolený interface přehrávače médií selhal při spuštění požadované funkce 