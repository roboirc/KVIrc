��          �      |      �     �            	   !  
   +  /   6  )   f     �     �     �     �     �     �     �     �  
                        2     7  �  ;  (   -  9   V     �     �     �  U   �  I      ,   j     �     �     �  Q   �     9     @     E     `     w     �  B   �     �     �                                                   
                                       	       &Clear Terminated &Copy Path to Clipboard &Open &Other... Clear &All Clear all transfers, including any in progress? Do you really want to delete the file %1? Failed to remove the file File Transfers Information Local &File MS-DOS Prompt at Location No OK Open &Location Open &With Progress Size: %1 Terminal at Location Type Yes Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: ctrlaltca <ctrlaltca@gmail.com>
Language-Team: Russian (http://www.transifex.com/kvirc/KVIrc/language/ru/)
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 Очистить &завершённые &Копировать путь в буфер обмена &Открыть &Прочее... Очистить &все Очистить все передачи, включая не завершённые? Вы действительно хотите удалить файл %1 ? Невозможно удалить файл Передачи файлов Информация Локальный &файл Открыть командную строку в папке назначения Нет Ок Открыть &папку Открыть &как Прогресс Размер: %1 Открыть терминал в папке назначения Тип Да 