��          �   %   �      P     Q     c     {     �  	   �  
   �  /   �  &   �     �       )   3     ]     w     �     �     �     �     �     �  
   �     �     �     �     �       e       n  4   �     �     �  	   �     �  ?   �  5   %  &   [      �  )   �     �     �                      A     F     I  
   ^  	   i     s          �     �                                                                	             
                                                &Clear Terminated &Copy Path to Clipboard &Delete File &Open &Other... Clear &All Clear all transfers, including any in progress? Confirm Clearing All Transfers - KVIrc Confirm File Delete - KVIrc Deleting File Failed - KVIrc Do you really want to delete the file %1? Failed to remove the file File Transfers Information Local &File MS-DOS Prompt at Location No OK Open &Location Open &With Progress Size: %1 Terminal at Location Type Yes Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Daniel Neves
Language-Team: Portuguese (Brazil) (http://www.transifex.com/kvirc/KVIrc/language/pt_BR/)
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 &Limpar Terminados &Copiar Localização para a Área de Transferência &Apagar Arquivo &Abrir &Outro... Limpar &Todos Limpar todas as transferências, incluindo alguma em progresso? Confirmar Limpeza de Todas as Transferências - KVIrc Confirmar Exclusão de Arquivo - KVIrc Falha ao Excluir Arquivo - KVIrc Você realmente quer apagar o arquivo %1? Falha ao remover o arquivo Transferências de Arquivos Informação &Arquivo Local Terminal MS-DOS na Localização Não OK Abrir &Localização Abrir &Com Progresso Tamanho: %1 Terminal na Localização Tipo Sim 