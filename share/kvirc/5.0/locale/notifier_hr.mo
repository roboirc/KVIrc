��          �      �       H     I     P  
   Y  
   d  	   o     y     �  &   �  *   �  .   �  #        +      D  �  e            	   %  	   /     9  
   B     M  $   T  &   y  =   �     �     �                 	                                       
               1 Hour 1 Minute 15 Minutes 30 Minutes 5 Minutes Disable Hide Permanently (Until Explicitly Enabled) The -t switch expects a timeout in seconds The specified timeout is not valid, assuming 0 The specified window does not exist Until KVIrc is Restarted Write text or commands to window Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: ctrlaltca <ctrlaltca@gmail.com>
Language-Team: Croatian (http://www.transifex.com/kvirc/KVIrc/language/hr/)
Language: hr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
 1 Sat 1 Minuta 15 Minuta 30 Minuta 5 Minuta Onemogući Sakrij Trajno (Do Izričitog Omogućavanja) -t shitch očekuje timeout u sekundama Navedeno vremensko ograničenje nije ispravno, postavljanje 0 Navedeni prozor ne postoji Do Restarta KVIrc-a Piši tekst ili naredbu u prozor 