��          t      �         &        8     W     n  9   �  <   �  N   �     H  .   W     �  f  �  "   
  #   -     Q     d  ;   �  G   �  N        U  -   d  *   �              	   
                                           Choosing torrent client interface "%Q" Invalid client interface "%Q"! Last interface error:  No client selected! No torrent client interface selected. Try /torrent.detect Seems that there is no usable torrent client on this machine The selected torrent client interface failed to execute the requested function Torrent Client Trying torrent client interface "%Q": score %d Using client interface "%Q". Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Robert Förster <Dessa@gmake.de>
Language-Team: French (http://www.transifex.com/kvirc/KVIrc/language/fr/)
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 On choisit l'interface client "%Q" Interface du client "%Q" invalide ! Dernière erreur : Pas de client sélectionné ! Pas de client torrent selectionné. Essayer /torrent.detect Il semble ne pas y avoir de client torrent utilisable sur cette machine Le client torrent selectionné à échoué lors de l'exécution de la requête Client torrent Test de l'interface du client "%Q" : score %d Utilisation de l'interface du client "%Q". 