��          �   %   �      P  A   Q  O   �  P   �  C   4  J   x  �   �  �   S  `   �  �   M  e     V   �    �  K   �  "   )     L  "   g  $   �     �     �  :   �  K   	  :   f	  U   �	  L   �	  ,   D
  g  q
  R   �  `   ,  a   �  T   �  [   D  �   �  �   C  q     �   }  v   _  g   �    >  \   R  )   �  &   �  +         ,     J     g  O   �  J   �  @   !  Y   b  P   �  )                                                         
                                                               	        An interface for Amarok2.
Download it from http://amarok.kde.org
 An interface for BMPx.
Download it from http://sourceforge.net/projects/beepmp
 An interface for Clementine.
Download it from http://www.clementine-player.org/
 An interface for Qmmp.
Download it from http://qmmp.ylsoftware.com
 An interface for Totem.
Download it from http://projects.gnome.org/totem/
 An interface for VLC.
Download it from http://www.videolan.org/
You need to manually enable the D-Bus control
interface in the VLC preferences
 An interface for the AMIP plugin.
You can download it from http://amip.tools-for.net
To use this interface you must install AMIP plugin for your player. An interface for the Audacious media player.
Download it from http://audacious-media-player.org
 An interface for the Mozilla Songbird media player.
Download it from http://www.getsongbird.com.
To use it you have to install also the MPRIS addon available at http://addons.songbirdnest.com/addon/1626.
 An interface for the UNIX Audacious media player.
Download it from http://audacious-media-player.org
 An interface for the UNIX XMMS media player.
Download it from http://legacy.xmms2.org
 An interface for the Winamp media player.
You can download it from http://www.winamp.com.
To use all the features of this interface you must copy the gen_kvirc.dll plugin found in the KVIrc distribution directory to the Winamp plugins folder and restart winamp. An interface for the XMMS2 media player.
Download it from http://xmms2.org
 Can't find a running Winamp window Can't find symbol %1 in %2 Can't load the player library (%1) Choosing media player interface "%Q" Function not implemented Last interface error:  No mediaplayer interface selected. Try /mediaplayer.detect Not sure about the results, trying a second, more aggressive detection pass Seems that there is no usable media player on this machine The Winamp plugin has not been installed properly. Check /help mediaplayer.nowplaying The selected media player interface failed to execute the requested function Trying media player interface "%1": score %2 Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Robert Förster <Dessa@gmake.de>
Language-Team: German (http://www.transifex.com/kvirc/KVIrc/language/de/)
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Eine Schnittstelle für Amarok2.
Laden Sie es von http://amarok.kde.org herunter.
 Eine Schnittstelle für BMPx.
Laden Sie es von http://sourceforge.net/projects/beepmp herunter.
 Eine Schnittstelle für Clementine.
Laden Sie es von http://www.clementine-player.org/ herunter.
 Eine Schnittstelle für Qmmp.
Laden Sie es von http://qmmp.ylsoftware.com herunter.
 Eine Schnittstelle für Totem.
Laden Sie es von http://projects.gnome.org/totem/ herunter.
 Eine Schnittstelle für VLC.
Laden Sie es von http://www.videolan.org/ herunter.
Sie müssen die D-Bus-Schnittstelle manuell in den
VLC-Einstellungen aktivieren.
 Eine Schnittstelle für das AMIP-Plugin.
Sie können es von http://amip.tools-for.net herunterladen.
Um diese Schnittstelle zu nutzen, muss das AMIP-Plugin für Ihren Media-Player installiert werden. Eine Schnittstelle für den Audacious Media-Player.
Laden Sie es von http://audacious-media-player.org herunter.
 Eine Schnittstelle für den Mozilla Songbird Media-Player.
Laden Sie es von http://www.getsongbird.com herunter.
Um sie zu nutzen, müssen Sie auch das MPRIS-Addon von http://addons.songbirdnest.com/addon/1626 installieren.

 Eine Schnittstelle für den UNIX Audacious Media-Player.
Laden Sie es von http://audacious-media-player.org herunter.
 Eine Schnittstelle für den UNIX XMMS Media-Player.
Laden Sie es von http://legacy.xmms2.org herunter.
 Eine Schnittstelle für den Winamp Media-Player.
Laden Sie es von http://www.winamp.com herunter.
Um alle Funktionen dieser Schnittstelle zu nutzen, müssen Sie das gen_kvirc.dll-Plugin aus ihrer KVIrc-Distribution in den Winamp-Plugin-Ordner kopieren und Winamp neu starten. Eine Schnittstelle für den XMMS2 Media-Player.
Laden Sie es von http://xmms2.org herunter.
 Kann kein laufendes Winamp-Fenster finden Kann das Symbol %1 nicht in %2 finden. Kann die Player-Bibliothek (%1) nicht laden Benutze Schnittstelle »%Q«. Funktion nicht implementiert Letzter Schnittstellenfehler:  Keine Mediaplayer Schnittstelle ausgewählt. Probieren Sie /mediaplayer.detect. Ergebnisse unklar; Versuche einen zweiten, agressiveren Erkennungsdurchauf Es scheint kein nutzbarer Mediaplayer auf diesem System zu sein. Das Winamp-Plugin wurde nicht korrekt installiert. Lesen Sie /help mediaplayer.nowplaying Die ausgewählte Mediaplayer Schnittstelle konnte die Funktion nicht ausführen. Probiere Schnittstelle »%1«: %2 Punkte. 