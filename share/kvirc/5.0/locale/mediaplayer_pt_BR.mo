��          �   %   �      P  A   Q  O   �  P   �  C   4  J   x  �   �  �   S  `   �  �   M  e     V   �    �  K   �  "   )     L  "   g  $   �     �     �  :   �  K   	  :   f	  U   �	  L   �	  ,   D
  y  q
  =   �  K   )  L   u  ?   �  F     �   I  �   �  Z   r  �   �  ^   �  N   �    F  E   T  ?   �  &   �  7     1   9     k     �  P   �  Q   �  I   E  W   �  W   �  7   ?                                                      
                                                               	        An interface for Amarok2.
Download it from http://amarok.kde.org
 An interface for BMPx.
Download it from http://sourceforge.net/projects/beepmp
 An interface for Clementine.
Download it from http://www.clementine-player.org/
 An interface for Qmmp.
Download it from http://qmmp.ylsoftware.com
 An interface for Totem.
Download it from http://projects.gnome.org/totem/
 An interface for VLC.
Download it from http://www.videolan.org/
You need to manually enable the D-Bus control
interface in the VLC preferences
 An interface for the AMIP plugin.
You can download it from http://amip.tools-for.net
To use this interface you must install AMIP plugin for your player. An interface for the Audacious media player.
Download it from http://audacious-media-player.org
 An interface for the Mozilla Songbird media player.
Download it from http://www.getsongbird.com.
To use it you have to install also the MPRIS addon available at http://addons.songbirdnest.com/addon/1626.
 An interface for the UNIX Audacious media player.
Download it from http://audacious-media-player.org
 An interface for the UNIX XMMS media player.
Download it from http://legacy.xmms2.org
 An interface for the Winamp media player.
You can download it from http://www.winamp.com.
To use all the features of this interface you must copy the gen_kvirc.dll plugin found in the KVIrc distribution directory to the Winamp plugins folder and restart winamp. An interface for the XMMS2 media player.
Download it from http://xmms2.org
 Can't find a running Winamp window Can't find symbol %1 in %2 Can't load the player library (%1) Choosing media player interface "%Q" Function not implemented Last interface error:  No mediaplayer interface selected. Try /mediaplayer.detect Not sure about the results, trying a second, more aggressive detection pass Seems that there is no usable media player on this machine The Winamp plugin has not been installed properly. Check /help mediaplayer.nowplaying The selected media player interface failed to execute the requested function Trying media player interface "%1": score %2 Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Robert Förster <Dessa@gmake.de>
Language-Team: Portuguese (Brazil) (http://www.transifex.com/kvirc/KVIrc/language/pt_BR/)
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Uma interface para Amarok2.
Baixe-o em http://amarok.kde.org
 Uma interface para BMPx.
Baixe-o em http://sourceforge.net/projects/beepmp
 Uma interface para Clementine.
Baixe-o em http://www.clementine-player.org/
 Uma interface para Qmmp.
Baixe-o em http://qmmp.ylsoftware.com
 Uma interface para Totem.
Baixe-o em http://projects.gnome.org/totem/
 Uma interface para VLC.
Baixe-o em http://www.videolan.org/
Você precisa habilitar manualmente o controle D-Bus
nas preferências do VLC
 Uma interface para o plugin AMIP.
Você pode baixá-lo em http://amip.tools-for.net
Para usar esta interface você deve instalar o plugin AMIP no seu player. Uma interface para o Audacious media player.
Baixe-o em http://audacious-media-player.org
 Uma interface para Mozilla Songbird media player.
Baixe-o em http://www.getsongbird.com.
Para usá-lo, você tem instalar também o addon MPRIS disponível em http://addons.songbirdnest.com/addon/1626.
 Uma interface para UNIX Audacious media player.
Baixe-o em  http://audacious-media-player.org
 Uma interface para UNIX XMMS media player.
Baixe-o em http://legacy.xmms2.org
 Uma interface para o Winamp media player.
Baixe-o em http://www.winamp.com.
Para usar todos os recursos dessa interface, você deve copiar o plugin gen_kvirc.dll encontrado no diretório da distribuição do KVIrc para o pasta de plugins do Winamp e reiniciar o winamp. Uma interface para o XMMS2 media player.
Baixe-o em http://xmms2.org
 Não foi possível encontrar uma janela do Winamp em execução Não pôde encontrar símbolo %1 em %2 Não foi possível carregar a biblioteca do player (%1) A escolher interface de reprodutor de Mídia "%Q" Função não implementada Erro do último interface:  Nenhum interface de reprodutor de Mídia seleccionado. Tente /midiaplayer.detect Resultados inconclusivos, tentando um segundo, passo de detecção mais agressivo Parece que não existe um reprodutor de Mídia utilizável nesta máquina O plugin do Winamp não foi instalado corretamente. Cheque /help mediaplayer.nowplaying O interface de reprodução da Mídia seleccionada falhou ao executar a função pedida Tentando media interface do player "%1": avaliação %2 