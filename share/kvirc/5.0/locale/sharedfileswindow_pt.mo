��          �   %   �      P     Q  
   Y     d     j     n  
   u     �  
   �     �     �  #   �     �     �     �     �  7        <     ?     G     S     `  C   ~  0   �     �  
     j       ~     �     �     �     �  
   �     �     �     �  %   �  -        @     I     N  !   T  O   v     �     �     �     �      �  O     =   k     �     �                                              
              	                                                                 &Add... &Browse... &Edit &OK Cancel Expire at: Expires File path: Filename Invalid timeout, ignoring Invalid visible name: using default Mask Name Never No active file sharedfile No sharedfile with visible name '%Q' and user mask '%Q' OK Re&move Share name: Shared Files The file '%Q' is not readable The file doesn't exist or it is not readable, please check the path The share name can't be empty, please correct it Total: %d sharedfile User mask: Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: ctrlaltca <ctrlaltca@gmail.com>
Language-Team: Portuguese (http://www.transifex.com/kvirc/KVIrc/language/pt/)
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 &Adicionar... &Procurar... &Editar &OK Cancelar Expira em: Expira Localização do ficheiro: Nome do ficheiro Intervalo de tempo inválido, ignorar Nome visível inválido: a usar o predefinido Máscara Nome Nunca Nenhum ficheiro partilhado activo Nenhum ficheiro partilhado com nome visível '%Q' e máscara de utilizador '%Q' OK Re&mover Nome da partilha: Ficheiros Partilhados O ficheiro '%Q' não é legível O ficheiro não existe ou não é legível, por favor verifique a localização O nome da partilha não pode estar vazio, por favor corrija-o Total: %d ficheiros partilhados Máscara de utilizador: 