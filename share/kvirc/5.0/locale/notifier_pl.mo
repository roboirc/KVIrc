��          �            h     i     p  
   y  
   �  	   �     �     �  &   �     �  *   �  .     #   <     `      y     �  
   �  	   �     �     �     �     �     �  /   �       7   6  8   n     �     �  !   �                     
                                                       	    1 Hour 1 Minute 15 Minutes 30 Minutes 5 Minutes Disable Hide Permanently (Until Explicitly Enabled) Show/Hide input line The -t switch expects a timeout in seconds The specified timeout is not valid, assuming 0 The specified window does not exist Until KVIrc is Restarted Write text or commands to window Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Sölve Svartskogen <solaizen@gmail.com>
Language-Team: Polish (http://www.transifex.com/kvirc/KVIrc/language/pl/)
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);
 1 godzinę 1 minutę 15 minut 30 minut 5 minut Wyłącz na Ukryj powiadamiacz Zawsze (dopóki nie zostanie jawnie włączony) Pokaż/Ukryj linię wejścia Przełącznik -t spodziewa sie limitu czasu w sekundach Określony limit czasu nie jest poprawny, przyjmowanie 0 Określone okno nie istnieje Do ponownego uruchomienia KVIrc Wpisz tekst lub polecenia do okna 