��    #      4  /   L           	  
             "     &     -  
   H     S  )   [  
   �     �     �     �     �     �  #   �     "     '     0     5     ;  7   U     �  %   �     �     �  !   �     �  �   �     z  C   �  0   �       
   "     -  	   .     8     H     P     T  '   [  	   �     �  (   �     �     �     �  &   �  (   	  "   @	  0   c	     �	  	   �	     �	     �	  "   �	  L   �	      
  6   #
     Z
     a
  *   w
     �
  t   �
     +  K   J  <   �  /   �                                     #                          !   
                               "                                                             	    &Add... &Browse... &Edit &OK Cancel Error Opening File - KVIrc Expire at: Expires Expires in %d hours %d minutes %d seconds File path: File: %s (%u bytes) Filename Invalid Expiry Time - KVIrc Invalid Share Name - KVIrc Invalid timeout, ignoring Invalid visible name: using default Mask Mask: %s Name Never No active file sharedfile No sharedfile with visible name '%Q' and user mask '%Q' OK Oops! Failed to add the sharedfile... Re&move Share name: Shared File Configuration - KVIrc Shared Files The expiry date/time is in the past: please either remove the "Expires at"check mark or specify a expiry date/time in the future The file '%Q' is not readable The file doesn't exist or it is not readable, please check the path The share name can't be empty, please correct it Total: %d sharedfile User mask: Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Sölve Svartskogen <solaizen@gmail.com>
Language-Team: Polish (http://www.transifex.com/kvirc/KVIrc/language/pl/)
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);
 &Dodaj... &Przeglądaj... &Edytuj &OK Anuluj Błąd podczas otwierania pliku - KVirc Wygasa w: Wygasa Wygasa za %d godzin, %d minut, %d sekund Ścieżka pliku: Plik: %s (%u bajtów) Nazwa pliku Niepoprawny czas wygaśnięcia - KVIrc Niepoprawna nazwa udostępniania - KVIrc Niepoprawny limit czasu, ignoruję Niepoprawna widoczna nazwa: używanie domyślnej Maska Maska: %s Nazwa Nigdy Nie udostępniasz żadnych plików Brak udostepnianego pliku z widoczną nazwą '%Q' i maską użytkownika '%Q' OK Ups! Nie udało się dodać współdzielonego pliku... Us&uń Nazwa udostępniania: Konfiguracja udostępnianego pliku - KVIrc Udostępniane pliki Data/czas wygaśnięcia już minął: proszę, odznacz przycisk wyboru "wygasa" lub określ datę/czas wygaśnięcia Plik '%Q' jest nieodczytywalny Plik nie istnieje lub nie da się go odczytać, proszę sprawdź ścieżkę Nazwa udostępniania nie może być pusta, proszę popraw to Sumarycznie: %d udostępniany(e/ch) plik(i/ów) Maska użytkownika: 