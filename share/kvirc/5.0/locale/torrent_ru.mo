��          t      �         &        8     W     n  9   �  <   �  N   �     H  .   W     �  �  �  ;   �  8   �  6        B  a   b  �   �  �   P     �  E   �  <   /              	   
                                           Choosing torrent client interface "%Q" Invalid client interface "%Q"! Last interface error:  No client selected! No torrent client interface selected. Try /torrent.detect Seems that there is no usable torrent client on this machine The selected torrent client interface failed to execute the requested function Torrent Client Trying torrent client interface "%Q": score %d Using client interface "%Q". Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Robert Förster <Dessa@gmake.de>
Language-Team: Russian (http://www.transifex.com/kvirc/KVIrc/language/ru/)
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 Выбран интерфейс torrent-клиента "%Q" Неверный интерфейс клиента "%Q"! Последняя ошибка интерфейса:  Не выбран клиент! Не выбран интерфейс torrent-клиента. Попробуйте /torrent.detect На данной машине не найдено torrent-клиента, который можно было бы использовать Выбранный интерфейс torrent-клиента не смог выполнить запрошенную функцию Клиент torrent Пробую интерфейс torrent-клиента "%Q": score %d Выбран интерфейс torrent-клиента "%Q". 