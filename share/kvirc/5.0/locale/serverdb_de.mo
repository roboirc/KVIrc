��          �      ,      �  $   �     �  #   �  $   �  "     !   8  #   Z  "   ~  $   �  .   �  -   �  *   #  0   N  ,     -   �  '   �  g    )   j  #   �  '   �  (   �  &   	  $   0  '   U  %   }  *   �  4   �  2     1   6  2   h  2   �  2   �  ,                                            
                           	          The network specified already exists The specified IP is not valid The specified network doesn't exist The specified protocol doesn't exist The specified proxy already exists The specified proxy doesn't exist The specified server already exists The specified server doesn't exist You must provide the IP as parameter You must provide the network name as parameter You must provide the port number as parameter You must provide the protocol as parameter You must provide the proxy hostname as parameter You must provide the proxy name as parameter You must provide the server name as parameter You must provide the value as parameter Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Robert Förster <Dessa@gmake.de>
Language-Team: German (http://www.transifex.com/kvirc/KVIrc/language/de/)
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Das angegebene Netzwerk existiert bereits Die angegebene IP ist nicht gültig Das angegebene Netzwerk existiert nicht Das angegebene Protokoll existiert nicht Der angegebene Proxy existiert bereits Der angegebene Proxy existiert nicht Der angegebene Server existiert bereits Der angegebene Server existiert nicht Die IP muss als Parameter angegeben werden Der Netzwerkname muss als Parameter angegeben werden Die Portnummer muss als Parameter angegeben werden Das Protokoll muss als Parameter angegeben werden Der Proxy-Name muss als Parameter angegeben werden Der Proxy-Name muss als Parameter angegeben werden Der Servername muss als Parameter angegeben werden Der Wert muss als Parameter angegeben werden 