��          �   %   �      @     A     G     S  
   `     k  
   y     �     �     �     �     �     �     �  +   �            x        �  
   �     �     �     �     �     �  e  �     W     ]     i     v     �     �     �     �     �     �     �          +  2   4     g     w  �   �               '     ;     V     f     v                                                                                  
   	                                         About About KVIrc Architecture Build Info Build command Build date Build flags CPU name Close Compiler flags Compiler name Executable Information Features Forged by the <b>KVIrc Development Team</b> Honor && Glory License Oops! Can't find the license file.
It MUST be included in the distribution...
Please report to <pragma at kvirc dot net> Qt theme Qt version Revision number Runtime Info Sources date System name System version Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Daniel Neves
Language-Team: Portuguese (Brazil) (http://www.transifex.com/kvirc/KVIrc/language/pt_BR/)
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Sobre Sobre KVIrc Arquitectura Informação de Compilação Comando de criação Data da criação Bandeiras da criação Nome da CPU Fechar Bandeiras do compilador Nome do compilador Informação do Executável Recursos Criado pela <b>Equipe de Desenvolvimento KVIrc</b> Honra e Glória Licença Oops! Não foi encontrado o arquivo de licença.
Isso DEVE ser incluído na distribuição...
Por favor, reportar para <pragma at kvirc dot net> Tema QT Versão do QT Número da revisão Informação de execução Data das fontes Nome do sistema Versão do sistema 