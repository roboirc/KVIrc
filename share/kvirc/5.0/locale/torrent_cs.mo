��    
      l      �       �   &   �           7     N  9   b  <   �  N   �  .   (     W  �  t  (   �  !        A     \  >   q  F   �  L   �  3   D  #   x                                
      	       Choosing torrent client interface "%Q" Invalid client interface "%Q"! Last interface error:  No client selected! No torrent client interface selected. Try /torrent.detect Seems that there is no usable torrent client on this machine The selected torrent client interface failed to execute the requested function Trying torrent client interface "%Q": score %d Using client interface "%Q". Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Robert Förster <Dessa@gmake.de>
Language-Team: Czech (http://www.transifex.com/kvirc/KVIrc/language/cs/)
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Vybírám interface torrent klienta "%Q" Neplatný interface klienta "%Q"! POslední chyba interfacu: Nebyl zvolen klient! Nebyl zvolen interface torrent klienta. Zkuste /torrent.detect Vypadá to, že na tomto počítači není použitelný torrent klient Zvolený interface torrent klienta selhal ve spuštění požadované funkce Zkouším interface torrent klienta "%Q": skóre %d Používám interface klienta "%Q". 