��            )   �      �     �     �     �     �     �     �     �     �  ,   �  &        D     W  '   u     �     �     �  
   �     �     �     �     �     �     �          #     3  '   C     k  j  �     �  '   �          &     ,     ;     C     Q  -   ^  %   �     �      �  .   �          "     *     >     W     m     ~     �  !   �     �     �     �     �  +        H                                                             
                                                            	                 %1 on %2 Can't log to file '%1' Cancel Channel Channel %1 on %2 Console Console on %1 DCC Chat Do you really wish to delete all these logs? Do you really wish to delete this log? Export Log - KVIrc Failed to export the log '%1' Failed to load logview module, aborting Filter Index Log File Log Viewer Log contents mask: Log name mask: Other Query Show DCC chat logs Show channel logs Show console logs Show other logs Show query logs This window has no logging capabilities Window '%1' not found Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: ctrlaltca <ctrlaltca@gmail.com>
Language-Team: Portuguese (http://www.transifex.com/kvirc/KVIrc/language/pt/)
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 %1 em %2 Não é possível gravar o registo '%1' Cancelar Canal Canal %1 em %2 Consola Consola em %1 Conversa DCC Deseja realmente apagar todos estes registos? Deseja realmente apagar este registo? Exportar Registo - KVIrc Falha ao exportar o registo '%1' Falha ao carregar o módulo logview, a abortar Filtro Índice Ficheiro de Registo Visualizador de Registos Filtro de conteúdos: Filtro de nomes: Outro Consulta Mostrar registos de conversas DCC Mostrar registos de canais Mostrar registos de consola Mostrar outros registos Mostrar registos de consultas Esta janela não tem capacidade de registar Janela '%1' não encontrada 