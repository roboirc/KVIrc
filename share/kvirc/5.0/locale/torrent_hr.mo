��    
      l      �       �   &   �           7     N  9   b  <   �  N   �  .   (     W  �  t  +   &  !   R     t     �  C   �  =   �  A   -  7   o     �                                
      	       Choosing torrent client interface "%Q" Invalid client interface "%Q"! Last interface error:  No client selected! No torrent client interface selected. Try /torrent.detect Seems that there is no usable torrent client on this machine The selected torrent client interface failed to execute the requested function Trying torrent client interface "%Q": score %d Using client interface "%Q". Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Robert Förster <Dessa@gmake.de>
Language-Team: Croatian (http://www.transifex.com/kvirc/KVIrc/language/hr/)
Language: hr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
 Odabiranje sučelja za torrent klijent "%Q" Neispravno klijent sučelje "%Q"! Posljednja greška sučelja.  Nema odabranog klijenta! Nema odabranih torrent klijent sučelja. Pokušajte /torrent.detect Čini se da nema korisnih torrent klijenata na ovom računalu Odabrani torrent klijent nije uspio izvršiti zatraženu funkciju Pokušavam torrent sučelje za klijenta "%Q": score %d  Koristim klijent sućelje "%Q". 