��          �      �        A   	  O   K  C   �  J   �  �   *  �   �  `   S  �   �  e   �  V   �  K   >  "   �     �  "   �  $   �          )  :   @  K   {  :   �  U     L   X  �  �  d   �
  r   �
  f   p  m   �  �   E       �   ?  \  �  �   &  �   �  t   i  =   �  '     U   D  =   �  *   �  5     e   9  �   �  _   +  �   �                                  	   
                                                                        An interface for Amarok2.
Download it from http://amarok.kde.org
 An interface for BMPx.
Download it from http://sourceforge.net/projects/beepmp
 An interface for Qmmp.
Download it from http://qmmp.ylsoftware.com
 An interface for Totem.
Download it from http://projects.gnome.org/totem/
 An interface for VLC.
Download it from http://www.videolan.org/
You need to manually enable the D-Bus control
interface in the VLC preferences
 An interface for the AMIP plugin.
You can download it from http://amip.tools-for.net
To use this interface you must install AMIP plugin for your player. An interface for the Audacious media player.
Download it from http://audacious-media-player.org
 An interface for the Mozilla Songbird media player.
Download it from http://www.getsongbird.com.
To use it you have to install also the MPRIS addon available at http://addons.songbirdnest.com/addon/1626.
 An interface for the UNIX Audacious media player.
Download it from http://audacious-media-player.org
 An interface for the UNIX XMMS media player.
Download it from http://legacy.xmms2.org
 An interface for the XMMS2 media player.
Download it from http://xmms2.org
 Can't find a running Winamp window Can't find symbol %1 in %2 Can't load the player library (%1) Choosing media player interface "%Q" Function not implemented Last interface error:  No mediaplayer interface selected. Try /mediaplayer.detect Not sure about the results, trying a second, more aggressive detection pass Seems that there is no usable media player on this machine The Winamp plugin has not been installed properly. Check /help mediaplayer.nowplaying The selected media player interface failed to execute the requested function Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Robert Förster <Dessa@gmake.de>
Language-Team: Russian (http://www.transifex.com/kvirc/KVIrc/language/ru/)
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 Интерфейс к Amarok2.
Вы можете загрузить его с http://amarok.kde.org
 Интерфейс к BMPx.
Вы можете загрузить его с http://sourceforge.net/projects/beepmp
 Интерфейс к Qmmp.
Вы можете загрузить его с http://qmmp.ylsoftware.com
 Интерфейс к Totem.
Вы можете загрузить его с http://projects.gnome.org/totem/
 Интерфейс к VLC.
Вы можете загрузить его с http://www.videolan.org/
Необходимо вручную включить управление через D-Bus
в настройках VLC
 Интерфейс к плагину AMIP.
Вы можете загрузить его с http://amip.tools-for.net
Для использования этого интерфейса вы должны установить плагин AMIP для вашего проигрывателя. Интерфейс к медиаплееру Audacious.
Вы можете загрузить его с http://audacious-media-player.org
 Интерфейс к медиапрогрывателю Mozilla Songbird.
Вы можете загрузить его с http://www.getsongbird.com.
Для использования вам также придётся установить дополнение MPRIS, доступное по адресу http://addons.songbirdnest.com/addon/1626.
 Интерфейс к популярному в UNIX медиаплееру Audacious.
Вы можете загрузить его с http://audacious-media-player.org
 Интерфейс к популярному в UNIX медиаплееру XMMS.
Вы можете загрузить его с http://legacy.xmms2.org
 Интерфейс к медиаплееру XMMS2.
Вы можете загрузить его с http://xmms2.org
 Не могу найти запущенное окно Winamp Не найден символ %1 в %2 Не могу загрузить библиотеку проигрывателя (%1) Выбран интерфейс медиаплеера «%Q» Функция не реализована Последняя ошибка интерфейса: Интерфейс медиаплеера не выбран. Попробуйте /mediaplayer.detect Не уверен в результате, пробую второй, более агрессивный способ обнаружения Кажется, на этой машине нет подходящих медиаплееров Плагин для Winamp не был установлен должным образом. Проверьте /help mediaplayer.nowplaying Выбранный интерфейс медиаплеера не смог выполнить требуемую функцию 