��          T      �       �   2   �      �   C     \   H  H   �  >   �  g  -  7   �     �  V   �  i   ?  F   �  >   �                                        Internal error: Python interpreter not initialized Python execution error: The pythoncore module can't be loaded: Python support not available The pythoncore module failed to execute the code: something is wrong with the Python support This KVIrc executable has been compiled without Python scripting support To see more details about loading failure try /pythoncore.load Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Robert Förster <Dessa@gmake.de>
Language-Team: German (http://www.transifex.com/kvirc/KVIrc/language/de/)
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Interner Fehler: Python-Interpreter nicht initialisiert Python-Ausführungsfehler: Das Pythoncore-Modul kann nicht geladen werden: Python-Unterstützung nicht verfügbar Das Pythoncore-Modul konnte den Code nicht ausführen: Irgendwas ist falsch mit der Python-Unterstützung Dieser KVIrc-Build wurde ohne Python-Scripting-Unterstützung erstellt Für mehr Details über den Lade-Fehler siehe /pythoncore.load 