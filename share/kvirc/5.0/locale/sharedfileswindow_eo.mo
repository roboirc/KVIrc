��          �   %   �      P     Q  
   Y     d     j     n  
   u     �  
   �     �     �  #   �     �     �     �     �  7        <     ?     G     S     `  C   ~  0   �     �  
     i    
   }     �     �     �     �     �     �     �  
   �     �  &         '     -     2  "   9  E   \     �  	   �     �     �     �  I   �  F   =  "   �     �                                              
              	                                                                 &Add... &Browse... &Edit &OK Cancel Expire at: Expires File path: Filename Invalid timeout, ignoring Invalid visible name: using default Mask Name Never No active file sharedfile No sharedfile with visible name '%Q' and user mask '%Q' OK Re&move Share name: Shared Files The file '%Q' is not readable The file doesn't exist or it is not readable, please check the path The share name can't be empty, please correct it Total: %d sharedfile User mask: Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: ctrlaltca <ctrlaltca@gmail.com>
Language-Team: Esperanto (http://www.transifex.com/kvirc/KVIrc/language/eo/)
Language: eo
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 &Aldoni... &Paĝumi... &Redakti &Bone Rezigni Senvalidiĝas je: Senvalidiĝas Dosiera pado: Dosiernomo Nevalida tempolimo, ignoras Nevalida videbla nomo: uzas defaŭlton Masko Nomo Neniam Neniu aktiva dosiero komune uzebla Neniu komune uzebla dosiero kun videbla nomo '%Q' kaj uzantmasko '%Q' Bone For&viŝi Dividi nomon: Komune Uzeblaj Dosieroj La dosiero '%Q' ne legeblas La dosiero ne ekzistas aŭ ĝi estas nelegebla, bonvolu kontroli la padon La komuna dosieruja nomo ne povas esti malplena. Bonvolu ĝustigi ĝin Totalo: %d dosieroj komune uzeblaj Uzantmasko: 