��          �            h     i     p  
   y  
   �  	   �     �     �  &   �     �  *   �  .     #   <     `      y  �  �     �     �     �     �     �     �     �  7   �  1   %  ]   W  V   �  3     5   @  =   v                     
                                                       	    1 Hour 1 Minute 15 Minutes 30 Minutes 5 Minutes Disable Hide Permanently (Until Explicitly Enabled) Show/Hide input line The -t switch expects a timeout in seconds The specified timeout is not valid, assuming 0 The specified window does not exist Until KVIrc is Restarted Write text or commands to window Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: ctrlaltca <ctrlaltca@gmail.com>
Language-Team: Russian (http://www.transifex.com/kvirc/KVIrc/language/ru/)
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 1 Час 1 Минута 15 Минут 30 Минут 5 Минут Выключить Скрыть Навсегда (До явного включения) Показать/Скрыть поле ввода Используйте ключ -t чтобы задать таймаут в секундах Заданный таймаут недействителен, принимается 0 Заданное окно не существует Пока KVIrc не будет перезапущен Напишите текст или команду в окне 