��          T      �       �   0   �      �   ?      X   @  F   �  <   �  g    1   �     �  L   �  U   !  P   w  F   �                                        Internal error: Perl interpreter not initialized Perl execution error: The perlcore module can't be loaded: Perl support not available The perlcore module failed to execute the code: something is wrong with the Perl support This KVIrc executable has been compiled without Perl scripting support To see more details about loading failure try /perlcore.load Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: ctrlaltca <ctrlaltca@gmail.com>
Language-Team: Italian (http://www.transifex.com/kvirc/KVIrc/language/it/)
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Errore interno: interprete Perl non inizializzato Errore nell'esecuzione Perl: Impossibile caricare il module perlcode: il supporto Perl non è disponibile Il modulo perlcore non ha eseguito il codice: quancosa non funziona nel supporto Perl Questo eseguibile KVIrc è stato compilato senza il supporto allo scripting Perl Per altri dettagli sul fallimento del caricamento prova /perlcore.load 