��          �      |      �     �            	   !  
   +  /   6  )   f     �     �     �     �     �     �     �     �  
                        2     7  i  ;     �     �  
   �     �     �  P     +   ]  "   �     �     �     �     �     �     �     �            
   (     3     M     T                                                   
                                       	       &Clear Terminated &Copy Path to Clipboard &Open &Other... Clear &All Clear all transfers, including any in progress? Do you really want to delete the file %1? Failed to remove the file File Transfers Information Local &File MS-DOS Prompt at Location No OK Open &Location Open &With Progress Size: %1 Terminal at Location Type Yes Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-23 21:28+0000
Last-Translator: ctrlaltca <ctrlaltca@gmail.com>
Language-Team: Hungarian (http://www.transifex.com/kvirc/KVIrc/language/hu/)
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Megszakított eltávolítása Útvonal másolása vágólapra Megnyitás Egyéb Minden eltávolítása Minden adatátvitel eltávolítása, beleértve a még folyamatban lévőket is? Tényleg törölni szeretnéd a fájlt: %1? A fájl eltávolítása sikertelen Fájl Átvitelek Információ Helyi Fájl MS-DOS Promt az adott helyen Nem OK Hely megnyitása Megnyitás ezzel... Folyamat Méret: %1 Terminál az adott helyen Típus Igen 