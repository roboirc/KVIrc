��    -      �  =   �      �     �     �     �                 !   .  "   P     s     {     �     �     �  ,   �  &   �               .  '   L     t     {     �     �  
   �     �     �  &   �     �     �               #     )  '   >     f     v     �     �     �     �     �  '   �       5     g  R  	   �     �  +   �  	    	     
	     	     $	     =	     V	     ^	     m	     {	     �	     �	     �	     �	     �	  2   	
  =   <
     z
     �
     �
     �
     �
     �
     �
  /   �
               !     (     ;     K  0   `     �     �     �     �     �  $        7  /   E     u  B   �     
                   %                  -      !      *            "      #                $      (       	                           '       )            &              ,                      +                    %1 on %2 Apply Filter Can't log to file '%1' Cancel Channel Channel %1 on %2 Confirm Current User Log Deletion Confirm Current User Logs Deletion Console Console on %1 Contents Filter DCC Chat DCC Chat with: %1 Do you really wish to delete all these logs? Do you really wish to delete this log? Export Log - KVIrc Export Log File to Failed to export the log '%1' Failed to load logview module, aborting Filter HTML Archive Index Log File Log Viewer Log contents mask: Log name mask: Missing window ID after the 'w' switch Only newer than: Only older than: Other Plain Text File Query Query with: %1 on %2 Remove All Log Files Within This Folder Remove Log File Show DCC chat logs Show channel logs Show console logs Show other logs Show query logs Something on: %1 This window has no logging capabilities Window '%1' not found Window with ID '%1' not found, returning empty string Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Robert Förster <Dessa@gmake.de>
Language-Team: German (http://www.transifex.com/kvirc/KVIrc/language/de/)
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 %1 auf %2 Filter anwenden Kann nicht in Datei ›%1‹ protokollieren Abbrechen Channel Channel %1 auf %2 Bestätige Log-Löschung Bestätige Log-Löschung Konsole Konsole auf %1 Inhaltsfilter DCC Chat DCC-Chat mit: %1 All diese Protokolle entfernen? Dieses Protokoll löschen? Protokoll exportieren – KVIrc Exportiere Logdatei nach Export des Protokolls nach ›%1‹ fehlgeschlagen Laden des Protokollbetrachtermoduls fehlgeschlagen, breche ab Filter HTML-Archiv Index Logdatei Loganzeiger Logdateien-Inhaltsmaske: Logdateienmaske: Fehlende Fenster-ID hinter dem ›w‹-Schalter Nur neuer als: Nur älter als: Andere Einfache Textdatei Anfrage (Query) Query mit: %1 auf %2 Lösche alle Logdateien innerhalb dieses Ordners Lösche Logdatei DCC-Chat-Logdateien anzeigen Channel-Logdateien anzeigen Konsolenlogdateien anzeigen Andere Logdateien anzeigen Anfrage- (Query) Logdateien anzeigen Etwas auf: %1 Dieses Fenster hat keine Protokoll-Fähigkeiten Fenster ›%1‹ nicht gefunden Fenster mit ID ›%1‹ nicht gefunden, gebe leeren String zurück 