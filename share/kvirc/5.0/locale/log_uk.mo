��            )   �      �     �     �     �     �     �     �     �     �  ,   �  &        D     W  '   u     �     �     �  
   �     �     �     �     �     �     �          #     3  '   C     k  �  �  
   5  <   @     }  
   �     �     �     �     �  K   �  D   6  '   {  >   �  Q   �     4     A     N  #   f  '   �  %   �     �     �  (   �  (   	  (   @	  "   i	  *   �	  I   �	  %   
                                                             
                                                            	                 %1 on %2 Can't log to file '%1' Cancel Channel Channel %1 on %2 Console Console on %1 DCC Chat Do you really wish to delete all these logs? Do you really wish to delete this log? Export Log - KVIrc Failed to export the log '%1' Failed to load logview module, aborting Filter Index Log File Log Viewer Log contents mask: Log name mask: Other Query Show DCC chat logs Show channel logs Show console logs Show other logs Show query logs This window has no logging capabilities Window '%1' not found Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: ctrlaltca <ctrlaltca@gmail.com>
Language-Team: Ukrainian (http://www.transifex.com/kvirc/KVIrc/language/uk/)
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 %1 на %2 Не можу писати журнал до файлу '%1' Скасувати Канал Канал %1 на %2 Консоль Консоль на %1 DCC-балачка Ви дійсно хочете видалити всі ці журнали? Ви дійсно хочете видалити цей журнал? Експортую журнал - KVIrc Не вдалось експортувати журнал '%1' Не вдалось завантажити модуль logview, скасовую Фільтр Індекс Файл журналу Переглядач журналу Маска вмісту журналу: Маска назви журналу: Інше Приват Показати логи DCC-чатів Показати логи каналів Показати логи консолі Показати інші логи Показати логи приватів Це вікно не має можливості журналювання Вікно '%1' не знайдене 