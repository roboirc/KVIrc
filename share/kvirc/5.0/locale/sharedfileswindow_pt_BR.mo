��    #      4  /   L           	  
             "     &     -  
   H     S  )   [  
   �     �     �     �     �     �  #   �     "     '     0     5     ;  7   U     �  %   �     �     �  !   �     �  �   �     z  C   �  0   �       
   "  e  -     �     �     �     �     �     �  
   �     �  )   �          9     P  )   `  )   �  %   �  -   �     	     	     	     #	     )	  L   I	     �	  8   �	     �	     �	  1   �	     
  �   4
     �
  N   �
  =   $  !   b     �                                #                          !   
                               "                                                             	    &Add... &Browse... &Edit &OK Cancel Error Opening File - KVIrc Expire at: Expires Expires in %d hours %d minutes %d seconds File path: File: %s (%u bytes) Filename Invalid Expiry Time - KVIrc Invalid Share Name - KVIrc Invalid timeout, ignoring Invalid visible name: using default Mask Mask: %s Name Never No active file sharedfile No sharedfile with visible name '%Q' and user mask '%Q' OK Oops! Failed to add the sharedfile... Re&move Share name: Shared File Configuration - KVIrc Shared Files The expiry date/time is in the past: please either remove the "Expires at"check mark or specify a expiry date/time in the future The file '%Q' is not readable The file doesn't exist or it is not readable, please check the path The share name can't be empty, please correct it Total: %d sharedfile User mask: Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Daniel Neves
Language-Team: Portuguese (Brazil) (http://www.transifex.com/kvirc/KVIrc/language/pt_BR/)
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 &Adicionar... &Procurar... &Editar &OK Cancelar Erro ao Abrir o Arquivo - KVIrc Expira em: Expira Expira em %d horas %d minutos %d segundos Localização do arquivo: Arquivo: %s (%u bytes) Nome do arquivo Inválido Horário de Expiração - KVIrc Inválido Nome do Compartilhamento- KVIrc Intervalo de tempo inválido, ignorar Nome visível inválido: usando o predefinido Máscara Máscara: %s Nome Nunca Nenhum arquivo partilhado ativo Nenhum arquivo partilhado com nome visível '%Q' e máscara de Usuário '%Q' OK Oops! Falha ao adicionar um arquivo para compartilhar... Re&mover Nome da partilha: Configuração de Arquivos Compartilhados - KVIrc Arquivos Partilhados A data/hora da expiração está no passado: por favor, ou remova a marcação em "Expira em" ou indique uma data/hora no futuro O arquivo '%Q' não é legível O arquivo não existe ou não é legível, por favor verifique a localização O nome da partilha não pode estar vazio, por favor corrija-o Total: %d arquivos compartilhados Máscara de Usuário: 