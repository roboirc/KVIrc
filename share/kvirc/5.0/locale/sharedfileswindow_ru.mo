��            )         �     �  
   �     �     �     �  
   �     �  )   �  
             1     :  #   T     x     }     �     �     �  7   �     �  %   �                  �   -     �  C   �  0        A  
   V  �  a     S     h     w     �     �     �     �  G   �          6     Q  R   c  k   �  
   "	     -	     <	     C	  1   R	  r   �	     �	  A   �	     >
     N
     n
  �   �
  %   7  m   ]  ~   �  &   J  $   q                                                                                                            	   
                         &Add... &Browse... &Edit &OK Cancel Expire at: Expires Expires in %d hours %d minutes %d seconds File path: File: %s (%u bytes) Filename Invalid timeout, ignoring Invalid visible name: using default Mask Mask: %s Name Never No active file sharedfile No sharedfile with visible name '%Q' and user mask '%Q' OK Oops! Failed to add the sharedfile... Re&move Share name: Shared Files The expiry date/time is in the past: please either remove the "Expires at"check mark or specify a expiry date/time in the future The file '%Q' is not readable The file doesn't exist or it is not readable, please check the path The share name can't be empty, please correct it Total: %d sharedfile User mask: Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: ctrlaltca <ctrlaltca@gmail.com>
Language-Team: Russian (http://www.transifex.com/kvirc/KVIrc/language/ru/)
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 &Добавить... &Обзор... &Редактировать &Ок Отмена Истекает в: Заканчивается Истекает через %d часов %d минут %d секунд Путь к файлу: Файл: %s (%u байт) Имя файла Недействительное время ожидания, игнорируем Недействительное видимое имя: используется умолчательное Маска Маска: %s Имя Никогда Нет активного общего файла Нет общего файла с видимым именем «%Q» и маской пользователя '%Q' Ок Ой! Не удалось добавить общий файл... &Удалить Имя общего файла: Общие файлы Дата/время окончания в прошлом: пожалуйста, удалите «Истекает в» или укажите дату/время в будущем Файл «%Q» не читается Файл не существует или не читаем, пожалуйста проверьте путь Имя общего файла не может быть пустым, пожалуйста скорректируйте его Всего: %d общих файлов Маска пользователя: 