��          �      �       0  $   1     V  "   t  #   �  $   �  .   �  -     *   =  0   h  ,   �  -   �  '   �  f       �     �     �     �  $   �  ,     2   C  *   v  ,   �  *   �  ,   �  (   &                                    
         	             The network specified already exists The specified IP is not valid The specified proxy already exists The specified server already exists You must provide the IP as parameter You must provide the network name as parameter You must provide the port number as parameter You must provide the protocol as parameter You must provide the proxy hostname as parameter You must provide the proxy name as parameter You must provide the server name as parameter You must provide the value as parameter Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: ctrlaltca <ctrlaltca@gmail.com>
Language-Team: Turkish (http://www.transifex.com/kvirc/KVIrc/language/tr/)
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Belirtilen ağ zaten var Belirtilen IP geçerli değil Belirtilen proxy zaten var Belirtilen sunucu zaten var Parametre olarak IP yazmalısınız. Parametre olarak ağ adını yazmalısınız Paramete olarak port numarasını yazmalısınız. Parametre olarak protokol yazmalısınız. Parametre olarak proxy host yazmalısınız. Parametre olarak proxy ad yazmalısınız. Parametre olarak sunucu adı yazmalısınız Parametre olarak değer yazmalısınız. 