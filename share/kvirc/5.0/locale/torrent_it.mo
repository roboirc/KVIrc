��          t      �         &        8     W     n  9   �  <   �  N   �     H  .   W     �  h  �  +     ,   8     e     u  :   �  A   �  H        V  8   e  ,   �              	   
                                           Choosing torrent client interface "%Q" Invalid client interface "%Q"! Last interface error:  No client selected! No torrent client interface selected. Try /torrent.detect Seems that there is no usable torrent client on this machine The selected torrent client interface failed to execute the requested function Torrent Client Trying torrent client interface "%Q": score %d Using client interface "%Q". Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Robert Förster <Dessa@gmake.de>
Language-Team: Italian (http://www.transifex.com/kvirc/KVIrc/language/it/)
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Scelgo l'interfaccia al client torrent "%Q" Interfaccia al client torrent "%Q" invalida! Ultimo errore:  Nessun client selezionato! Nessuna interfaccia selezionata. Prova con /torrent.detect Sembra che non ci siano client torrent usabili su questa macchina L'interfaccia selezionata ha fallito nell'eseguire la funzione richiesta Client Torrent Provo l'interfaccia al client torrent "%Q": punteggio %d Scelgo l'interfaccia al client torrent "%Q". 