��          t      �         &        8     W     n  9   �  <   �  N   �     H  .   W     �  k  �  .     $   >     c       G   �  D   �  P   )     z  <   �  #   �              	   
                                           Choosing torrent client interface "%Q" Invalid client interface "%Q"! Last interface error:  No client selected! No torrent client interface selected. Try /torrent.detect Seems that there is no usable torrent client on this machine The selected torrent client interface failed to execute the requested function Torrent Client Trying torrent client interface "%Q": score %d Using client interface "%Q". Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Robert Förster <Dessa@gmake.de>
Language-Team: Portuguese (http://www.transifex.com/kvirc/KVIrc/language/pt/)
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 A escolher o interface do cliente torrent "%Q" Interface de cliente inválido "%Q"! Erro do último interface:  Nenhum cliente seleccionado! Nenhum interface de cliente torrent seleccionado. Tente /torrent.detect Parece que não existe um cliente torrent utilizável nesta máquina O interface do cliente torrent seleccionado falhou ao executar a função pedida Cliente Torrent A tentar o interface do cliente torrent "%Q": pontuação %d A usar o interface do cliente "%Q". 