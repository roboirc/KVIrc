��          �            h     i     p  
   y  
   �  	   �     �     �  &   �     �  *   �  .     #   <     `      y  g  �            
     
     	   *     4  
   A  "   L     o  5   �  :   �  &   �     #  %   >                     
                                                       	    1 Hour 1 Minute 15 Minutes 30 Minutes 5 Minutes Disable Hide Permanently (Until Explicitly Enabled) Show/Hide input line The -t switch expects a timeout in seconds The specified timeout is not valid, assuming 0 The specified window does not exist Until KVIrc is Restarted Write text or commands to window Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Robert Förster <Dessa@gmake.de>
Language-Team: German (http://www.transifex.com/kvirc/KVIrc/language/de/)
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 1 Stunde 1 Minute 15 Minuten 30 Minuten 5 Minuten Deaktivieren Ausblenden Permanent (Bis explizit aktiviert) Zeige/Verberge Eingabezeile Der -t-Schalter erwartet einen Zeitablauf in Sekunden Der angegebene Zeitablauf ist ungültig, 0 wird angenommen Das angegebene Fenster existiert nicht Bis KVIrc neugestartet ist Text oder Kommandos zu Fenster senden 