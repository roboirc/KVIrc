��          t      �         &        8     W     n  9   �  <   �  N   �     H  .   W     �  j  �  3     !   B     d     }  I   �  D   �  T   '     |     �  +   �              	   
                                           Choosing torrent client interface "%Q" Invalid client interface "%Q"! Last interface error:  No client selected! No torrent client interface selected. Try /torrent.detect Seems that there is no usable torrent client on this machine The selected torrent client interface failed to execute the requested function Torrent Client Trying torrent client interface "%Q": score %d Using client interface "%Q". Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-23 21:28+0000
Last-Translator: Robert Förster <Dessa@gmake.de>
Language-Team: Hungarian (http://www.transifex.com/kvirc/KVIrc/language/hu/)
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Torrent kliens interfészének kiválasztása: "%Q" Helytelen kliens interfész: "%Q" Utolsó interfész hiba: Nincs kliens kiválasztva! Nincs torrent kliens kiválasztva. Próbáld a /torrent.detect parancsot. Úgy néz ki nincs torrent kliens telepítve ezen a számítógépen A kiválasztott torrent kliens interfész nem tudta végrehajtani a kért műveletet Torrent Kliens  "%Q" tesztelése: %d pont Kliens interfészének kiválasztása: "%Q" 