��          t      �         &        8     W     n  9   �  <   �  N   �     H  .   W     �  �  �  D   X  <   �  4   �  !     h   1  g   �  ~        �  U   �  @   �              	   
                                           Choosing torrent client interface "%Q" Invalid client interface "%Q"! Last interface error:  No client selected! No torrent client interface selected. Try /torrent.detect Seems that there is no usable torrent client on this machine The selected torrent client interface failed to execute the requested function Torrent Client Trying torrent client interface "%Q": score %d Using client interface "%Q". Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Robert Förster <Dessa@gmake.de>
Language-Team: Ukrainian (http://www.transifex.com/kvirc/KVIrc/language/uk/)
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Вибираю інтерфейс торрент-клієнта "%Q" Помилковий інтерфейс клієнта "%Q"! Остання помилка інтерфейсу:  Клієнт не вибрано! Не вибрано інтерфейс торрент-клієнта. Спробуйте /torrent.detect Здається на цій машині немає придатного торрент-клієнта Вибраний інтерфейс торрент-клієнта не зміг виконати вказану функцію Торрент-клієнт Пробую інтерфейс торрент-клієнта "%Q": рахунок %d Використовую інтерфейс клієнта "%Q". 