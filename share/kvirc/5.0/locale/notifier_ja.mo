��          �      �       H     I     P  
   Y  
   d  	   o     y     �     �  *   �  .   �  #   �           2  a  S     �     �     �     �     �     �     �     �  M     P   T  -   �  6   �  <   
                                           
       	               1 Hour 1 Minute 15 Minutes 30 Minutes 5 Minutes Disable Hide Show/Hide input line The -t switch expects a timeout in seconds The specified timeout is not valid, assuming 0 The specified window does not exist Until KVIrc is Restarted Write text or commands to window Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: ctrlaltca <ctrlaltca@gmail.com>
Language-Team: Japanese (http://www.transifex.com/kvirc/KVIrc/language/ja/)
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 1 時間 1 分 15 分 30 分 5 分 無効 隠す 入力行を表示する/隠す スイッチ -t はタイムアウト(単位: 秒)を期待しています。 タイムアウトの指定が正しくありません。0 とみなします。 指定のウィンドウが存在しません KVIrc ユーティリティは再起動しました。 ウィンドウにテキストかコマンドを書き込む 