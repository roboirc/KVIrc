��          �   %   �      P     Q  
   Y     d     j     n  
   u     �  
   �     �     �  #   �     �     �     �     �  7        <     ?     G     S     `  C   ~  0   �     �  
     �       �     �     �     �               -     F     `  0   s  R   �  
   �          
  9     a   Q     �     �     �     �  8     h   ?  m   �     	  "   6	                                              
              	                                                                 &Add... &Browse... &Edit &OK Cancel Expire at: Expires File path: Filename Invalid timeout, ignoring Invalid visible name: using default Mask Name Never No active file sharedfile No sharedfile with visible name '%Q' and user mask '%Q' OK Re&move Share name: Shared Files The file '%Q' is not readable The file doesn't exist or it is not readable, please check the path The share name can't be empty, please correct it Total: %d sharedfile User mask: Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: ctrlaltca <ctrlaltca@gmail.com>
Language-Team: Ukrainian (http://www.transifex.com/kvirc/KVIrc/language/uk/)
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 &Додати... &Огляд... &Редагувати &Гаразд Скасувати Минає в: Закінчується Шлях до файлу: Ім'я файлу Невірний тайм-аут, ігнорую Невірне видиме ім'я: використовую стандартне Маска Ім'я Ніколи Немає активних спільних файлів Немає спільного файлу з видимим іменем '%Q' і маскою '%Q' Гаразд &Видалити Спільне ім'я: Загальні файли Файл '%Q' непридатний до читання Файл не існує або не читається, будь ласка перевірте шлях Спільне ім'я не може бути порожнім, будь ласка виправте його Загалом: %d файлів Маска користувача: 