��          �      �       H     I     P  
   Y  
   d  	   o     y     �  &   �  *   �  .   �  #        +      D  �  e     �     �     �          
            *   "  /   M  *   }     �     �  #   �           	                                       
               1 Hour 1 Minute 15 Minutes 30 Minutes 5 Minutes Disable Hide Permanently (Until Explicitly Enabled) The -t switch expects a timeout in seconds The specified timeout is not valid, assuming 0 The specified window does not exist Until KVIrc is Restarted Write text or commands to window Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: ctrlaltca <ctrlaltca@gmail.com>
Language-Team: Czech (http://www.transifex.com/kvirc/KVIrc/language/cs/)
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 1 hodina 1 ninuta 15 minut 30 minut 5 minut Vypnout Schovat Pemanentně (Dokud je explicitně povolen) Přepínač -t očekává prodlevu v sekundách Zadaná prodleva je neplatná, nastavuji 0 Zadané okno neexistuje Dokud se KVIrc nerestartuje Napíše text nebo příkaz do okna 