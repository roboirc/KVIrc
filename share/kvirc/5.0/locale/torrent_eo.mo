��          t      �         &        8     W     n  9   �  <   �  N   �     H  .   W     �  j  �  )         8     Y     q  ?   �  >   �  O        X  4   h     �              	   
                                           Choosing torrent client interface "%Q" Invalid client interface "%Q"! Last interface error:  No client selected! No torrent client interface selected. Try /torrent.detect Seems that there is no usable torrent client on this machine The selected torrent client interface failed to execute the requested function Torrent Client Trying torrent client interface "%Q": score %d Using client interface "%Q". Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Robert Förster <Dessa@gmake.de>
Language-Team: Esperanto (http://www.transifex.com/kvirc/KVIrc/language/eo/)
Language: eo
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Elektas torentan klientan interfacon "%Q" Nevalida klienta interfaco "%Q"! Lasta interfaca eraro:  Neniu kliento elektita! Neniu torenta klienta interfaco elektita. Provu /torrent.detect Ŝajnas ke ne estas uzebla torenta kliento ĉe ĉi tiu maŝino La elektita torenta klienta interfaco malsukcesis ruligi la demandatan funkcion Torenta Kliento Provas torentan klientan interfacon "%Q": poentoj %d Uzas klientan interfacon "%Q". 