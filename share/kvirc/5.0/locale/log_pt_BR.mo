��    -      �  =   �      �     �     �     �                 !   .  "   P     s     {     �     �     �  ,   �  &   �               .  '   L     t     {     �     �  
   �     �     �  &   �     �     �               #     )  '   >     f     v     �     �     �     �     �  '   �       5     e  R     �     �  $   �     �     �     	  0   	  2   D	     w	     	     �	     �	     �	  4   �	  *   �	     $
  !   >
  !   `
  C   �
     �
     �
     �
     �
     �
  "   
     -  -   J     x     �     �     �  	   �     �  :   �     "     =     [     q     �     �     �  +   �     �  :        
                   %                  -      !      *            "      #                $      (       	                           '       )            &              ,                      +                    %1 on %2 Apply Filter Can't log to file '%1' Cancel Channel Channel %1 on %2 Confirm Current User Log Deletion Confirm Current User Logs Deletion Console Console on %1 Contents Filter DCC Chat DCC Chat with: %1 Do you really wish to delete all these logs? Do you really wish to delete this log? Export Log - KVIrc Export Log File to Failed to export the log '%1' Failed to load logview module, aborting Filter HTML Archive Index Log File Log Viewer Log contents mask: Log name mask: Missing window ID after the 'w' switch Only newer than: Only older than: Other Plain Text File Query Query with: %1 on %2 Remove All Log Files Within This Folder Remove Log File Show DCC chat logs Show channel logs Show console logs Show other logs Show query logs Something on: %1 This window has no logging capabilities Window '%1' not found Window with ID '%1' not found, returning empty string Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Daniel Neves
Language-Team: Portuguese (Brazil) (http://www.transifex.com/kvirc/KVIrc/language/pt_BR/)
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 %1 em %2 Aplicar Filtro Não pôde registrar no arquivo '%1' Cancelar Canal Canal %1 em %2 Confirmar o Apagar do Registro do Usuário Atual Confirmar o Apagar dos Registros do Usuário Atual Consola Console em %1 Filtro de Conteúdo Conversa DCC Bate-Papo DCC com: %1 Você realmente deseja apagar todos esses registros? Você realmente quer apagar este registro? Exportar Registro - KVIrc Exportar Arquivo de Registro para Falha ao exportar o registro '%1' Falhar ao carregar módulo de visualização de registro, abortando Filtro Arquivo HTML Índice Arquivo de Registo Visualizador de Logs Máscara de conteúdos do registo: Máscara de nome do registo: Perdendo a janela de ID depois do comando 'w' Só mais recente do que: Só mais velhos do que: Outro Arquivo em Texto Plano Pesquisar Conversa com: %1 em %2 Remover Todos Arquivos de Registro do Interior Desta Pasta Remover o Arquivo Registro Mostrar logs de conversas DCC Mostrar logs de canal Mostrar logs de console Mostrar outros logs Mostrar logs de privado Algo em: %1 Esta janela não tem capacidades de registo Janela'%1' não encontrada Janela com ID '%1' não encontrada, retornando linha vazia 