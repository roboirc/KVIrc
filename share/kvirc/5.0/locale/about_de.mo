��          �   %   �      @     A     G     S  
   `     k  
   y     �     �     �     �     �     �     �  +   �            x        �  
   �     �     �     �     �     �  g  �     Y     _     k     w     �     �  
   �     �  
   �     �     �     �  
   �  (        ,     9  �   @     �  
   �     �     �               )                                                                                  
   	                                         About About KVIrc Architecture Build Info Build command Build date Build flags CPU name Close Compiler flags Compiler name Executable Information Features Forged by the <b>KVIrc Development Team</b> Honor && Glory License Oops! Can't find the license file.
It MUST be included in the distribution...
Please report to <pragma at kvirc dot net> Qt theme Qt version Revision number Runtime Info Sources date System name System version Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Robert Förster <Dessa@gmake.de>
Language-Team: German (http://www.transifex.com/kvirc/KVIrc/language/de/)
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Über Über KVIrc Architektur Erstellungsinfo Build-Kommando Build-Datum Buildflags CPU-Name Schließen Compilerflags Compiler-Name Programmdateiinformationen Funktionen Erstellt vom <b>KVIrc-Entwicklerteam</b> Ruhm && Ehre Lizenz Ups! Die Lizenzdatei kann nicht gefunden werden.
Sie MUSS in der Distribution enthalten sein...
Bitte melden Sie dies an <pragma at kvirc dot net> Qt-Theme Qt-Version Revisionsnummer Laufzeitinformationen Quellcode-Datum System-Name System-Version 