��          �   %   �      P     Q  
   Y     d     j     n  
   u     �  
   �     �     �  #   �     �     �     �     �  7        <     ?     G     S     `  C   ~  0   �     �  
     g       {  
   �  	   �     �     �  	   �     �  	   �     �     �  *   �          "     '     +  :   G     �     �     �     �     �  J   �  =        T     n                                              
              	                                                                 &Add... &Browse... &Edit &OK Cancel Expire at: Expires File path: Filename Invalid timeout, ignoring Invalid visible name: using default Mask Name Never No active file sharedfile No sharedfile with visible name '%Q' and user mask '%Q' OK Re&move Share name: Shared Files The file '%Q' is not readable The file doesn't exist or it is not readable, please check the path The share name can't be empty, please correct it Total: %d sharedfile User mask: Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: ctrlaltca <ctrlaltca@gmail.com>
Language-Team: Italian (http://www.transifex.com/kvirc/KVIrc/language/it/)
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 &Aggiungi... &Scegli... &Modifica &OK Cancella Scadenza: Scadenza Percorso: Nome del File Timeout invalido, ignoro Nome visibile invalido: uso il predefinito Maschera Nome Mai Nessuna condivisione attiva Nessun file condiviso col nome '%Q' e maschera utente '%Q' OK &Rimuovi Nome Condiviso: File Condivisi Il file '%Q' non è leggibile Il file non esiste o non è leggibile, perfavore controlla il suo percorso Il nome condiviso non puo essere nullo, perfavore correggerlo Totale: %d file condivisi Maschera Utente: 