��          �            h     i     p  
   y  
   �  	   �     �     �  &   �     �  *   �  .     #   <     `      y  k  �            
     
   "  	   -     7     C  0   J  !   {  8   �  E   �  $     &   A  3   h                     
                                                       	    1 Hour 1 Minute 15 Minutes 30 Minutes 5 Minutes Disable Hide Permanently (Until Explicitly Enabled) Show/Hide input line The -t switch expects a timeout in seconds The specified timeout is not valid, assuming 0 The specified window does not exist Until KVIrc is Restarted Write text or commands to window Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-23 21:28+0000
Last-Translator: Ludovic Bellière <xrogaan@gmail.com>
Language-Team: French (http://www.transifex.com/kvirc/KVIrc/language/fr/)
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 1 heure 1 minute 15 Minutes 30 minutes 5 Minutes Désactiver Cacher En permanence (jusqu'à réactivation explicite) Montrer/Cacher la ligne de saisie L'option -t nécessite un temps d'expiration en secondes Le temps d'expiration spécifié est invalide, on suppose que c'est 0 La fenêtre spécifiée n'existe pas Jusqu'à ce que KVIrc soit redémarré Écrivez du texte ou des commandes dans la fenêtre 