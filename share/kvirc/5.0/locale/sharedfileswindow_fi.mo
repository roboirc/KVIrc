��          �      \      �     �  
   �     �     �     �  
   �        
                  !     &     ,     /     7     C  C   P  0   �  
   �  g  �     8  	   D     N     W     [     c     l     t     {     �     �  
   �     �     �  	   �     �  Q   �  3        J                                                                        
                	           &Add... &Browse... &Edit &OK Cancel Expire at: Expires File path: Filename Mask Name Never OK Re&move Share name: Shared Files The file doesn't exist or it is not readable, please check the path The share name can't be empty, please correct it User mask: Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: ctrlaltca <ctrlaltca@gmail.com>
Language-Team: Finnish (http://www.transifex.com/kvirc/KVIrc/language/fi/)
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 &Lisää... &Selaa... &Muokkaa &OK Peruuta Poistuu: Poistuu Polku: Tiedostonimi Maski Nimi Ei koskaan OK &Poista Jaosnimi: Jaetut tiedostot Tiedosto ei joko ole olemassa tai sitä ei voi lukea, ole hyvä ja tarkista polku Jaosnimi ei voi olla tyhjä, ole hyvä ja korjaa se Käyttäjämaski: 