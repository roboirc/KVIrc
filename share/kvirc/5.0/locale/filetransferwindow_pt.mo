��          �      |      �     �            	   !  
   +  /   6  )   f     �     �     �     �     �     �     �     �  
                        2     7  j  ;     �  4   �     �  	   �     �  ?     &   M     t     �     �     �      �     �     �     �  
     	             )     C     H                                                   
                                       	       &Clear Terminated &Copy Path to Clipboard &Open &Other... Clear &All Clear all transfers, including any in progress? Do you really want to delete the file %1? Failed to remove the file File Transfers Information Local &File MS-DOS Prompt at Location No OK Open &Location Open &With Progress Size: %1 Terminal at Location Type Yes Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: ctrlaltca <ctrlaltca@gmail.com>
Language-Team: Portuguese (http://www.transifex.com/kvirc/KVIrc/language/pt/)
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 &Limpar Terminados &Copiar Localização para a Área de Transferência &Abrir &Outro... Limpar &Todos Limpar todas as transferências, incluindo alguma em progresso? Deseja realmente apagar o ficheiro %1? Falha ao remover o ficheiro Transferências de Ficheiros Informação &Ficheiro Local Terminal MS-DOS na Localização Não OK Abrir &Localização Abrir &Com Progresso Tamanho: %1 Terminal na Localização Tipo Sim 