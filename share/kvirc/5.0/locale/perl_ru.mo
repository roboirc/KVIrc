��          T      �       �   0   �      �   ?      X   @  F   �  <   �       f     '   �  m   �  p     A   �  q   �                                        Internal error: Perl interpreter not initialized Perl execution error: The perlcore module can't be loaded: Perl support not available The perlcore module failed to execute the code: something is wrong with the Perl support This KVIrc executable has been compiled without Perl scripting support To see more details about loading failure try /perlcore.load Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Alexey Sokolov <alexey+transifex@asokolov.org>
Language-Team: Russian (http://www.transifex.com/kvirc/KVIrc/language/ru/)
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 Внутренняя ошибка: интерпретатор perl не инициализирован Ошибка выполнения perl: Не возможно загрузить модуль perlcore: поддержка perl не доступна Модуль perlcore не может выполнить код: проблемма с поддержкой perl Эта сборка KVIrc не имеет поддержки perl Более подробная информация об ошибке загрузки модуля /perlcore.load 