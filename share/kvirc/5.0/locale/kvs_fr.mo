��            )         �  >   �  %   �       $   6     [     t  %   �     �     �  	   �     �     
       #   /     S     s     �     �  #   �     �  /   �          #     @  $   Q  %   v  (   �     �     �  	   �  f  �  q   X  9   �  &     D   +  #   p  '   �  ,   �  !   �     	     	      *	     K	     _	  $   t	  '   �	     �	     �	     �	     �	  "   
  5   ?
     u
     }
     �
  )   �
  ,   �
  .        6     G  
   ^                                                        	                                                         
                       /me can be used only in channels, queries and DCC chat windows A class can't be a subclass of itself Call to undefined function '%Q' Can't delete a null object reference Can't find the icon '%Q' Can't find the requested image Can't override the builtin class '%Q' Class '%Q' is not defined Division by zero Error: %Q Failed to start the process IP address %d: %Q Missing alias name Missing non-optional parameter "%1" No button with type %Q named %Q No such event (%Q) None The alias '%Q' does not exist The specified object does not exist There is no option named '%Q' This window is not associated to an IRC context Unknown Unknown binary operator '%q' Unknown operator You don't appear to be on channel %s You're not connected to an IRC server [KVS]   Destroyed window with pointer %x [KVS] Window: [KVS]%c Warning: %Q [RAW]: %Q Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-23 21:28+0000
Last-Translator: Robert Förster <Dessa@gmake.de>
Language-Team: French (http://www.transifex.com/kvirc/KVIrc/language/fr/)
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 la commande '/me' ne peut être utilisée que dans les fenêtres de salons, messages privés et conversations DCC Une classe ne peut pas être une sous-classe d'elle-même Appel à la fonction non définie '%Q' Impossible de supprimer l'objet référencé car il est de type null Impossible de trouver l'icône '%Q' Impossible de trouver l'image demandée Impossible d'écraser la classe interne '%Q' La classe '%Q' n'est pas définie Division par zéro Erreur : %Q Échec du lancement du processus Adresse IP %d : %Q Nom d'alias manquant Paramètre obligatoire "%1" manquant Aucun boutton de type %Q avec le nom %Q Évènement inexistant (%Q) Rien L'alias '%Q' n'existe pas L'objet spécifié n'existe pas Il n'y a pas d'option nommée '%Q' Cette fenêtre n'est pas associée à un contexte IRC Inconnu Opérateur binaire '%q' inconnu Opérateur inconnu Vous ne semblez pas être sur le salon %s Vous n'êtes pas connecté à un serveur IRC [KVS]   Fenêtre détruite avec le pointeur %x [KVS] Fenêtre : [KVS]%c Attention : %Q [RAW ]: %Q 