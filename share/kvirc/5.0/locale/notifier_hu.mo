��          �            h     i     p  
   y  
   �  	   �     �     �  &   �     �  *   �  .     #   <     `      y  b  �     �                      	   "     ,  ,   3  "   `  4   �  ?   �     �  "     '   9                     
                                                       	    1 Hour 1 Minute 15 Minutes 30 Minutes 5 Minutes Disable Hide Permanently (Until Explicitly Enabled) Show/Hide input line The -t switch expects a timeout in seconds The specified timeout is not valid, assuming 0 The specified window does not exist Until KVIrc is Restarted Write text or commands to window Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-23 21:28+0000
Last-Translator: fiber <vfiber@gmail.com>
Language-Team: Hungarian (http://www.transifex.com/kvirc/KVIrc/language/hu/)
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 1 Óra 1 Perc 15 Perc 30 Perc 5 Perc Letiltás Elrejt Végleg (amíg újra engedélyezve nem lesz) Beviteli mezőt Elrejt/Megjelenít A -t kapcsoló időtúllépést vár másodpercekben A megadott időtúllépés érvénytelen, beállítás nullára A megadott ablak nem létezik Amíg a KVIrcet újra nem indítod Szöveg vagy parancs írása az ablakba 