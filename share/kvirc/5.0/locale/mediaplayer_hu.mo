��    
      l      �       �      �   "     $   /     T     m  :   �  :   �  L   �  ,   G  j  t  '   �  /     *   7     b     {  H   �  =   �  J     .   c         	                                
    Can't find symbol %1 in %2 Can't load the player library (%1) Choosing media player interface "%Q" Function not implemented Last interface error:  No mediaplayer interface selected. Try /mediaplayer.detect Seems that there is no usable media player on this machine The selected media player interface failed to execute the requested function Trying media player interface "%1": score %2 Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-23 21:28+0000
Last-Translator: Robert Förster <Dessa@gmake.de>
Language-Team: Hungarian (http://www.transifex.com/kvirc/KVIrc/language/hu/)
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Nem található %1 szimbólum ebben: %2 Lejátszó függvénytár nem betölthető (%1) Médialejátszó eszköz kiválasztva "%Q" Művelet nem támogatott Utolsó eszközhiba:  Nincs médialjeátszó kiválasztva. Próbáld így: /mediaplayer.detect Azt hiszem nincs használható médialejátszó ezen a gépen A kiválasztott lejátszóeszköz nem tudta lefuttatni a kért függvényt "%1" médialejátszó próbája, eredmény: %2 