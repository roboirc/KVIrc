��          T      �       �   $   �      �      �   :     :   I  L   �  f  �  0   8     i  #   �  S   �  O   �  V   L                                        Choosing media player interface "%Q" Function not implemented Last interface error:  No mediaplayer interface selected. Try /mediaplayer.detect Seems that there is no usable media player on this machine The selected media player interface failed to execute the requested function Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Robert Förster <Dessa@gmake.de>
Language-Team: French (http://www.transifex.com/kvirc/KVIrc/language/fr/)
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Choix de l'interface du lecteur multimédia "%Q" Fonction non implémentée Dernière erreur de l'interface :  Aucune interface de lecteur multimédia sélectionnée. Essayez /mediaplayer.detect Il semble qu'il n'y ait pas de lecteur multimédia utilisable sur cette machine Le lecteur multimédia choisi a échoué lors de l'exécution de la fonction demandée 