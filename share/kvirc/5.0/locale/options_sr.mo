��    ;      �  O   �           	                    )     0     8  	   X  
   b     m     t     �     �     �     �  &   �                    #     ,     8  	   A     K     X  	   [     e  
   k  	   v     �     �     �     �     �     �          	          (     B     U     g     v     �  	   �     �  
   �     �     �     �       	     4        P  -   \  
   �     �     �  �  �     `
     f
     n
     t
     �
     �
  %   �
  
   �
  	   �
     �
     �
     �
  +   �
          '  .   D     s     �     �     �     �     �     �  
   �     �     �     �     �  	   �     �        *     (   2     [  "   {     �     �  $   �     �     �               3     L     X     f     u      �  *   �  (   �     �  	   
  6        K  1   Y  	   �     �     �        3   2           !      
      6   )      7                     8                  (                                    "   ;                        *   4   #   &       .   $       '   5         ,   :         1   	   0             /      -                     +         9   %        &New Abort Add Cache IP address Cancel Channel Check USERHOST for online users Choose... Connection Delete Description Description: Disable broken event handlers Enable URL highlighting Enable word highlighting Force half-duplex mode on sound device Kill broken timers Name Network Network: New Network Nickname Nickname: No selection OK Password: Port: Properties Protocol: Proxy Proxy: Rejoin channels after reconnect Reopen queries after reconnect Request missing avatars Send unknown commands as /RAW Server Server: Show channel sync time Show compact mode changes Show message icons Show server pings Show timestamp Show user and host Socket Timestamp Transparent UnknownNet Use global application font User input exits away mode User-defined prefix and postfix Username Username: Warp cursor at the end of line when browsing history Window List [PREFIX]nickname[!user@host][POSTFIX] message connection irc.unknown.net unnamed Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-04-09 20:33+0000
Last-Translator: Robert Förster <Dessa@gmake.de>
Language-Team: Serbian (http://www.transifex.com/kvirc/KVIrc/language/sr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sr
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 &Novi Prekini Dodaj Keširaj IP adresu Poništi Kanal Proveri USERHOST za korisnike na vezi Izaberi... Konekcija Obriši Opis Opis: Isključi prekinute upravljače događajima Omogući URL isticanje Omogući naglašavanje reči Forsiraj half-duplex mod na muzičkom uređaju Ubij prekinute tajmere Naziv Mreža Mreža Nova Mreža Nadimak Nickname (nadimak): Bez izbora OK Lozinka: Port: Podešavanja Protokol: Proxy Proxy: Ponovo uđi na kanale po ponovnom kačenju Ponovo otvori upite po ponovnom kačenju Zahtevaj avatare koji nedostaju Posalji nepoznatu komandu kao /RAW Server Server: Pokaži vreme sinhronizovanja kanala Pokaži skraceno promene moda Pokaži ikone poruka Prikazi pingove servera Pokaži datum i vreme Pokaži korisnika i host Priključak Datum i vreme Transparentno  NepoznataMreža Koristi globalni font aplikacije Korisnikov unos izlaska iz moda odsutnosti Korisnički-definisan prefiks i postfiks Korisničko ime Korisnik: Postavi kursor na kraj linije kad se pregleda istorija Lista prozora [PREFIKS]nadimak[!korisnik@host][POSTFIKS] poruka konekcija irc.nepoznat.net bezimena 