��          �   %   �      @     A  
   I     T     Z     ^  
   e     p  
   x     �     �     �     �     �     �  7   �                         ,  C   J  0   �     �  
   �  e  �     E     Q     ^     f     o     w     �     �     �  $   �     �     �     �     �  =        B  
   E     P     b  "   u  =   �  *   �  !        #                                               
              	                                                                 &Add... &Browse... &Edit &OK Cancel Expire at: Expires File path: Filename Invalid timeout, ignoring Mask Name Never No active file sharedfile No sharedfile with visible name '%Q' and user mask '%Q' OK Re&move Share name: Shared Files The file '%Q' is not readable The file doesn't exist or it is not readable, please check the path The share name can't be empty, please correct it Total: %d sharedfile User mask: Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-23 21:28+0000
Last-Translator: ctrlaltca <ctrlaltca@gmail.com>
Language-Team: French (http://www.transifex.com/kvirc/KVIrc/language/fr/)
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 &Ajouter... &Naviguer... &Editer &Valider Annuler Expire le : Expire Chemin du fichier : Nom de fichier Temps d'expiration invalide, ignoré Masque Nom Jamais Pas de fichier partagé actif Aucun fichier partagé dont le nom est '%Q' et le masque '%Q' OK Suppri&mer Nom de partage : Fichiers partagés Le fichier '%Q', n'est pas lisible Le fichier n'existe pas ou est illisible, vérifiez le chemin Le nom de partage ne peut pas être vide ! Total : %d fichier(s) partagé(s) Masque utilisateur : 