��          �      ,      �  $   �     �  #   �  $   �  "     !   8  #   Z  "   ~  $   �  .   �  -   �  *   #  0   N  ,     -   �  '   �            $   "     G  "   e  #   �  "   �     �     �  )     -   8  ,   f  +   �  8   �  3   �  /   ,  *   \                                         
                           	          The network specified already exists The specified IP is not valid The specified network doesn't exist The specified protocol doesn't exist The specified proxy already exists The specified proxy doesn't exist The specified server already exists The specified server doesn't exist You must provide the IP as parameter You must provide the network name as parameter You must provide the port number as parameter You must provide the protocol as parameter You must provide the proxy hostname as parameter You must provide the proxy name as parameter You must provide the server name as parameter You must provide the value as parameter Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Sölve Svartskogen <solaizen@gmail.com>
Language-Team: Polish (http://www.transifex.com/kvirc/KVIrc/language/pl/)
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);
 Określona sieć już istnieje Określony adres IP jest niepoprawny Określona sieć nie istnieje Określony protokół nie istnieje Określony pośrednik już istnieje Określony pośrednik nie istnieje Określony serwer już istnieje Określony serwer nie istnieje Musisz dostarczyć adres IP jako parametr Musisz dostarczyć nazwę sieci jako parametr Musisz dostarczyć numer portu jako parametr Musisz dostarczyć protokół jako parametr Musisz określić nazwę hosta pośrednika jako parametr Musisz dostarczyć nazwę pośrednika jako parametr Musisz dostarczyć nazwę serwera jako parametr Musisz dostarczyć wartość jako parametr 