��            )   �      �     �     �     �     �     �     �     �     �            ,     &   J     q     x     ~  
   �     �     �     �     �     �     �     �     �               )     9  5   O  �  �     w     �  ;   �     �  
   �     �            #   0  
   T  Y   _  R   �               &  %   >  1   d  %   �     �     �     �     �  2   �  2   1	  4   d	  0   �	  4   �	  #   �	  ]   #
                                                                                    	             
                                        %1 on %2 Apply Filter Can't log to file '%1' Cancel Channel Channel %1 on %2 Console Console on %1 Contents Filter DCC Chat Do you really wish to delete all these logs? Do you really wish to delete this log? Filter Index Log File Log Viewer Log contents mask: Log name mask: Only newer than: Only older than: Other Query Show DCC chat logs Show channel logs Show console logs Show other logs Show query logs Window '%1' not found Window with ID '%1' not found, returning empty string Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: ctrlaltca <ctrlaltca@gmail.com>
Language-Team: Russian (http://www.transifex.com/kvirc/KVIrc/language/ru/)
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 %1 в %2 Применить фильтр Не могу журналировать в файл «%1» Отмена Канал Канал %1 в %2 Консоль Консоль для %1 Фильтр содержимого DCC Чат Вы действительно хотите удалить все эти журналы? Вы действительно хотите удалить этот журнал? Фильтр Список Файл журнала Просмотрщик журнала Маска содержимого журнала: Маска имени журнала: Новее чем: Старше чем: Прочие Приват Показывать журналы чатов DCC Показывать журналы каналов Показывать журналы консолей Показывать прочие журналы Показывать журналы приватов Окно «%1» не найдено Окно с ID «%1» не найдено, возвращается пустая строка 