��          �            h     i     p  
   y  
   �  	   �     �     �  &   �     �  *   �  .     #   <     `      y  g  �          	  
     
     	   (  
   2     =  7   F  !   ~  <   �  @   �  !        @  )   _                     
                                                       	    1 Hour 1 Minute 15 Minutes 30 Minutes 5 Minutes Disable Hide Permanently (Until Explicitly Enabled) Show/Hide input line The -t switch expects a timeout in seconds The specified timeout is not valid, assuming 0 The specified window does not exist Until KVIrc is Restarted Write text or commands to window Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: ctrlaltca <ctrlaltca@gmail.com>
Language-Team: Spanish (http://www.transifex.com/kvirc/KVIrc/language/es/)
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 1 hora 1 minuto 15 minutos 30 minutos 5 minutos Desactivar Esconder Permanentemente (hasta que se habilite explícitamente) Mostrar/ocultar línea de entrada El parámetro -t espera un tiempo de expiración en segundos El tiempo de expiración especificado no es válido, asumiendo 0 No existe la ventana especificada Hasta que KVIrc sea reiniciado Escribir texto o comandos para la ventana 