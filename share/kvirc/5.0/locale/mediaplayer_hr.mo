��          T      �       �   $   �      �      �   :     :   I  L   �  �  �     �     �     �  <   �  9     <   N                                        Choosing media player interface "%Q" Function not implemented Last interface error:  No mediaplayer interface selected. Try /mediaplayer.detect Seems that there is no usable media player on this machine The selected media player interface failed to execute the requested function Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Robert Förster <Dessa@gmake.de>
Language-Team: Croatian (http://www.transifex.com/kvirc/KVIrc/language/hr/)
Language: hr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
 Odabirem media player "%Q" Funkcija nije implementirana Posljednja greška sucelja: Nema odabranog media playera. Pokušajte /mediaplayer.detect Čini se da nema korisnih media playera na ovom računalu Odabrani media player nije uspio izvršiti traženu funkciju 