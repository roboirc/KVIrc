��            )   �      �     �     �     �     �     �     �     �     �  ,   �  &        D     W  '   u     �     �     �  
   �     �     �     �     �     �     �          #     3  '   C     k  g  �     �     �               !     1     9     G  0   P  *   �     �  !   �  2   �               #     /     B     Z     m     s     y     �     �     �     �  '   �                                                                  
                                                            	                 %1 on %2 Can't log to file '%1' Cancel Channel Channel %1 on %2 Console Console on %1 DCC Chat Do you really wish to delete all these logs? Do you really wish to delete this log? Export Log - KVIrc Failed to export the log '%1' Failed to load logview module, aborting Filter Index Log File Log Viewer Log contents mask: Log name mask: Other Query Show DCC chat logs Show channel logs Show console logs Show other logs Show query logs This window has no logging capabilities Window '%1' not found Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: ctrlaltca <ctrlaltca@gmail.com>
Language-Team: Italian (http://www.transifex.com/kvirc/KVIrc/language/it/)
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 %1 su %2 Non posso loggare sul file '%1' Annulla Canale Canale %1 su %2 Console Console su %1 DCC Chat Vuoi davvero eliminare tutti questi file di log? Vuoi davvero eliminare questo file di log? Esporta Log - KVIrc Esportazione del log '%1' fallita Caricamento del modulo logview fallito, interrompo Filtro Indice File di Log Visualizzatore Log Maschera contenuti log: Maschera nome log: Altro Query Mostra i log delle DCC chat Mostra i log del canale Mostra i log delle console Mostra altri log Mostra i log delle query Questa finestra non può essere loggata Finestra '%1' non trovata 