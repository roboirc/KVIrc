��          �      |      �     �            	   !  
   +  /   6  )   f     �     �     �     �     �     �     �     �  
                        2     7  �  ;  $   �  7        L     ^     k  T   �  <   �  ,        C     a     v  2   �     �     �  &   �     �          +     <     [     b                                                   
                                       	       &Clear Terminated &Copy Path to Clipboard &Open &Other... Clear &All Clear all transfers, including any in progress? Do you really want to delete the file %1? Failed to remove the file File Transfers Information Local &File MS-DOS Prompt at Location No OK Open &Location Open &With Progress Size: %1 Terminal at Location Type Yes Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: ctrlaltca <ctrlaltca@gmail.com>
Language-Team: Ukrainian (http://www.transifex.com/kvirc/KVIrc/language/uk/)
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Очистити &завершені &Копіювати шлях у буфер обміну &Відкрити &Інше... Очистити &всі Очистити всі передачі, включаючи незавершені? Ви дійсно хочете видалити файл %1? Неможливо видалити файл Передача файлів Інформація Локальний &файл Командний рядок MS-DOS у місці Ні Гаразд Відкрити &розміщення Відкрити &через Прогрес Розмір: %1 Термінал у місці Тип Так 