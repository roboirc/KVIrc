��          �            h     i     p  
   y  
   �  	   �     �     �  &   �     �  *   �  .     #   <     `      y  g  �            	     	        %  
   .     9  +   B  '   n  /   �  .   �  "   �  !     &   :                     
                                                       	    1 Hour 1 Minute 15 Minutes 30 Minutes 5 Minutes Disable Hide Permanently (Until Explicitly Enabled) Show/Hide input line The -t switch expects a timeout in seconds The specified timeout is not valid, assuming 0 The specified window does not exist Until KVIrc is Restarted Write text or commands to window Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: ctrlaltca <ctrlaltca@gmail.com>
Language-Team: Italian (http://www.transifex.com/kvirc/KVIrc/language/it/)
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 1 ora 1 minuto 15 minuti 30 minuti 5 minuti Disabilita Nascondi Permanentemente (Finché non sia Abilitato) Mostra/Nascondi l'inserimento del testo Lo switch -t necessita di un timeout in secondi Il timeout specificato non è valido, assumo 0 La finestra specificata non esiste Fino al prossimo riavvio di KVIrc Spedisci testo o comandi alla finestra 