��          t      �         &        8     W     n  9   �  <   �  N   �     H  .   W     �  g  �  ,     '   8     `      �  [   �  Q   �  S   O     �  8   �  $   �              	   
                                           Choosing torrent client interface "%Q" Invalid client interface "%Q"! Last interface error:  No client selected! No torrent client interface selected. Try /torrent.detect Seems that there is no usable torrent client on this machine The selected torrent client interface failed to execute the requested function Torrent Client Trying torrent client interface "%Q": score %d Using client interface "%Q". Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Robert Förster <Dessa@gmake.de>
Language-Team: German (http://www.transifex.com/kvirc/KVIrc/language/de/)
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Benutze Torrent-Client Schnittstelle »%Q«. Ungültige Client-Schnittstelle »%Q«! Letzter Schnittstellen-Fehler:  Keine Schnittstelle ausgewählt! Keine Torrent-Client Schnittstelle ausgewählt. Probieren Sie /torrent.detect auszuführen. Es scheint kein Benutzbarer Torrent-Client auf diesem System installiert zu sein. Die ausgewählte Torrent-Client Schnittstelle konnte die Funktion nicht ausführen. Torrent Client Versuche Torrent-Client Schnittstelle »%Q«: %d Punkte. Benutze Client Schnittstelle »%Q«. 