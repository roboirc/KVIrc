��            )   �      �  E   �  p   �  p   H     �  $   �     �          #     5  0   M     ~     �     �     �     �     �  >     >   @  >        �     �     �               *     ?  F   Y     �  f  �  I     �   h  �   �  *   �	  6   �	     �	     	
     (
     F
  9   a
     �
     �
     �
     �
       $     C   A  C   �  C   �          (     B     \     n     �  !   �  W   �                   	                                   
                                                                                         $resize() requires either an array as first parameter or two integers $setBackgroundColor requires either an array as first parameter, one hex string or color name, or three integers $setForegroundColor requires either an array as first parameter, one hex string or color name, or three integers '%Q' is not a valid file path A string of 6 hex digits is required Can't find the tab Can't find the tab  File is not open! Icon '%Q' doesn't exist Internal error: no valid pointer for this object Invalid parameters No such line '%d' Not an hex digit Object in invalid state Pixmap object required Process could not be started. The array passed as parameter must contain at least 2 elements The array passed as parameter must contain at least 3 elements The array passed as parameter must contain at least 4 elements The pixmap is not valid Unknown alignment '%Q' Unknown alignment: '%Q' Unknown mode '%Q' Unknown style '%Q' Unsupported datatype Unsupported datatype '%Q' Value %d for port is out of range (values allowed are from 0 to 65535) Widget object required Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-23 21:28+0000
Last-Translator: Robert Förster <Dessa@gmake.de>
Language-Team: French (http://www.transifex.com/kvirc/KVIrc/language/fr/)
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 $resize() a besoin d'un tableau ou de deux entiers, en premier paramètre $setBackgroundColor a besoin soit d'un tableau, d'une chaîne hexadécimale, d'un nom de couleur ou de 3 entiers en tant que premier paramètre $setForegroundColor a besoin soit d'un tableau, d'une chaîne hexadécimale, d'un nom de couleur ou de 3 entiers en tant que premier paramètre '%Q' n'est pas un chemin de fichier valide Une chaîne de 6 caractères hexadécimaux est requise Impossible de trouver l'onglet Impossible de trouver l'onglet Le fichier n'est pas ouvert ! L'icône '%Q' n'existe pas Erreur interne : pas de pointeur valide pour cette object Paramétres invalide La ligne '%d' n'existe pas Ce n'est pas un nombre Hexa État de l'objet invalide Object pixmap nécéssaire Le processus n'a pas pu être lancer Le tableau passé en paramètre doit contenir au moins 2 éléments Le tableau passé en paramètre doit contenir au moins 3 éléments Le tableau passé en paramètre doit contenir au moins 4 éléments Le pixmap n'est pas valide Alignement inconnu : '%Q' Alignement inconnu : '%Q' Mode inconnu '%Q' Style inconnu '%Q' Type de données non géré Type de données  '%Q' non géré La valeur %d pour le port n'est pas valide (les valeurs autorisées vont de 0 à 65535) Object widget nécéssaire 