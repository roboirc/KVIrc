��          �   %   �      P     Q  
   Y     d     j     n  
   u     �  
   �     �     �  #   �     �     �     �     �  7        <     ?     G     S     `  C   ~  0   �     �  
     f       z  
   �  	   �     �  	   �  
   �  	   �     �  
   �  -   �  5        9     >     A     F  F   c     �     �     �     �     �  >   �  0   8     i     �                                              
              	                                                                 &Add... &Browse... &Edit &OK Cancel Expire at: Expires File path: Filename Invalid timeout, ignoring Invalid visible name: using default Mask Name Never No active file sharedfile No sharedfile with visible name '%Q' and user mask '%Q' OK Re&move Share name: Shared Files The file '%Q' is not readable The file doesn't exist or it is not readable, please check the path The share name can't be empty, please correct it Total: %d sharedfile User mask: Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: ctrlaltca <ctrlaltca@gmail.com>
Language-Team: Turkish (http://www.transifex.com/kvirc/KVIrc/language/tr/)
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 &Ekle... &Gözat... &Düzenle &Tamam İptal Et Sona erme: Sona erme Dosya yolu: Dosya Adı Geçersiz zaman aşımı, göz ardı ediliyor Geçersiz görünür adı: varsayılan kullanılıyor Mask Ad Asla Aktif paylaşılan dosya yok Paylaşılan dosya görülebilir adı '%Q' ve kullanıcı maskesi '%Q' Tamam S&il Paylaşım adı: Paylaşılan Dosyalar Dosya '%Q' okunabilir değil Bu dosya yok veya okunabilir değil, dosya yolunu kontrol edin Paylaşım adı boş olamaz, lütfen düzeltiniz Toplam: %d paylaşılan dosya Kullanıcı maskesi: 