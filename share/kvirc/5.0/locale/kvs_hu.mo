��          L      |       �   	   �      �   ;   �   L     s   N  j  �     -     6  D   E  _   �  x   �                                         Error: %Q IP address %d: %Q Listing Qt properties for object named '%Q' of KVS class %Q Slot target object destroyed while emitting signal '%Q' from object '%Q::%Q' The argument of the hash count '#' operator didn't evaluate to a hash: automatic conversion from type '%Q' supplied Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-23 21:28+0000
Last-Translator: Robert Förster <Dessa@gmake.de>
Language-Team: Hungarian (http://www.transifex.com/kvirc/KVIrc/language/hu/)
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Hiba: %Q IP cím %d: %Q '%Q' objektum Qt tulajdonságainak listázása a %Q KVS osztályból '%Q' szignál kibocsátása alatt a foglalat objektum törlődött. Szignált küldő: '%Q::%Q' A '#' mint hash számláló operátor nem értékelhető ki hashként: '%Q' típusról automatikus konverzió történt. 