��          �      �       0  $   1     V  "   t  #   �  $   �  .   �  -     *   =  0   h  ,   �  -   �  '   �  i    "   �  !   �  "   �      �  '     0   7  -   h  *   �  1   �  ,   �  .      *   O                                    
         	             The network specified already exists The specified IP is not valid The specified proxy already exists The specified server already exists You must provide the IP as parameter You must provide the network name as parameter You must provide the port number as parameter You must provide the protocol as parameter You must provide the proxy hostname as parameter You must provide the proxy name as parameter You must provide the server name as parameter You must provide the value as parameter Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-23 21:28+0000
Last-Translator: ctrlaltca <ctrlaltca@gmail.com>
Language-Team: Hungarian (http://www.transifex.com/kvirc/KVIrc/language/hu/)
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 A megadott hálózat már létezik A megadott IP cím nem érvényes A megadott protokoll már létezik A megadott szerver már létezik IP címet kell megadnod paraméterként A hálózat nevét kell megadnod paraméterként A port számát kell megadnod paraméterként A protokollt kell megadnod paraméterként A proxy hosztnevét kell megadnod paraméterként A proxy nevét kell megadnod paraméterként A szerver nevét kell megadnod paraméterként Az értéket kell megadnod paraméterként 