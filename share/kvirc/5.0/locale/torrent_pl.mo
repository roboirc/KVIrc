��          |      �          Q   !  &   s     �     �     �  9   �  <     N   [     �  .   �     �       M     *   T  &        �     �  @   �  G     Q   _     �  5   �      �              
                                         	    An interface for KDE's KTorrent client.
Download it from http://www.ktorrent.org
 Choosing torrent client interface "%Q" Invalid client interface "%Q"! Last interface error:  No client selected! No torrent client interface selected. Try /torrent.detect Seems that there is no usable torrent client on this machine The selected torrent client interface failed to execute the requested function Torrent Client Trying torrent client interface "%Q": score %d Using client interface "%Q". Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Sölve Svartskogen <solaizen@gmail.com>
Language-Team: Polish (http://www.transifex.com/kvirc/KVIrc/language/pl/)
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);
 Interfejs dla klienta KTorrent.
Możesz pobrać go z http://www.ktorrent.org
 Wybieranie interfejsu klienta torrent "%Q" Nieprawidłowy interfejs klienta "%Q"! Ostatni błąd interfejsu: Nie wybrano klienta! Nie wybrano interfejsu klienta torrent. Spróbuj /torrent.detect Wygląda na to, że nie ma użytecznego klienta torrent na tej maszynie Wybranemu interfejsowi klienta torrent nie udało się wykonać żądanej funkcji Klient torrent Próbowanie interfejsu klienta torrent "%Q": wynik %d Użycie interfejsu klienta "%Q". 