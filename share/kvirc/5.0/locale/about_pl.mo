��          �   %   �      @     A     G     S  
   `     k  
   y     �     �     �     �     �     �     �  +   �            x        �  
   �     �     �     �     �     �     �     �     �     �     	          *     6  	   C     M     U     g     y     �  *   �     �     �  �   �     v  	        �     �     �     �     �                                                                                  
   	                                         About About KVIrc Architecture Build Info Build command Build date Build flags CPU name Close Compiler flags Compiler name Executable Information Features Forged by the <b>KVIrc Development Team</b> Honor && Glory License Oops! Can't find the license file.
It MUST be included in the distribution...
Please report to <pragma at kvirc dot net> Qt theme Qt version Revision number Runtime Info Sources date System name System version Project-Id-Version: KVIrc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-22 12:41+0000
Last-Translator: Sölve Svartskogen <solaizen@gmail.com>
Language-Team: Polish (http://www.transifex.com/kvirc/KVIrc/language/pl/)
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);
 O O KVIrc Architektura Informacje budowy Komenda budowy Data budowy Flagi budowy Nazwa CPU Zamknij Flagi kompilatora Nazwa kompilatora Informacje pliku wykonywalnego Funkcjonalności Wykuto przez <b>KVIrc Development Team</b> Honor && Chwała Licencja Ups! Nie udało się znaleźć pliku licencji.
MUSI być ona zawarta w dystrybucji...
Proszę zgłosić to do <pragma at kvirc dot net> Motyw Qt Wersja Qt Numer rewizji Informacje środowiska Data źródeł Nazwa systemu Wersja systemu 