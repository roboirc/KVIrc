<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<link rel="dns-prefetch" href="https://github.githubassets.com">
<link rel="dns-prefetch" href="https://avatars0.githubusercontent.com">
<link rel="dns-prefetch" href="https://avatars1.githubusercontent.com">
<link rel="dns-prefetch" href="https://avatars2.githubusercontent.com">
<link rel="dns-prefetch" href="https://avatars3.githubusercontent.com">
<link rel="dns-prefetch" href="https://github-cloud.s3.amazonaws.com">
<link rel="dns-prefetch" href="https://user-images.githubusercontent.com/">

<link crossorigin="anonymous" media="all" integrity="sha512-hddDYPWR0gBbqLRmIZP242WMEiYsVkYI2UCYCVUHB4h5DhD2cbtFJYG+HPh21dZGb+sbgDHxQBNJCBq7YbmlBQ==" rel="stylesheet" href="https://github.githubassets.com/assets/frameworks-02a3eaa24db2bd1ed9b64450595fc2cf.css" />
<link crossorigin="anonymous" media="all" integrity="sha512-virfO4ceMMzCPFi08e0bL1pXMXF3ykBOdO+cOkFu6hqjl/ympiylGiiYhsaumnY+oVIHNqPwmxmEeCcYGWZwlA==" rel="stylesheet" href="https://github.githubassets.com/assets/site-889ba3896abfbcb8742b625020c44299.css" />
<link crossorigin="anonymous" media="all" integrity="sha512-yJL6LQkIGy+RsTeoKHdBKWVjPdOGVp9EoO3oeIhRGyhgAz22YhKB52hM6mGvQ6U/rOjQoR4x1atooFUwM0l7fQ==" rel="stylesheet" href="https://github.githubassets.com/assets/github-9f1009c5473fcfa407b651513175cb55.css" />

<meta name="viewport" content="width=device-width">
      
<div id="readme" class="Box-body readme blob js-code-block-container">
<article class="markdown-body entry-content p-3 p-md-6" itemprop="text"><h1><a id="user-content--kvirc" class="anchor" aria-hidden="true" href="#-kvirc"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a><a target="_blank" rel="noopener noreferrer" href="https://github.com/kvirc/KVIrc/wiki/images/KVIrc-midnight.png"><img src="https://github.com/kvirc/KVIrc/wiki/images/KVIrc-midnight.png" alt="KVIrc-Midnight" style="max-width:100%;"></a> KVIrc</h1>
<p><a href="https://travis-ci.org/kvirc/KVIrc" rel="nofollow"><img src="https://camo.githubusercontent.com/3cb2039c8913d8c665c9842108d0b8492fe9501a/68747470733a2f2f7472617669732d63692e6f72672f6b766972632f4b564972632e7376673f6272616e63683d6d6173746572" alt="Travis Build Status" data-canonical-src="https://travis-ci.org/kvirc/KVIrc.svg?branch=master" style="max-width:100%;"></a>
<a href="https://ci.appveyor.com/project/DarthGandalf/kvirc/branch/master" rel="nofollow"><img src="https://camo.githubusercontent.com/350634b0dc11f2d0951312f2cb8ae7b1921a912b/68747470733a2f2f63692e6170707665796f722e636f6d2f6170692f70726f6a656374732f7374617475732f6a36746a656c3065616579697863626e2f6272616e63682f6d61737465723f7376673d74727565" alt="AppVeyor Build status" data-canonical-src="https://ci.appveyor.com/api/projects/status/j6tjel0eaeyixcbn/branch/master?svg=true" style="max-width:100%;"></a>
<a href="https://scan.coverity.com/projects/kvirc-coverity" rel="nofollow"><img src="https://camo.githubusercontent.com/7091e419b907cb09eabeb0b2bb1b3bc0037d5116/68747470733a2f2f7363616e2e636f7665726974792e636f6d2f70726f6a656374732f363834312f62616467652e737667" alt="Coverity Scan Build Status" data-canonical-src="https://scan.coverity.com/projects/6841/badge.svg" style="max-width:100%;"></a></p>

<p>Welcome to the development and bug tracker for the KVIrc project.</p>
<p>KVIrc is a free and portable IRC client leveraging the Qt GUI toolkit. KVIrc is written by Szymon Stefanek and the KVIrc development team, with <a href="https://github.com/kvirc/KVIrc/graphs/contributors">contributions</a> and support of many IRC addicted developers around the world.</p>
<h2><a id="user-content-getting-involved" class="anchor" aria-hidden="true" href="#getting-involved"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Getting Involved</h2>

<ul>
<li><a href="https://github.com/kvirc/KVIrc/wiki/Getting-involved-and-contributing">Contributing</a></li>
</ul>

<h2><a id="user-content-help--support" class="anchor" aria-hidden="true" href="#help--support"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Help &amp; Support</h2>
<ul>
<li><a href="https://github.com/kvirc/KVIrc/wiki/home">KVIrc Wiki</a></li>
<li><a href="http://www.kvirc.net/doc" rel="nofollow">KVIrc Manual</a></li>
<li><a href="http://www.kvirc.net/api" rel="nofollow">KVIrc API</a></li>
<li><a href="https://github.com/kvirc/KVIrc/wiki/Submitting-a-bug-report">How to submit a bug report</a></li>
<li><a href="https://github.com/kvirc/KVIrc/issues">Open a bug report or make a suggestion</a></li>
<li>Talk to us - <a href="https://webchat.freenode.net?nick=kvirc-user&amp;channels=%23kvirc&amp;prompt=1&amp;uio=OT10cnVlde" rel="nofollow">Join #KVIrc</a> on freenode.net</li>
</ul>
<hr>
<h2><a id="user-content-downloads" class="anchor" aria-hidden="true" href="#downloads"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Downloads</h2>
<p>Try one of our fresh off-the-press <a href="https://github.com/kvirc/KVIrc/wiki/Downloading-KVIrcs-nightly-source-or-binaries">nightly builds</a> for Windows, Ubuntu and Debian.</p>
<p>We're also looking for volunteer packagers and maintainers to other Linux distributions, that can help us extend KVIrc's availability in other operating systems.</p>
<h2><a id="user-content-compile-your-own" class="anchor" aria-hidden="true" href="#compile-your-own"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Compile Your Own</h2>
<p>If you want to compile KVIrc on your chosen platform, we have guides on how to do that for a few popular ones:</p>
<ul>
<li><a href="https://github.com/kvirc/KVIrc/wiki/installation">Linux</a></li>
<li><a href="https://github.com/kvirc/KVIrc/wiki/Compiling-KVIrc-on-macOS">macOS</a></li>
<li><a href="https://github.com/kvirc/KVIrc/wiki/Compiling-KVIrc-on-Windows">Windows</a></li>
</ul>
<h2><a id="user-content-license" class="anchor" aria-hidden="true" href="#license"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>License</h2>
<p><a href="/roboirc/KVIrc/blob/master/COPYING"><img src="https://camo.githubusercontent.com/5c1ebd4a87a76bc5c04a2e1ecd19c484f68764b4/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f4b564972632d47504c76322b2d626c75652e737667" alt="KVIrc GPLv2+" data-canonical-src="https://img.shields.io/badge/KVIrc-GPLv2+-blue.svg" style="max-width:100%;"></a>
<a href="/roboirc/KVIrc/blob/master/doc/LICENSE-OPENSSL"><img src="https://camo.githubusercontent.com/6307a8546500c9618e2f144d439713d9412fe0a2/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f4f70656e53534c2d4c6963656e63652d6f72616e67652e737667" alt="OpenSSL" data-canonical-src="https://img.shields.io/badge/OpenSSL-Licence-orange.svg" style="max-width:100%;"></a>
<a href="/roboirc/KVIrc/blob/master/doc/LICENSE-OPENSSL"><img src="https://camo.githubusercontent.com/b7ee6a7971e68647ce09eee8ccf155a1272c4c14/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f4f70656e53534c2d457863657074696f6e2d6f72616e67652e737667" alt="OpenSSL exception" data-canonical-src="https://img.shields.io/badge/OpenSSL-Exception-orange.svg" style="max-width:100%;"></a></p>
<h2><a id="user-content-roboircs-plans-and-progress-striked-out" class="anchor" aria-hidden="true" href="#roboircs-plans-and-progress-striked-out"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>roboirc's plans and progress (striked out):</h2>
<p>I hope to implement more features in KVIrc and bring it upto standard with a usable IRC client as compared to others. Being a cross platform client (especially Linux based) makes KVIrc interesting and powerful. Anywhere in code where I made modifications, I will put the date and my nickname as a comment. I will also strike out items already done. All the features will be ported to this repository: <a href="https://github.com/roboirc/KVIrc">https://github.com/roboirc/KVIrc</a>.</p>
<h2><a id="user-content-features-already-implemented" class="anchor" aria-hidden="true" href="#features-already-implemented"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Features already implemented:</h2>

<ul>
<li><p><s> In the "Servers and Configuration" show description column on the left side and server information column on the right side. </s></p></li>
<li><p><s> Icon Table should show all icons by names identifiers not numbers because the popup-editor uses names. </s></p></li>
<li><p><s> Make icons in Icon Table larger and table window centered. </s></p></li>
<li><p><s> Get rid of Hops. Hops tells how far away a nickname is from its home server. </s></p></li>
<li><p><s> Right click popup menu has no option to open file location in "File Transfers". </s></p></li>
<li><p><s> Include a character count indicator for topic for the user so that the user stays within IRC limit and make the tooltip</s></p><s></s></li><s>
<li><p><s> Get rid of friendly mode </s></p></li>
<li><p><s> Remove Topic History button </s></p></li>
<li><p><s> IRC Notices to be shown in channel instead of as queries / messages (like in other clients) </s><br>Option already exists in settings "Open Query for Private Notices". By default currently IRC notices are sent as a private msg.</p></li>
<li>Let the user choose colour (foreground / background) of away users instead of just greying them out </s><br>Option already exists under Configure Theme -&gt; Userlist -&gt; Foreground Tab </s></p></li>
<li><p><s> Include topic character length and max limit per server on topic tooltip </s></p></li>
<li><p><s> Include a command window.saveBufferAs that lets user save current window contents to any file </s></p></li></ul>
<h2>Features that I hope to implement in KVIrc:</h2>
<ul>
<li><p>Integrate updated KVirc docs with the one inside KVirc</p></li>
<li><p>Implement better way of zooming in / zooming out (change size of chat channel) via keyboard shortcut and/or mouse.</p></li>
<li><p>Implement some more basic missing features in KVirc such as options in menus etc.include information about number of characters in topic. </p></li>
<li><p>Show only favourite servers in the drop down menu under "Connect To" with Description only, no server info!.</p></li>
<li><p>Allow the user to be able to get the reply to any IRC command into a variable for future parsing / manipulation in code.</p></li>
<li><p>Improve the "ison " command and make it return 1 or 0 instead of text into the channel as " is online" which is   not useful in coding. (<a href="http://www.kvirc.net/doc/cmd_ison.html" rel="nofollow">http://www.kvirc.net/doc/cmd_ison.html</a>)</p></li>
<li><p>Expand the Zoom In / Zoom Out feature of IRC channel (font) to work on mouse shortcut.</p></li>
<li><p>When downloading files, the user is not asked regarding destination folder.</p></li>
<li><p>Allow users to open downloaded file's folder upon double click in "File Transfers"</p></li>
<li><p>Get rid of "Configure Bandwidth" in "File Transfers"</p></li>
<li><p>Get rid of "Dock" and "Undock" in KVIrc</p></li>
<li><p><a href="https://github.com/kvirc/KVIrc/issues/2434#issue-452457076">https://github.com/kvirc/KVIrc/issues/2434#issue-452457076</a></p></li>
<li><p><a href="https://github.com/kvirc/KVIrc/issues/2429#issue-444771968">https://github.com/kvirc/KVIrc/issues/2429#issue-444771968</a></p></li>
<li><p><a href="https://github.com/kvirc/KVIrc/issues/2427#issue-443000110">https://github.com/kvirc/KVIrc/issues/2427#issue-443000110</a></p></li>
<li><p><a href="https://github.com/kvirc/KVIrc/issues/2433#issue-452455202">https://github.com/kvirc/KVIrc/issues/2433#issue-452455202</a></p></li>
<li><p><a href="https://github.com/kvirc/KVIrc/issues/2430#issue-448599495">https://github.com/kvirc/KVIrc/issues/2430#issue-448599495</a></p></li>
<li><p>Allow user to be able to open selected item from "Servers and Configuration" into a new IRC Context via right click popup option.</p></li>
<li><p>Improve channel settings options such as Ban Masks, Modes, Exceptions etc. Icon toolbar is confusing. Will create a new dialog window.</p></li>
<li><p>Make "Connect Now" smaller MAYBE.</p></li>
<li><p>Replace old icon images with newer ones from google or completely remove icons in KVIrc</p></li>
<li><p>Allow user to scroll through the channels in left treebar with arrow keys only NOT wheel mouse.</p></li>
<li><p>In right click popup menu of channel in treebar, change "Hop" to "Cycle Channel" because its confusing. Cycle Channel means   leave and then rejoin quickly. It confuses with the other "Hop" for nicknames.</p></li>
<li><p>$serialize and $unserialize don't seem to work well and need to be updated. <a href="http://www.kvirc.net/doc/fnc_serialize.html" rel="nofollow">http://www.kvirc.net/doc/fnc_serialize.html</a> and   <a href="http://www.kvirc.net/doc/fnc_unserialize.html" rel="nofollow">http://www.kvirc.net/doc/fnc_unserialize.html</a>. Currently after serializing, the data becomes scrambled not in the order it came in.</p></li>
<li><p>Create a delay in command to create a pause for time in ms.</p></li>
<li><p>Organize and sort out settings panel</p></li>
<li><p>For some reason the user cannot see their own pastes in channel via .kvs script. Need to fix.</p></li>
<li><p>If a nickname is not online, WHOIS reply gives in Server / Console window for the server: "(nickname) no such server" Need to fix this message as its unclear. And the message should be given in the target channel</p></li>
<li><p>Remove "Ban Exceptions" and "Invite Exceptions"</p></li>
<li><p>Right click popup menu anywhere in the "File Transfer" window should let users "Clear All", "Clear Terminated", Not just on top of the transfers.</p></li>
<li><p>Create $+ concatenation operator equivalent</p></li>
<li><p>When exec command is used, the output is not stored / displayed properly. It should be stored to a String for later manipulation.</p></li>
<li><p>Need to improve the logic behind echo to destination channel for information. -w = $window() doesn't always work properly. Need to fix this...</p></li>
<li><p>Allow the user to be able to select multiple items in the Popup Menus and be able to copy / delete them. Right one the user can only do this for 1 item at a time and its painful.</p></li>
<li><p>Right now the user cannot check if an item is in an array. Need a function like this.</p></li>
<li><p>Error messages from within specific sections of code such as callback etc are not descriptive and are often unclear hard to pinpoint. Better to give error in terms of line location in entire file / script such as "[14:47:07] [KVS]   In script context "/home/roboirc.kvs", line 514, near character 15" instead of "[14:53:27] [KVS]   In script context "click callback for onjoindatastop", line 7, near character 1". Its easier to find the error in terms of location / line number in the entire file instead of just a specific callback IMO. Update error messages so that they are more clear. Right now if something is faulty it gives too many messages. Better to give only the line and column and the fault. Let the user figure out the rest.</p></li>
<li><p>colordialog class doesn't work</p></li>
<li><p>In Script Tester, member functions / variables , etc should show in a dropdown menu for all the built in methods of KVirc such as classes, etc. This includes when user types in "-&gt;" in the Script Tester, it should show all the class's methods in a drop down menu. This would be useful for people not familiar with the methods at all.</p></li>
<li><p>Include line numbers in the Script Tester for easy navigation.</p></li>
<li><p>Add more parameters ($0, $1, ...) in events lacking information such as channel source, etc.</p></li>
<li><p>OnIRCConnectionEstablished event has been poorly labeled. Can be confused for: OnIRC event. Need to fix this.</p></li>
<li><p>Get rid of "User list" toggle button / option. User list should always be enabled imo.</p></li>
<li><p>Get rid of "Split screen" toggle button / option.</p></li>
<li><p>Add OnScriptLoad and OnScriptUnload events.</p></li>
<li><p>config related commands don't work at all, it seems not implemented properly.</p></li>
<li><p>Get rid of tree format in Servers and Configuration and replace with simple list with description of connection only.</p></li>
<li><p>Allow users to strip specific characters from left and right ends of a string instead of just whitespace as in $str.strip</p></li>
<li><p>The documentation mentions signals and slots but doesn't specify whether we can use aliases as slots. If not this should be implemented.</p></li>
<li><p>Put all channel related functions / options on a new dialog window for easy access including topic like in mirc / etc</p></li>
<li><p>List all possible object events since documentation lacks it like "clicked" etc.</p></li>
<li><p>Allow users to copy / cut / paste multiple items in the popup editor.</p></li>
<li><p><s>After importing list from mirc.com in the "Servers and Configuration", the tree widget shows up expanded. Need to make sure all branches in tree widget are compressed not expanded.</s></p></li>
<li><p>Allow user to access any channel's buffer via an identifier</p></li>
<li><p>Right now touchpad scrolling does not control the channel tree scrolling, need to fix this. Have to make it same as nicklist scrolling.</p></li>
<li><p>Make search results in Settings highlight in RED instead of underline</p></li>
</ul>

<p>more later...</p>
<p><b> If you have any feature requests, please post in issues! </b></p>
<h2><a id="user-content-roboircs-discussion" class="anchor" aria-hidden="true" href="#roboircs-discussion"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>roboirc's discussion:</h2>

If getting errors in KVIRC, may need these files when compiling KVIrc for the first time:

1) Install "libaudiofile-dev" package
2) Install "libperl-dev" package
3) Install "python-dev" package
4) Install "qtmultimedia5-dev" package

<br>
<p>
I recommend CLion IDE, a very good IDE based on same engine as Android Studio to build the source code! First "git clone ..." the repository into a folder. Then run the following commands in the the KVIrc folder:</p>

<p><b>cmake -DCMAKE_INSTALL_PREFIX=./ -DLIB_SUFFIX=./<br>
make<br>
make install</b>
</p>

<p>It will install all the required libraries etc into the same folder "KVIrc"
Then you can easily use CLion IDE to open the project / folder and build/compile etc whatever you want to do with it. But remember when you modify any code, you MUST rebuild all. It takes time but this is where CLion is a bit fast.</p>
<p>You can recompile from within the terminal inside CLion IDE too.</p>
<h1><a id="user-content-roboirckvs" class="anchor" aria-hidden="true" href="#roboirckvs"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>roboirc.kvs</h1>
<p>I have also included my kvirc script .kvs in which I am trying to include as many features as I can slowly.</p>
<p>To load my script just go to <b>Scripting -&gt; Execute Script</b>, find the .kvs file and it is loaded. Then to uninstall go to <b>Settings -&gt; Manage Addons -&gt; roboirc script -&gt; Trash Bin Symbol</b> to unload the script.</p>
