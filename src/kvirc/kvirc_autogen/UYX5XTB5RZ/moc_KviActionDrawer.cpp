/****************************************************************************
** Meta object code from reading C++ file 'KviActionDrawer.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../ui/KviActionDrawer.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'KviActionDrawer.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_KviActionDrawer_t {
    QByteArrayData data[1];
    char stringdata0[16];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_KviActionDrawer_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_KviActionDrawer_t qt_meta_stringdata_KviActionDrawer = {
    {
QT_MOC_LITERAL(0, 0, 15) // "KviActionDrawer"

    },
    "KviActionDrawer"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_KviActionDrawer[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void KviActionDrawer::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject KviActionDrawer::staticMetaObject = { {
    &QTabWidget::staticMetaObject,
    qt_meta_stringdata_KviActionDrawer.data,
    qt_meta_data_KviActionDrawer,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *KviActionDrawer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *KviActionDrawer::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_KviActionDrawer.stringdata0))
        return static_cast<void*>(this);
    return QTabWidget::qt_metacast(_clname);
}

int KviActionDrawer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QTabWidget::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_KviActionDrawerPage_t {
    QByteArrayData data[1];
    char stringdata0[20];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_KviActionDrawerPage_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_KviActionDrawerPage_t qt_meta_stringdata_KviActionDrawerPage = {
    {
QT_MOC_LITERAL(0, 0, 19) // "KviActionDrawerPage"

    },
    "KviActionDrawerPage"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_KviActionDrawerPage[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void KviActionDrawerPage::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject KviActionDrawerPage::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_KviActionDrawerPage.data,
    qt_meta_data_KviActionDrawerPage,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *KviActionDrawerPage::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *KviActionDrawerPage::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_KviActionDrawerPage.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int KviActionDrawerPage::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_KviActionDrawerPageListWidget_t {
    QByteArrayData data[1];
    char stringdata0[30];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_KviActionDrawerPageListWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_KviActionDrawerPageListWidget_t qt_meta_stringdata_KviActionDrawerPageListWidget = {
    {
QT_MOC_LITERAL(0, 0, 29) // "KviActionDrawerPageListWidget"

    },
    "KviActionDrawerPageListWidget"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_KviActionDrawerPageListWidget[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void KviActionDrawerPageListWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject KviActionDrawerPageListWidget::staticMetaObject = { {
    &KviTalListWidget::staticMetaObject,
    qt_meta_stringdata_KviActionDrawerPageListWidget.data,
    qt_meta_data_KviActionDrawerPageListWidget,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *KviActionDrawerPageListWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *KviActionDrawerPageListWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_KviActionDrawerPageListWidget.stringdata0))
        return static_cast<void*>(this);
    return KviTalListWidget::qt_metacast(_clname);
}

int KviActionDrawerPageListWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KviTalListWidget::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
