/****************************************************************************
** Meta object code from reading C++ file 'KviMainWindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../ui/KviMainWindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'KviMainWindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_KviMainWindow_t {
    QByteArrayData data[32];
    char stringdata0[669];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_KviMainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_KviMainWindow_t qt_meta_stringdata_KviMainWindow = {
    {
QT_MOC_LITERAL(0, 0, 13), // "KviMainWindow"
QT_MOC_LITERAL(1, 14, 19), // "activeWindowChanged"
QT_MOC_LITERAL(2, 34, 0), // ""
QT_MOC_LITERAL(3, 35, 20), // "activeContextChanged"
QT_MOC_LITERAL(4, 56, 25), // "activeContextStateChanged"
QT_MOC_LITERAL(5, 82, 31), // "activeConnectionNickNameChanged"
QT_MOC_LITERAL(6, 114, 31), // "activeConnectionUserModeChanged"
QT_MOC_LITERAL(7, 146, 32), // "activeConnectionAwayStateChanged"
QT_MOC_LITERAL(8, 179, 33), // "activeConnectionServerInfoCha..."
QT_MOC_LITERAL(9, 213, 26), // "activeConnectionLagChanged"
QT_MOC_LITERAL(10, 240, 33), // "activeWindowSelectionStateCha..."
QT_MOC_LITERAL(11, 274, 16), // "bGotSelectionNow"
QT_MOC_LITERAL(12, 291, 10), // "newConsole"
QT_MOC_LITERAL(13, 302, 22), // "executeInternalCommand"
QT_MOC_LITERAL(14, 325, 5), // "index"
QT_MOC_LITERAL(15, 331, 15), // "toggleStatusBar"
QT_MOC_LITERAL(16, 347, 13), // "toggleMenuBar"
QT_MOC_LITERAL(17, 361, 16), // "toggleWindowList"
QT_MOC_LITERAL(18, 378, 17), // "customizeToolBars"
QT_MOC_LITERAL(19, 396, 18), // "switchToNextWindow"
QT_MOC_LITERAL(20, 415, 18), // "switchToPrevWindow"
QT_MOC_LITERAL(21, 434, 29), // "switchToNextHighlightedWindow"
QT_MOC_LITERAL(22, 464, 29), // "switchToPrevHighlightedWindow"
QT_MOC_LITERAL(23, 494, 27), // "switchToNextWindowInContext"
QT_MOC_LITERAL(24, 522, 27), // "switchToPrevWindowInContext"
QT_MOC_LITERAL(25, 550, 17), // "closeActiveWindow"
QT_MOC_LITERAL(26, 568, 14), // "accelActivated"
QT_MOC_LITERAL(27, 583, 21), // "toolbarsPopupSelected"
QT_MOC_LITERAL(28, 605, 8), // "QAction*"
QT_MOC_LITERAL(29, 614, 7), // "pAction"
QT_MOC_LITERAL(30, 622, 21), // "iconSizePopupSelected"
QT_MOC_LITERAL(31, 644, 24) // "buttonStylePopupSelected"

    },
    "KviMainWindow\0activeWindowChanged\0\0"
    "activeContextChanged\0activeContextStateChanged\0"
    "activeConnectionNickNameChanged\0"
    "activeConnectionUserModeChanged\0"
    "activeConnectionAwayStateChanged\0"
    "activeConnectionServerInfoChanged\0"
    "activeConnectionLagChanged\0"
    "activeWindowSelectionStateChanged\0"
    "bGotSelectionNow\0newConsole\0"
    "executeInternalCommand\0index\0"
    "toggleStatusBar\0toggleMenuBar\0"
    "toggleWindowList\0customizeToolBars\0"
    "switchToNextWindow\0switchToPrevWindow\0"
    "switchToNextHighlightedWindow\0"
    "switchToPrevHighlightedWindow\0"
    "switchToNextWindowInContext\0"
    "switchToPrevWindowInContext\0"
    "closeActiveWindow\0accelActivated\0"
    "toolbarsPopupSelected\0QAction*\0pAction\0"
    "iconSizePopupSelected\0buttonStylePopupSelected"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_KviMainWindow[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      26,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       9,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  144,    2, 0x06 /* Public */,
       3,    0,  145,    2, 0x06 /* Public */,
       4,    0,  146,    2, 0x06 /* Public */,
       5,    0,  147,    2, 0x06 /* Public */,
       6,    0,  148,    2, 0x06 /* Public */,
       7,    0,  149,    2, 0x06 /* Public */,
       8,    0,  150,    2, 0x06 /* Public */,
       9,    0,  151,    2, 0x06 /* Public */,
      10,    1,  152,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      12,    0,  155,    2, 0x0a /* Public */,
      13,    1,  156,    2, 0x0a /* Public */,
      15,    0,  159,    2, 0x0a /* Public */,
      16,    0,  160,    2, 0x0a /* Public */,
      17,    0,  161,    2, 0x0a /* Public */,
      18,    0,  162,    2, 0x0a /* Public */,
      19,    0,  163,    2, 0x09 /* Protected */,
      20,    0,  164,    2, 0x09 /* Protected */,
      21,    0,  165,    2, 0x09 /* Protected */,
      22,    0,  166,    2, 0x09 /* Protected */,
      23,    0,  167,    2, 0x09 /* Protected */,
      24,    0,  168,    2, 0x09 /* Protected */,
      25,    0,  169,    2, 0x09 /* Protected */,
      26,    0,  170,    2, 0x09 /* Protected */,
      27,    1,  171,    2, 0x09 /* Protected */,
      30,    1,  174,    2, 0x09 /* Protected */,
      31,    1,  177,    2, 0x09 /* Protected */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   11,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   14,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 28,   29,
    QMetaType::Void, 0x80000000 | 28,   29,
    QMetaType::Void, 0x80000000 | 28,   29,

       0        // eod
};

void KviMainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<KviMainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->activeWindowChanged(); break;
        case 1: _t->activeContextChanged(); break;
        case 2: _t->activeContextStateChanged(); break;
        case 3: _t->activeConnectionNickNameChanged(); break;
        case 4: _t->activeConnectionUserModeChanged(); break;
        case 5: _t->activeConnectionAwayStateChanged(); break;
        case 6: _t->activeConnectionServerInfoChanged(); break;
        case 7: _t->activeConnectionLagChanged(); break;
        case 8: _t->activeWindowSelectionStateChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 9: _t->newConsole(); break;
        case 10: _t->executeInternalCommand((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 11: _t->toggleStatusBar(); break;
        case 12: _t->toggleMenuBar(); break;
        case 13: _t->toggleWindowList(); break;
        case 14: _t->customizeToolBars(); break;
        case 15: _t->switchToNextWindow(); break;
        case 16: _t->switchToPrevWindow(); break;
        case 17: _t->switchToNextHighlightedWindow(); break;
        case 18: _t->switchToPrevHighlightedWindow(); break;
        case 19: _t->switchToNextWindowInContext(); break;
        case 20: _t->switchToPrevWindowInContext(); break;
        case 21: _t->closeActiveWindow(); break;
        case 22: _t->accelActivated(); break;
        case 23: _t->toolbarsPopupSelected((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 24: _t->iconSizePopupSelected((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 25: _t->buttonStylePopupSelected((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (KviMainWindow::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&KviMainWindow::activeWindowChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (KviMainWindow::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&KviMainWindow::activeContextChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (KviMainWindow::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&KviMainWindow::activeContextStateChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (KviMainWindow::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&KviMainWindow::activeConnectionNickNameChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (KviMainWindow::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&KviMainWindow::activeConnectionUserModeChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (KviMainWindow::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&KviMainWindow::activeConnectionAwayStateChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (KviMainWindow::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&KviMainWindow::activeConnectionServerInfoChanged)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (KviMainWindow::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&KviMainWindow::activeConnectionLagChanged)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (KviMainWindow::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&KviMainWindow::activeWindowSelectionStateChanged)) {
                *result = 8;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject KviMainWindow::staticMetaObject = { {
    &KviTalMainWindow::staticMetaObject,
    qt_meta_stringdata_KviMainWindow.data,
    qt_meta_data_KviMainWindow,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *KviMainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *KviMainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_KviMainWindow.stringdata0))
        return static_cast<void*>(this);
    return KviTalMainWindow::qt_metacast(_clname);
}

int KviMainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KviTalMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 26)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 26;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 26)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 26;
    }
    return _id;
}

// SIGNAL 0
void KviMainWindow::activeWindowChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void KviMainWindow::activeContextChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void KviMainWindow::activeContextStateChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void KviMainWindow::activeConnectionNickNameChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void KviMainWindow::activeConnectionUserModeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void KviMainWindow::activeConnectionAwayStateChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void KviMainWindow::activeConnectionServerInfoChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void KviMainWindow::activeConnectionLagChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}

// SIGNAL 8
void KviMainWindow::activeWindowSelectionStateChanged(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
