/****************************************************************************
** Meta object code from reading C++ file 'KviMenuBar.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../ui/KviMenuBar.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'KviMenuBar.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_KviMenuBar_t {
    QByteArrayData data[23];
    char stringdata0[365];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_KviMenuBar_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_KviMenuBar_t qt_meta_stringdata_KviMenuBar = {
    {
QT_MOC_LITERAL(0, 0, 10), // "KviMenuBar"
QT_MOC_LITERAL(1, 11, 13), // "menuDestroyed"
QT_MOC_LITERAL(2, 25, 0), // ""
QT_MOC_LITERAL(3, 26, 14), // "setupMainPopup"
QT_MOC_LITERAL(4, 41, 6), // "QMenu*"
QT_MOC_LITERAL(5, 48, 3), // "pop"
QT_MOC_LITERAL(6, 52, 18), // "setupSettingsPopup"
QT_MOC_LITERAL(7, 71, 14), // "setupHelpPopup"
QT_MOC_LITERAL(8, 86, 19), // "setupScriptingPopup"
QT_MOC_LITERAL(9, 106, 15), // "setupToolsPopup"
QT_MOC_LITERAL(10, 122, 15), // "updateMainPopup"
QT_MOC_LITERAL(11, 138, 19), // "updateSettingsPopup"
QT_MOC_LITERAL(12, 158, 24), // "updateRecentServersPopup"
QT_MOC_LITERAL(13, 183, 19), // "updateToolbarsPopup"
QT_MOC_LITERAL(14, 203, 23), // "updateModulesToolsPopup"
QT_MOC_LITERAL(15, 227, 23), // "updateActionsToolsPopup"
QT_MOC_LITERAL(16, 251, 16), // "updateToolsPopup"
QT_MOC_LITERAL(17, 268, 21), // "newConnectionToServer"
QT_MOC_LITERAL(18, 290, 8), // "QAction*"
QT_MOC_LITERAL(19, 299, 7), // "pAction"
QT_MOC_LITERAL(20, 307, 21), // "modulesToolsTriggered"
QT_MOC_LITERAL(21, 329, 15), // "actionTriggered"
QT_MOC_LITERAL(22, 345, 19) // "actionTriggeredBool"

    },
    "KviMenuBar\0menuDestroyed\0\0setupMainPopup\0"
    "QMenu*\0pop\0setupSettingsPopup\0"
    "setupHelpPopup\0setupScriptingPopup\0"
    "setupToolsPopup\0updateMainPopup\0"
    "updateSettingsPopup\0updateRecentServersPopup\0"
    "updateToolbarsPopup\0updateModulesToolsPopup\0"
    "updateActionsToolsPopup\0updateToolsPopup\0"
    "newConnectionToServer\0QAction*\0pAction\0"
    "modulesToolsTriggered\0actionTriggered\0"
    "actionTriggeredBool"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_KviMenuBar[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      22,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  124,    2, 0x09 /* Protected */,
       3,    1,  125,    2, 0x09 /* Protected */,
       3,    0,  128,    2, 0x29 /* Protected | MethodCloned */,
       6,    1,  129,    2, 0x09 /* Protected */,
       6,    0,  132,    2, 0x29 /* Protected | MethodCloned */,
       7,    1,  133,    2, 0x09 /* Protected */,
       7,    0,  136,    2, 0x29 /* Protected | MethodCloned */,
       8,    1,  137,    2, 0x09 /* Protected */,
       8,    0,  140,    2, 0x29 /* Protected | MethodCloned */,
       9,    1,  141,    2, 0x09 /* Protected */,
       9,    0,  144,    2, 0x29 /* Protected | MethodCloned */,
      10,    0,  145,    2, 0x09 /* Protected */,
      11,    0,  146,    2, 0x09 /* Protected */,
      12,    0,  147,    2, 0x09 /* Protected */,
      13,    0,  148,    2, 0x09 /* Protected */,
      14,    0,  149,    2, 0x09 /* Protected */,
      15,    0,  150,    2, 0x09 /* Protected */,
      16,    0,  151,    2, 0x09 /* Protected */,
      17,    1,  152,    2, 0x09 /* Protected */,
      20,    1,  155,    2, 0x09 /* Protected */,
      21,    1,  158,    2, 0x09 /* Protected */,
      22,    1,  161,    2, 0x09 /* Protected */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    5,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    5,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    5,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    5,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    5,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 18,   19,
    QMetaType::Void, 0x80000000 | 18,   19,
    QMetaType::Void, 0x80000000 | 18,   19,
    QMetaType::Void, QMetaType::Bool,    2,

       0        // eod
};

void KviMenuBar::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<KviMenuBar *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->menuDestroyed(); break;
        case 1: _t->setupMainPopup((*reinterpret_cast< QMenu*(*)>(_a[1]))); break;
        case 2: _t->setupMainPopup(); break;
        case 3: _t->setupSettingsPopup((*reinterpret_cast< QMenu*(*)>(_a[1]))); break;
        case 4: _t->setupSettingsPopup(); break;
        case 5: _t->setupHelpPopup((*reinterpret_cast< QMenu*(*)>(_a[1]))); break;
        case 6: _t->setupHelpPopup(); break;
        case 7: _t->setupScriptingPopup((*reinterpret_cast< QMenu*(*)>(_a[1]))); break;
        case 8: _t->setupScriptingPopup(); break;
        case 9: _t->setupToolsPopup((*reinterpret_cast< QMenu*(*)>(_a[1]))); break;
        case 10: _t->setupToolsPopup(); break;
        case 11: _t->updateMainPopup(); break;
        case 12: _t->updateSettingsPopup(); break;
        case 13: _t->updateRecentServersPopup(); break;
        case 14: _t->updateToolbarsPopup(); break;
        case 15: _t->updateModulesToolsPopup(); break;
        case 16: _t->updateActionsToolsPopup(); break;
        case 17: _t->updateToolsPopup(); break;
        case 18: _t->newConnectionToServer((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 19: _t->modulesToolsTriggered((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 20: _t->actionTriggered((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 21: _t->actionTriggeredBool((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QMenu* >(); break;
            }
            break;
        case 3:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QMenu* >(); break;
            }
            break;
        case 5:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QMenu* >(); break;
            }
            break;
        case 7:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QMenu* >(); break;
            }
            break;
        case 9:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QMenu* >(); break;
            }
            break;
        case 18:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAction* >(); break;
            }
            break;
        case 19:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAction* >(); break;
            }
            break;
        case 20:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAction* >(); break;
            }
            break;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject KviMenuBar::staticMetaObject = { {
    &KviTalMenuBar::staticMetaObject,
    qt_meta_stringdata_KviMenuBar.data,
    qt_meta_data_KviMenuBar,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *KviMenuBar::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *KviMenuBar::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_KviMenuBar.stringdata0))
        return static_cast<void*>(this);
    return KviTalMenuBar::qt_metacast(_clname);
}

int KviMenuBar::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KviTalMenuBar::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 22)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 22;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 22)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 22;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
