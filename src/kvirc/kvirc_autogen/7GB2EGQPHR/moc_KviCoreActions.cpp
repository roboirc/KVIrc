/****************************************************************************
** Meta object code from reading C++ file 'KviCoreActions.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../kernel/KviCoreActions.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'KviCoreActions.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_KviConnectAction_t {
    QByteArrayData data[1];
    char stringdata0[17];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_KviConnectAction_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_KviConnectAction_t qt_meta_stringdata_KviConnectAction = {
    {
QT_MOC_LITERAL(0, 0, 16) // "KviConnectAction"

    },
    "KviConnectAction"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_KviConnectAction[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void KviConnectAction::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject KviConnectAction::staticMetaObject = { {
    &KviAction::staticMetaObject,
    qt_meta_stringdata_KviConnectAction.data,
    qt_meta_data_KviConnectAction,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *KviConnectAction::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *KviConnectAction::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_KviConnectAction.stringdata0))
        return static_cast<void*>(this);
    return KviAction::qt_metacast(_clname);
}

int KviConnectAction::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KviAction::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_KviSeparatorAction_t {
    QByteArrayData data[1];
    char stringdata0[19];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_KviSeparatorAction_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_KviSeparatorAction_t qt_meta_stringdata_KviSeparatorAction = {
    {
QT_MOC_LITERAL(0, 0, 18) // "KviSeparatorAction"

    },
    "KviSeparatorAction"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_KviSeparatorAction[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void KviSeparatorAction::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject KviSeparatorAction::staticMetaObject = { {
    &KviAction::staticMetaObject,
    qt_meta_stringdata_KviSeparatorAction.data,
    qt_meta_data_KviSeparatorAction,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *KviSeparatorAction::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *KviSeparatorAction::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_KviSeparatorAction.stringdata0))
        return static_cast<void*>(this);
    return KviAction::qt_metacast(_clname);
}

int KviSeparatorAction::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KviAction::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_KviSubmenuAction_t {
    QByteArrayData data[6];
    char stringdata0[67];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_KviSubmenuAction_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_KviSubmenuAction_t qt_meta_stringdata_KviSubmenuAction = {
    {
QT_MOC_LITERAL(0, 0, 16), // "KviSubmenuAction"
QT_MOC_LITERAL(1, 17, 16), // "popupAboutToShow"
QT_MOC_LITERAL(2, 34, 0), // ""
QT_MOC_LITERAL(3, 35, 14), // "popupActivated"
QT_MOC_LITERAL(4, 50, 8), // "QAction*"
QT_MOC_LITERAL(5, 59, 7) // "pAction"

    },
    "KviSubmenuAction\0popupAboutToShow\0\0"
    "popupActivated\0QAction*\0pAction"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_KviSubmenuAction[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   24,    2, 0x09 /* Protected */,
       3,    1,   25,    2, 0x09 /* Protected */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    5,

       0        // eod
};

void KviSubmenuAction::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<KviSubmenuAction *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->popupAboutToShow(); break;
        case 1: _t->popupActivated((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAction* >(); break;
            }
            break;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject KviSubmenuAction::staticMetaObject = { {
    &KviKvsAction::staticMetaObject,
    qt_meta_stringdata_KviSubmenuAction.data,
    qt_meta_data_KviSubmenuAction,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *KviSubmenuAction::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *KviSubmenuAction::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_KviSubmenuAction.stringdata0))
        return static_cast<void*>(this);
    return KviKvsAction::qt_metacast(_clname);
}

int KviSubmenuAction::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KviKvsAction::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
struct qt_meta_stringdata_KviJoinChannelAction_t {
    QByteArrayData data[6];
    char stringdata0[71];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_KviJoinChannelAction_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_KviJoinChannelAction_t qt_meta_stringdata_KviJoinChannelAction = {
    {
QT_MOC_LITERAL(0, 0, 20), // "KviJoinChannelAction"
QT_MOC_LITERAL(1, 21, 16), // "popupAboutToShow"
QT_MOC_LITERAL(2, 38, 0), // ""
QT_MOC_LITERAL(3, 39, 14), // "popupActivated"
QT_MOC_LITERAL(4, 54, 8), // "QAction*"
QT_MOC_LITERAL(5, 63, 7) // "pAction"

    },
    "KviJoinChannelAction\0popupAboutToShow\0"
    "\0popupActivated\0QAction*\0pAction"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_KviJoinChannelAction[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   24,    2, 0x09 /* Protected */,
       3,    1,   25,    2, 0x09 /* Protected */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    5,

       0        // eod
};

void KviJoinChannelAction::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<KviJoinChannelAction *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->popupAboutToShow(); break;
        case 1: _t->popupActivated((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAction* >(); break;
            }
            break;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject KviJoinChannelAction::staticMetaObject = { {
    &KviSubmenuAction::staticMetaObject,
    qt_meta_stringdata_KviJoinChannelAction.data,
    qt_meta_data_KviJoinChannelAction,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *KviJoinChannelAction::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *KviJoinChannelAction::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_KviJoinChannelAction.stringdata0))
        return static_cast<void*>(this);
    return KviSubmenuAction::qt_metacast(_clname);
}

int KviJoinChannelAction::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KviSubmenuAction::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
struct qt_meta_stringdata_KviChangeNickAction_t {
    QByteArrayData data[6];
    char stringdata0[70];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_KviChangeNickAction_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_KviChangeNickAction_t qt_meta_stringdata_KviChangeNickAction = {
    {
QT_MOC_LITERAL(0, 0, 19), // "KviChangeNickAction"
QT_MOC_LITERAL(1, 20, 16), // "popupAboutToShow"
QT_MOC_LITERAL(2, 37, 0), // ""
QT_MOC_LITERAL(3, 38, 14), // "popupActivated"
QT_MOC_LITERAL(4, 53, 8), // "QAction*"
QT_MOC_LITERAL(5, 62, 7) // "pAction"

    },
    "KviChangeNickAction\0popupAboutToShow\0"
    "\0popupActivated\0QAction*\0pAction"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_KviChangeNickAction[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   24,    2, 0x09 /* Protected */,
       3,    1,   25,    2, 0x09 /* Protected */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    5,

       0        // eod
};

void KviChangeNickAction::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<KviChangeNickAction *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->popupAboutToShow(); break;
        case 1: _t->popupActivated((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAction* >(); break;
            }
            break;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject KviChangeNickAction::staticMetaObject = { {
    &KviSubmenuAction::staticMetaObject,
    qt_meta_stringdata_KviChangeNickAction.data,
    qt_meta_data_KviChangeNickAction,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *KviChangeNickAction::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *KviChangeNickAction::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_KviChangeNickAction.stringdata0))
        return static_cast<void*>(this);
    return KviSubmenuAction::qt_metacast(_clname);
}

int KviChangeNickAction::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KviSubmenuAction::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
struct qt_meta_stringdata_KviConnectToServerAction_t {
    QByteArrayData data[6];
    char stringdata0[75];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_KviConnectToServerAction_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_KviConnectToServerAction_t qt_meta_stringdata_KviConnectToServerAction = {
    {
QT_MOC_LITERAL(0, 0, 24), // "KviConnectToServerAction"
QT_MOC_LITERAL(1, 25, 16), // "popupAboutToShow"
QT_MOC_LITERAL(2, 42, 0), // ""
QT_MOC_LITERAL(3, 43, 14), // "popupActivated"
QT_MOC_LITERAL(4, 58, 8), // "QAction*"
QT_MOC_LITERAL(5, 67, 7) // "pAction"

    },
    "KviConnectToServerAction\0popupAboutToShow\0"
    "\0popupActivated\0QAction*\0pAction"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_KviConnectToServerAction[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   24,    2, 0x09 /* Protected */,
       3,    1,   25,    2, 0x09 /* Protected */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    5,

       0        // eod
};

void KviConnectToServerAction::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<KviConnectToServerAction *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->popupAboutToShow(); break;
        case 1: _t->popupActivated((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAction* >(); break;
            }
            break;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject KviConnectToServerAction::staticMetaObject = { {
    &KviSubmenuAction::staticMetaObject,
    qt_meta_stringdata_KviConnectToServerAction.data,
    qt_meta_data_KviConnectToServerAction,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *KviConnectToServerAction::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *KviConnectToServerAction::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_KviConnectToServerAction.stringdata0))
        return static_cast<void*>(this);
    return KviSubmenuAction::qt_metacast(_clname);
}

int KviConnectToServerAction::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KviSubmenuAction::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
struct qt_meta_stringdata_KviChangeUserModeAction_t {
    QByteArrayData data[6];
    char stringdata0[74];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_KviChangeUserModeAction_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_KviChangeUserModeAction_t qt_meta_stringdata_KviChangeUserModeAction = {
    {
QT_MOC_LITERAL(0, 0, 23), // "KviChangeUserModeAction"
QT_MOC_LITERAL(1, 24, 16), // "popupAboutToShow"
QT_MOC_LITERAL(2, 41, 0), // ""
QT_MOC_LITERAL(3, 42, 14), // "popupActivated"
QT_MOC_LITERAL(4, 57, 8), // "QAction*"
QT_MOC_LITERAL(5, 66, 7) // "pAction"

    },
    "KviChangeUserModeAction\0popupAboutToShow\0"
    "\0popupActivated\0QAction*\0pAction"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_KviChangeUserModeAction[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   24,    2, 0x09 /* Protected */,
       3,    1,   25,    2, 0x09 /* Protected */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    5,

       0        // eod
};

void KviChangeUserModeAction::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<KviChangeUserModeAction *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->popupAboutToShow(); break;
        case 1: _t->popupActivated((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAction* >(); break;
            }
            break;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject KviChangeUserModeAction::staticMetaObject = { {
    &KviSubmenuAction::staticMetaObject,
    qt_meta_stringdata_KviChangeUserModeAction.data,
    qt_meta_data_KviChangeUserModeAction,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *KviChangeUserModeAction::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *KviChangeUserModeAction::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_KviChangeUserModeAction.stringdata0))
        return static_cast<void*>(this);
    return KviSubmenuAction::qt_metacast(_clname);
}

int KviChangeUserModeAction::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KviSubmenuAction::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
struct qt_meta_stringdata_KviIrcToolsAction_t {
    QByteArrayData data[6];
    char stringdata0[68];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_KviIrcToolsAction_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_KviIrcToolsAction_t qt_meta_stringdata_KviIrcToolsAction = {
    {
QT_MOC_LITERAL(0, 0, 17), // "KviIrcToolsAction"
QT_MOC_LITERAL(1, 18, 16), // "popupAboutToShow"
QT_MOC_LITERAL(2, 35, 0), // ""
QT_MOC_LITERAL(3, 36, 14), // "popupActivated"
QT_MOC_LITERAL(4, 51, 8), // "QAction*"
QT_MOC_LITERAL(5, 60, 7) // "pAction"

    },
    "KviIrcToolsAction\0popupAboutToShow\0\0"
    "popupActivated\0QAction*\0pAction"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_KviIrcToolsAction[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   24,    2, 0x09 /* Protected */,
       3,    1,   25,    2, 0x09 /* Protected */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    5,

       0        // eod
};

void KviIrcToolsAction::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<KviIrcToolsAction *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->popupAboutToShow(); break;
        case 1: _t->popupActivated((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAction* >(); break;
            }
            break;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject KviIrcToolsAction::staticMetaObject = { {
    &KviSubmenuAction::staticMetaObject,
    qt_meta_stringdata_KviIrcToolsAction.data,
    qt_meta_data_KviIrcToolsAction,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *KviIrcToolsAction::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *KviIrcToolsAction::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_KviIrcToolsAction.stringdata0))
        return static_cast<void*>(this);
    return KviSubmenuAction::qt_metacast(_clname);
}

int KviIrcToolsAction::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KviSubmenuAction::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
struct qt_meta_stringdata_KviIrcOperationsAction_t {
    QByteArrayData data[6];
    char stringdata0[73];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_KviIrcOperationsAction_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_KviIrcOperationsAction_t qt_meta_stringdata_KviIrcOperationsAction = {
    {
QT_MOC_LITERAL(0, 0, 22), // "KviIrcOperationsAction"
QT_MOC_LITERAL(1, 23, 16), // "popupAboutToShow"
QT_MOC_LITERAL(2, 40, 0), // ""
QT_MOC_LITERAL(3, 41, 14), // "popupActivated"
QT_MOC_LITERAL(4, 56, 8), // "QAction*"
QT_MOC_LITERAL(5, 65, 7) // "pAction"

    },
    "KviIrcOperationsAction\0popupAboutToShow\0"
    "\0popupActivated\0QAction*\0pAction"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_KviIrcOperationsAction[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   24,    2, 0x09 /* Protected */,
       3,    1,   25,    2, 0x09 /* Protected */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    5,

       0        // eod
};

void KviIrcOperationsAction::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<KviIrcOperationsAction *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->popupAboutToShow(); break;
        case 1: _t->popupActivated((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAction* >(); break;
            }
            break;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject KviIrcOperationsAction::staticMetaObject = { {
    &KviSubmenuAction::staticMetaObject,
    qt_meta_stringdata_KviIrcOperationsAction.data,
    qt_meta_data_KviIrcOperationsAction,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *KviIrcOperationsAction::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *KviIrcOperationsAction::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_KviIrcOperationsAction.stringdata0))
        return static_cast<void*>(this);
    return KviSubmenuAction::qt_metacast(_clname);
}

int KviIrcOperationsAction::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KviSubmenuAction::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
struct qt_meta_stringdata_KviIrcContextDisplayAction_t {
    QByteArrayData data[1];
    char stringdata0[27];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_KviIrcContextDisplayAction_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_KviIrcContextDisplayAction_t qt_meta_stringdata_KviIrcContextDisplayAction = {
    {
QT_MOC_LITERAL(0, 0, 26) // "KviIrcContextDisplayAction"

    },
    "KviIrcContextDisplayAction"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_KviIrcContextDisplayAction[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void KviIrcContextDisplayAction::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject KviIrcContextDisplayAction::staticMetaObject = { {
    &KviAction::staticMetaObject,
    qt_meta_stringdata_KviIrcContextDisplayAction.data,
    qt_meta_data_KviIrcContextDisplayAction,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *KviIrcContextDisplayAction::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *KviIrcContextDisplayAction::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_KviIrcContextDisplayAction.stringdata0))
        return static_cast<void*>(this);
    return KviAction::qt_metacast(_clname);
}

int KviIrcContextDisplayAction::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KviAction::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_KviGoAwayAction_t {
    QByteArrayData data[1];
    char stringdata0[16];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_KviGoAwayAction_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_KviGoAwayAction_t qt_meta_stringdata_KviGoAwayAction = {
    {
QT_MOC_LITERAL(0, 0, 15) // "KviGoAwayAction"

    },
    "KviGoAwayAction"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_KviGoAwayAction[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void KviGoAwayAction::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject KviGoAwayAction::staticMetaObject = { {
    &KviKvsAction::staticMetaObject,
    qt_meta_stringdata_KviGoAwayAction.data,
    qt_meta_data_KviGoAwayAction,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *KviGoAwayAction::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *KviGoAwayAction::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_KviGoAwayAction.stringdata0))
        return static_cast<void*>(this);
    return KviKvsAction::qt_metacast(_clname);
}

int KviGoAwayAction::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KviKvsAction::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
