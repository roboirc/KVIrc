//=============================================================================
//
//   File : KviColorSelectionWindow.cpp
//   Creation date : Wed Jan  6 1999 04:30:20 by Szymon Stefanek
//
//   This file is part of the KVIrc IRC client distribution
//   Copyright (C) 1999-2010 Szymon Stefanek (pragma at kvirc dot net)
//
//   This program is FREE software. You can redistribute it and/or
//   modify it under the terms of the GNU General Public License
//   as published by the Free Software Foundation; either version 2
//   of the License, or (at your option) any later version.
//
//   This program is distributed in the HOPE that it will be USEFUL,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//   See the GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program. If not, write to the Free Software Foundation,
//   Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//
//=============================================================================

#include "KviColorSelectionWindow.h"
#include "KviApplication.h"
#include "KviOptions.h"

#include <QPainter>
#include <QString>
#include <QEvent>
#include <QMouseEvent>
#include <QDebug>

//roboirc 18 to 30
int unit = 30;
int items_per_row = 25;
int max = 16;
int number_of_rows = 4;
int mirc_max_color = 99;

KviColorWindow::KviColorWindow() : QWidget(nullptr)
{
    setObjectName("toplevel_color_window");
	setWindowFlags(Qt::Popup);
	setFocusPolicy(Qt::NoFocus);
	//setFixedSize(144, 36);
    int width = unit*items_per_row;
    int height = unit*number_of_rows;

    setFixedSize(width, height);


	m_pOwner = nullptr;
	QFont fnt = QFont();
	fnt.setStyleHint(QFont::TypeWriter);
	fnt.setPointSize(10);
	setFont(fnt);
	m_iTimerId = -1;
}

KviColorWindow::~KviColorWindow()
{
	if(m_iTimerId != -1)
		killTimer(m_iTimerId);
	//if(m_pOwner)m_pOwner->setFocus();
}

void KviColorWindow::popup(QWidget * pOwner)
{
	m_pOwner = pOwner;
	show();
}

//roboirc
void KviColorWindow::paintEvent(QPaintEvent *)
{
    // Black = 1, White = 0
    //roboirc
    //static int clrIdx[16] = { 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1 };
	QPainter p(this);
    int Black_Labels_Only[] = {0, 8, 9, 11, 15, 54, 55, 56, 57, 58, 65, 66, 67, 68, 69, 70, 77, 78, 79, 80, 81, 82, 97, 98};
    int size_of_Black_Labels_Only = sizeof(Black_Labels_Only)/sizeof(Black_Labels_Only[0]);

	for(int i = 0; i < mirc_max_color; i++)
	{

            // Draws colour boxes
            QRectF qRectF((i % items_per_row) * unit, (i / items_per_row) * unit, unit, unit);
            p.fillRect(qRectF, getMircColor(i));
            int temp = i;

            for(int j = 0; j<size_of_Black_Labels_Only; j++)
            {
                if(std::find(Black_Labels_Only, std::end(Black_Labels_Only), i)!=std::end(Black_Labels_Only))
                {
                    //qDebug() << "Found " + QString::number(i);
                    p.setPen(QColor("#000000"));
                    KviCString szI(KviCString::Format, "%d", temp);
                    p.drawText((temp % items_per_row) * unit, (temp / items_per_row) * unit, unit, unit, Qt::AlignVCenter | Qt::AlignHCenter, szI.ptr());
                }
                else
                {
                    p.setPen(QColor("#FFFFFF"));
                    KviCString szI(KviCString::Format, "%d", temp);
                    p.drawText((temp % items_per_row) * unit, (temp / items_per_row) * unit, unit, unit, Qt::AlignVCenter | Qt::AlignHCenter, szI.ptr());
                }

            }

            // Sets colour of text inside the colour boxes
            //p.setPen(KVI_OPTION_MIRCCOLOR(clrIdx[i]));



	}

}

//roboirc
void KviColorWindow::mousePressEvent(QMouseEvent * e)
{

	QString szStr;
	if(!((e->pos().x() < 0) || (e->pos().x() > width()) || (e->pos().y() < 0) || (e->pos().y() > height())))
	{
	    // This checks the value of x position of the mouse click
		int iKey = 0;

        for(int i = 0; i <= KVI_EXTCOLOR_MAX; i++)
        {
                // Draws colour boxes
                QRectF qRectF((i % items_per_row) * unit, (i / items_per_row) * unit, unit, unit);
                if (qRectF.contains(e->pos().x(), e->pos().y()))
                {
                    iKey = i;
                }
        }

		// Can make a loop that checks each of the boxes to see if they contain that mouse click x,y coordinate
		// and to use that number from counter

        //float x_remainder = e->x() % unit;
		//float y_remainder = e->y() % unit;

		//qDebug() << "y_remainder: " + QString::number(y_remainder);
        //qDebug() << "x_remainder: " + QString::number(x_remainder);

		/*
		if(e->x() < unit*2 && e->y() > unit)
			iKey += 8;
		if(e->x() > unit*2 && e->y() > unit)
			iKey -= 2;
        */

		//// FIXME: is this right? maybe it should be szStr.setNum(iAscii);
		//szStr.setNum(0);

		/*
		if((e->x() > unit*2) && (e->y() > unit))
		{
			if(m_pOwner)
				g_pApp->sendEvent(m_pOwner, new QKeyEvent(QEvent::KeyPress, Qt::Key_1, Qt::NoModifier, "1"));
				g_pApp->sendEvent(m_pOwner, new QKeyEvent(QEvent::KeyPress, Qt::Key_1, Qt::NoModifier, "1"));
		}
		*/

        szStr.setNum(iKey);

        // This outputs the string
		if(m_pOwner)
			g_pApp->sendEvent(m_pOwner, new QKeyEvent(QEvent::KeyPress, iKey, (Qt::KeyboardModifiers)Qt::NoModifier, szStr));
	}

	if(m_iTimerId != -1)
	{
        killTimer(m_iTimerId);
    }
	hide();
}

void KviColorWindow::keyPressEvent(QKeyEvent * e)
{
    if(m_iTimerId != -1)
    {
        killTimer(m_iTimerId);
        m_iTimerId = -1;
    }
    hide();
    if(m_pOwner)
        g_pApp->sendEvent(m_pOwner, e);
}


void KviColorWindow::show()
{

	m_iTimerId = startTimer(10000); //10 sec ...seems enough
	QWidget::show();
}

void KviColorWindow::timerEvent(QTimerEvent *)
{
	if(m_iTimerId != -1)
		killTimer(m_iTimerId);
	hide();

	//roboirc
    if(m_iTimerId != -1)
        killTimer(m_iTimerId);

}
