/****************************************************************************
** Meta object code from reading C++ file 'PackAddonDialog.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../PackAddonDialog.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'PackAddonDialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_PackAddonDialog_t {
    QByteArrayData data[1];
    char stringdata0[16];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PackAddonDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PackAddonDialog_t qt_meta_stringdata_PackAddonDialog = {
    {
QT_MOC_LITERAL(0, 0, 15) // "PackAddonDialog"

    },
    "PackAddonDialog"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PackAddonDialog[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void PackAddonDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject PackAddonDialog::staticMetaObject = { {
    &QWizard::staticMetaObject,
    qt_meta_stringdata_PackAddonDialog.data,
    qt_meta_data_PackAddonDialog,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *PackAddonDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PackAddonDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PackAddonDialog.stringdata0))
        return static_cast<void*>(this);
    return QWizard::qt_metacast(_clname);
}

int PackAddonDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWizard::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_PackAddonInfoPackageWidget_t {
    QByteArrayData data[1];
    char stringdata0[27];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PackAddonInfoPackageWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PackAddonInfoPackageWidget_t qt_meta_stringdata_PackAddonInfoPackageWidget = {
    {
QT_MOC_LITERAL(0, 0, 26) // "PackAddonInfoPackageWidget"

    },
    "PackAddonInfoPackageWidget"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PackAddonInfoPackageWidget[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void PackAddonInfoPackageWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject PackAddonInfoPackageWidget::staticMetaObject = { {
    &QWizardPage::staticMetaObject,
    qt_meta_stringdata_PackAddonInfoPackageWidget.data,
    qt_meta_data_PackAddonInfoPackageWidget,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *PackAddonInfoPackageWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PackAddonInfoPackageWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PackAddonInfoPackageWidget.stringdata0))
        return static_cast<void*>(this);
    return QWizardPage::qt_metacast(_clname);
}

int PackAddonInfoPackageWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWizardPage::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_PackAddonFileSelectionWidget_t {
    QByteArrayData data[1];
    char stringdata0[29];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PackAddonFileSelectionWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PackAddonFileSelectionWidget_t qt_meta_stringdata_PackAddonFileSelectionWidget = {
    {
QT_MOC_LITERAL(0, 0, 28) // "PackAddonFileSelectionWidget"

    },
    "PackAddonFileSelectionWidget"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PackAddonFileSelectionWidget[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void PackAddonFileSelectionWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject PackAddonFileSelectionWidget::staticMetaObject = { {
    &QWizardPage::staticMetaObject,
    qt_meta_stringdata_PackAddonFileSelectionWidget.data,
    qt_meta_data_PackAddonFileSelectionWidget,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *PackAddonFileSelectionWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PackAddonFileSelectionWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PackAddonFileSelectionWidget.stringdata0))
        return static_cast<void*>(this);
    return QWizardPage::qt_metacast(_clname);
}

int PackAddonFileSelectionWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWizardPage::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_PackAddonSaveSelectionWidget_t {
    QByteArrayData data[1];
    char stringdata0[29];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PackAddonSaveSelectionWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PackAddonSaveSelectionWidget_t qt_meta_stringdata_PackAddonSaveSelectionWidget = {
    {
QT_MOC_LITERAL(0, 0, 28) // "PackAddonSaveSelectionWidget"

    },
    "PackAddonSaveSelectionWidget"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PackAddonSaveSelectionWidget[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void PackAddonSaveSelectionWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject PackAddonSaveSelectionWidget::staticMetaObject = { {
    &QWizardPage::staticMetaObject,
    qt_meta_stringdata_PackAddonSaveSelectionWidget.data,
    qt_meta_data_PackAddonSaveSelectionWidget,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *PackAddonSaveSelectionWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PackAddonSaveSelectionWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PackAddonSaveSelectionWidget.stringdata0))
        return static_cast<void*>(this);
    return QWizardPage::qt_metacast(_clname);
}

int PackAddonSaveSelectionWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWizardPage::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_PackAddonSummaryInfoWidget_t {
    QByteArrayData data[1];
    char stringdata0[27];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PackAddonSummaryInfoWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PackAddonSummaryInfoWidget_t qt_meta_stringdata_PackAddonSummaryInfoWidget = {
    {
QT_MOC_LITERAL(0, 0, 26) // "PackAddonSummaryInfoWidget"

    },
    "PackAddonSummaryInfoWidget"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PackAddonSummaryInfoWidget[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void PackAddonSummaryInfoWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject PackAddonSummaryInfoWidget::staticMetaObject = { {
    &QWizardPage::staticMetaObject,
    qt_meta_stringdata_PackAddonSummaryInfoWidget.data,
    qt_meta_data_PackAddonSummaryInfoWidget,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *PackAddonSummaryInfoWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PackAddonSummaryInfoWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PackAddonSummaryInfoWidget.stringdata0))
        return static_cast<void*>(this);
    return QWizardPage::qt_metacast(_clname);
}

int PackAddonSummaryInfoWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWizardPage::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_PackAddonSummaryFilesWidget_t {
    QByteArrayData data[4];
    char stringdata0[43];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PackAddonSummaryFilesWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PackAddonSummaryFilesWidget_t qt_meta_stringdata_PackAddonSummaryFilesWidget = {
    {
QT_MOC_LITERAL(0, 0, 27), // "PackAddonSummaryFilesWidget"
QT_MOC_LITERAL(1, 28, 6), // "accept"
QT_MOC_LITERAL(2, 35, 0), // ""
QT_MOC_LITERAL(3, 36, 6) // "reject"

    },
    "PackAddonSummaryFilesWidget\0accept\0\0"
    "reject"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PackAddonSummaryFilesWidget[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   24,    2, 0x09 /* Protected */,
       3,    0,   25,    2, 0x09 /* Protected */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void PackAddonSummaryFilesWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<PackAddonSummaryFilesWidget *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->accept(); break;
        case 1: _t->reject(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject PackAddonSummaryFilesWidget::staticMetaObject = { {
    &QDialog::staticMetaObject,
    qt_meta_stringdata_PackAddonSummaryFilesWidget.data,
    qt_meta_data_PackAddonSummaryFilesWidget,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *PackAddonSummaryFilesWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PackAddonSummaryFilesWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PackAddonSummaryFilesWidget.stringdata0))
        return static_cast<void*>(this);
    return QDialog::qt_metacast(_clname);
}

int PackAddonSummaryFilesWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
