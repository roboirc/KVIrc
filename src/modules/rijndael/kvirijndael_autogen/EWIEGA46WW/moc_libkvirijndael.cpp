/****************************************************************************
** Meta object code from reading C++ file 'libkvirijndael.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../libkvirijndael.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'libkvirijndael.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_KviRijndaelEngine_t {
    QByteArrayData data[1];
    char stringdata0[18];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_KviRijndaelEngine_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_KviRijndaelEngine_t qt_meta_stringdata_KviRijndaelEngine = {
    {
QT_MOC_LITERAL(0, 0, 17) // "KviRijndaelEngine"

    },
    "KviRijndaelEngine"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_KviRijndaelEngine[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void KviRijndaelEngine::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject KviRijndaelEngine::staticMetaObject = { {
    &KviCryptEngine::staticMetaObject,
    qt_meta_stringdata_KviRijndaelEngine.data,
    qt_meta_data_KviRijndaelEngine,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *KviRijndaelEngine::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *KviRijndaelEngine::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_KviRijndaelEngine.stringdata0))
        return static_cast<void*>(this);
    return KviCryptEngine::qt_metacast(_clname);
}

int KviRijndaelEngine::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KviCryptEngine::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_KviRijndaelHexEngine_t {
    QByteArrayData data[1];
    char stringdata0[21];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_KviRijndaelHexEngine_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_KviRijndaelHexEngine_t qt_meta_stringdata_KviRijndaelHexEngine = {
    {
QT_MOC_LITERAL(0, 0, 20) // "KviRijndaelHexEngine"

    },
    "KviRijndaelHexEngine"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_KviRijndaelHexEngine[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void KviRijndaelHexEngine::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject KviRijndaelHexEngine::staticMetaObject = { {
    &KviRijndaelEngine::staticMetaObject,
    qt_meta_stringdata_KviRijndaelHexEngine.data,
    qt_meta_data_KviRijndaelHexEngine,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *KviRijndaelHexEngine::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *KviRijndaelHexEngine::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_KviRijndaelHexEngine.stringdata0))
        return static_cast<void*>(this);
    return KviRijndaelEngine::qt_metacast(_clname);
}

int KviRijndaelHexEngine::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KviRijndaelEngine::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_KviRijndael128HexEngine_t {
    QByteArrayData data[1];
    char stringdata0[24];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_KviRijndael128HexEngine_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_KviRijndael128HexEngine_t qt_meta_stringdata_KviRijndael128HexEngine = {
    {
QT_MOC_LITERAL(0, 0, 23) // "KviRijndael128HexEngine"

    },
    "KviRijndael128HexEngine"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_KviRijndael128HexEngine[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void KviRijndael128HexEngine::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject KviRijndael128HexEngine::staticMetaObject = { {
    &KviRijndaelHexEngine::staticMetaObject,
    qt_meta_stringdata_KviRijndael128HexEngine.data,
    qt_meta_data_KviRijndael128HexEngine,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *KviRijndael128HexEngine::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *KviRijndael128HexEngine::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_KviRijndael128HexEngine.stringdata0))
        return static_cast<void*>(this);
    return KviRijndaelHexEngine::qt_metacast(_clname);
}

int KviRijndael128HexEngine::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KviRijndaelHexEngine::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_KviRijndael192HexEngine_t {
    QByteArrayData data[1];
    char stringdata0[24];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_KviRijndael192HexEngine_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_KviRijndael192HexEngine_t qt_meta_stringdata_KviRijndael192HexEngine = {
    {
QT_MOC_LITERAL(0, 0, 23) // "KviRijndael192HexEngine"

    },
    "KviRijndael192HexEngine"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_KviRijndael192HexEngine[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void KviRijndael192HexEngine::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject KviRijndael192HexEngine::staticMetaObject = { {
    &KviRijndaelHexEngine::staticMetaObject,
    qt_meta_stringdata_KviRijndael192HexEngine.data,
    qt_meta_data_KviRijndael192HexEngine,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *KviRijndael192HexEngine::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *KviRijndael192HexEngine::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_KviRijndael192HexEngine.stringdata0))
        return static_cast<void*>(this);
    return KviRijndaelHexEngine::qt_metacast(_clname);
}

int KviRijndael192HexEngine::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KviRijndaelHexEngine::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_KviRijndael256HexEngine_t {
    QByteArrayData data[1];
    char stringdata0[24];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_KviRijndael256HexEngine_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_KviRijndael256HexEngine_t qt_meta_stringdata_KviRijndael256HexEngine = {
    {
QT_MOC_LITERAL(0, 0, 23) // "KviRijndael256HexEngine"

    },
    "KviRijndael256HexEngine"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_KviRijndael256HexEngine[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void KviRijndael256HexEngine::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject KviRijndael256HexEngine::staticMetaObject = { {
    &KviRijndaelHexEngine::staticMetaObject,
    qt_meta_stringdata_KviRijndael256HexEngine.data,
    qt_meta_data_KviRijndael256HexEngine,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *KviRijndael256HexEngine::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *KviRijndael256HexEngine::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_KviRijndael256HexEngine.stringdata0))
        return static_cast<void*>(this);
    return KviRijndaelHexEngine::qt_metacast(_clname);
}

int KviRijndael256HexEngine::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KviRijndaelHexEngine::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_KviRijndaelBase64Engine_t {
    QByteArrayData data[1];
    char stringdata0[24];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_KviRijndaelBase64Engine_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_KviRijndaelBase64Engine_t qt_meta_stringdata_KviRijndaelBase64Engine = {
    {
QT_MOC_LITERAL(0, 0, 23) // "KviRijndaelBase64Engine"

    },
    "KviRijndaelBase64Engine"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_KviRijndaelBase64Engine[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void KviRijndaelBase64Engine::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject KviRijndaelBase64Engine::staticMetaObject = { {
    &KviRijndaelEngine::staticMetaObject,
    qt_meta_stringdata_KviRijndaelBase64Engine.data,
    qt_meta_data_KviRijndaelBase64Engine,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *KviRijndaelBase64Engine::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *KviRijndaelBase64Engine::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_KviRijndaelBase64Engine.stringdata0))
        return static_cast<void*>(this);
    return KviRijndaelEngine::qt_metacast(_clname);
}

int KviRijndaelBase64Engine::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KviRijndaelEngine::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_KviRijndael128Base64Engine_t {
    QByteArrayData data[1];
    char stringdata0[27];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_KviRijndael128Base64Engine_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_KviRijndael128Base64Engine_t qt_meta_stringdata_KviRijndael128Base64Engine = {
    {
QT_MOC_LITERAL(0, 0, 26) // "KviRijndael128Base64Engine"

    },
    "KviRijndael128Base64Engine"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_KviRijndael128Base64Engine[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void KviRijndael128Base64Engine::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject KviRijndael128Base64Engine::staticMetaObject = { {
    &KviRijndaelBase64Engine::staticMetaObject,
    qt_meta_stringdata_KviRijndael128Base64Engine.data,
    qt_meta_data_KviRijndael128Base64Engine,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *KviRijndael128Base64Engine::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *KviRijndael128Base64Engine::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_KviRijndael128Base64Engine.stringdata0))
        return static_cast<void*>(this);
    return KviRijndaelBase64Engine::qt_metacast(_clname);
}

int KviRijndael128Base64Engine::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KviRijndaelBase64Engine::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_KviRijndael192Base64Engine_t {
    QByteArrayData data[1];
    char stringdata0[27];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_KviRijndael192Base64Engine_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_KviRijndael192Base64Engine_t qt_meta_stringdata_KviRijndael192Base64Engine = {
    {
QT_MOC_LITERAL(0, 0, 26) // "KviRijndael192Base64Engine"

    },
    "KviRijndael192Base64Engine"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_KviRijndael192Base64Engine[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void KviRijndael192Base64Engine::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject KviRijndael192Base64Engine::staticMetaObject = { {
    &KviRijndaelBase64Engine::staticMetaObject,
    qt_meta_stringdata_KviRijndael192Base64Engine.data,
    qt_meta_data_KviRijndael192Base64Engine,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *KviRijndael192Base64Engine::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *KviRijndael192Base64Engine::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_KviRijndael192Base64Engine.stringdata0))
        return static_cast<void*>(this);
    return KviRijndaelBase64Engine::qt_metacast(_clname);
}

int KviRijndael192Base64Engine::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KviRijndaelBase64Engine::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_KviRijndael256Base64Engine_t {
    QByteArrayData data[1];
    char stringdata0[27];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_KviRijndael256Base64Engine_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_KviRijndael256Base64Engine_t qt_meta_stringdata_KviRijndael256Base64Engine = {
    {
QT_MOC_LITERAL(0, 0, 26) // "KviRijndael256Base64Engine"

    },
    "KviRijndael256Base64Engine"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_KviRijndael256Base64Engine[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void KviRijndael256Base64Engine::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject KviRijndael256Base64Engine::staticMetaObject = { {
    &KviRijndaelBase64Engine::staticMetaObject,
    qt_meta_stringdata_KviRijndael256Base64Engine.data,
    qt_meta_data_KviRijndael256Base64Engine,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *KviRijndael256Base64Engine::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *KviRijndael256Base64Engine::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_KviRijndael256Base64Engine.stringdata0))
        return static_cast<void*>(this);
    return KviRijndaelBase64Engine::qt_metacast(_clname);
}

int KviRijndael256Base64Engine::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KviRijndaelBase64Engine::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_KviMircryptionEngine_t {
    QByteArrayData data[1];
    char stringdata0[21];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_KviMircryptionEngine_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_KviMircryptionEngine_t qt_meta_stringdata_KviMircryptionEngine = {
    {
QT_MOC_LITERAL(0, 0, 20) // "KviMircryptionEngine"

    },
    "KviMircryptionEngine"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_KviMircryptionEngine[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void KviMircryptionEngine::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject KviMircryptionEngine::staticMetaObject = { {
    &KviCryptEngine::staticMetaObject,
    qt_meta_stringdata_KviMircryptionEngine.data,
    qt_meta_data_KviMircryptionEngine,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *KviMircryptionEngine::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *KviMircryptionEngine::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_KviMircryptionEngine.stringdata0))
        return static_cast<void*>(this);
    return KviCryptEngine::qt_metacast(_clname);
}

int KviMircryptionEngine::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KviCryptEngine::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
