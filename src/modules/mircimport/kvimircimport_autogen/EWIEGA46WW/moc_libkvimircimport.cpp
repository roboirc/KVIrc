/****************************************************************************
** Meta object code from reading C++ file 'libkvimircimport.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../libkvimircimport.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'libkvimircimport.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_KviMircServersIniImport_t {
    QByteArrayData data[1];
    char stringdata0[24];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_KviMircServersIniImport_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_KviMircServersIniImport_t qt_meta_stringdata_KviMircServersIniImport = {
    {
QT_MOC_LITERAL(0, 0, 23) // "KviMircServersIniImport"

    },
    "KviMircServersIniImport"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_KviMircServersIniImport[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void KviMircServersIniImport::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject KviMircServersIniImport::staticMetaObject = { {
    &KviMexServerImport::staticMetaObject,
    qt_meta_stringdata_KviMircServersIniImport.data,
    qt_meta_data_KviMircServersIniImport,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *KviMircServersIniImport::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *KviMircServersIniImport::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_KviMircServersIniImport.stringdata0))
        return static_cast<void*>(this);
    return KviMexServerImport::qt_metacast(_clname);
}

int KviMircServersIniImport::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KviMexServerImport::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_KviRemoteMircServersIniImport_t {
    QByteArrayData data[1];
    char stringdata0[30];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_KviRemoteMircServersIniImport_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_KviRemoteMircServersIniImport_t qt_meta_stringdata_KviRemoteMircServersIniImport = {
    {
QT_MOC_LITERAL(0, 0, 29) // "KviRemoteMircServersIniImport"

    },
    "KviRemoteMircServersIniImport"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_KviRemoteMircServersIniImport[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void KviRemoteMircServersIniImport::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject KviRemoteMircServersIniImport::staticMetaObject = { {
    &KviMircServersIniImport::staticMetaObject,
    qt_meta_stringdata_KviRemoteMircServersIniImport.data,
    qt_meta_data_KviRemoteMircServersIniImport,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *KviRemoteMircServersIniImport::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *KviRemoteMircServersIniImport::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_KviRemoteMircServersIniImport.stringdata0))
        return static_cast<void*>(this);
    return KviMircServersIniImport::qt_metacast(_clname);
}

int KviRemoteMircServersIniImport::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KviMircServersIniImport::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_KviRemoteMircServerImportWizard_t {
    QByteArrayData data[8];
    char stringdata0[102];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_KviRemoteMircServerImportWizard_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_KviRemoteMircServerImportWizard_t qt_meta_stringdata_KviRemoteMircServerImportWizard = {
    {
QT_MOC_LITERAL(0, 0, 31), // "KviRemoteMircServerImportWizard"
QT_MOC_LITERAL(1, 32, 14), // "getListMessage"
QT_MOC_LITERAL(2, 47, 0), // ""
QT_MOC_LITERAL(3, 48, 7), // "message"
QT_MOC_LITERAL(4, 56, 17), // "getListTerminated"
QT_MOC_LITERAL(5, 74, 8), // "bSuccess"
QT_MOC_LITERAL(6, 83, 12), // "pageSelected"
QT_MOC_LITERAL(7, 96, 5) // "title"

    },
    "KviRemoteMircServerImportWizard\0"
    "getListMessage\0\0message\0getListTerminated\0"
    "bSuccess\0pageSelected\0title"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_KviRemoteMircServerImportWizard[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   29,    2, 0x09 /* Protected */,
       4,    1,   32,    2, 0x09 /* Protected */,
       6,    1,   35,    2, 0x09 /* Protected */,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::Bool,    5,
    QMetaType::Void, QMetaType::QString,    7,

       0        // eod
};

void KviRemoteMircServerImportWizard::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<KviRemoteMircServerImportWizard *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->getListMessage((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->getListTerminated((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->pageSelected((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject KviRemoteMircServerImportWizard::staticMetaObject = { {
    &KviTalWizard::staticMetaObject,
    qt_meta_stringdata_KviRemoteMircServerImportWizard.data,
    qt_meta_data_KviRemoteMircServerImportWizard,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *KviRemoteMircServerImportWizard::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *KviRemoteMircServerImportWizard::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_KviRemoteMircServerImportWizard.stringdata0))
        return static_cast<void*>(this);
    return KviTalWizard::qt_metacast(_clname);
}

int KviRemoteMircServerImportWizard::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KviTalWizard::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
