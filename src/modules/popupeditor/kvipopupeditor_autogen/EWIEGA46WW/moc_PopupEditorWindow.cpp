/****************************************************************************
** Meta object code from reading C++ file 'PopupEditorWindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../PopupEditorWindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'PopupEditorWindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_SinglePopupEditor_t {
    QByteArrayData data[32];
    char stringdata0[587];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SinglePopupEditor_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SinglePopupEditor_t qt_meta_stringdata_SinglePopupEditor = {
    {
QT_MOC_LITERAL(0, 0, 17), // "SinglePopupEditor"
QT_MOC_LITERAL(1, 18, 10), // "contextCut"
QT_MOC_LITERAL(2, 29, 0), // ""
QT_MOC_LITERAL(3, 30, 11), // "contextCopy"
QT_MOC_LITERAL(4, 42, 13), // "contextRemove"
QT_MOC_LITERAL(5, 56, 17), // "contextPasteBelow"
QT_MOC_LITERAL(6, 74, 17), // "contextPasteAbove"
QT_MOC_LITERAL(7, 92, 18), // "contextPasteInside"
QT_MOC_LITERAL(8, 111, 24), // "contextNewSeparatorBelow"
QT_MOC_LITERAL(9, 136, 24), // "contextNewSeparatorAbove"
QT_MOC_LITERAL(10, 161, 25), // "contextNewSeparatorInside"
QT_MOC_LITERAL(11, 187, 19), // "contextNewItemBelow"
QT_MOC_LITERAL(12, 207, 19), // "contextNewItemAbove"
QT_MOC_LITERAL(13, 227, 20), // "contextNewItemInside"
QT_MOC_LITERAL(14, 248, 19), // "contextNewMenuBelow"
QT_MOC_LITERAL(15, 268, 19), // "contextNewMenuAbove"
QT_MOC_LITERAL(16, 288, 20), // "contextNewMenuInside"
QT_MOC_LITERAL(17, 309, 22), // "contextNewExtMenuBelow"
QT_MOC_LITERAL(18, 332, 22), // "contextNewExtMenuAbove"
QT_MOC_LITERAL(19, 355, 23), // "contextNewExtMenuInside"
QT_MOC_LITERAL(20, 379, 20), // "contextNewLabelBelow"
QT_MOC_LITERAL(21, 400, 20), // "contextNewLabelAbove"
QT_MOC_LITERAL(22, 421, 21), // "contextNewLabelInside"
QT_MOC_LITERAL(23, 443, 18), // "contextNewPrologue"
QT_MOC_LITERAL(24, 462, 18), // "contextNewEpilogue"
QT_MOC_LITERAL(25, 481, 16), // "selectionChanged"
QT_MOC_LITERAL(26, 498, 26), // "customContextMenuRequested"
QT_MOC_LITERAL(27, 525, 3), // "pnt"
QT_MOC_LITERAL(28, 529, 9), // "testPopup"
QT_MOC_LITERAL(29, 539, 23), // "testModeMenuItemClicked"
QT_MOC_LITERAL(30, 563, 20), // "KviKvsPopupMenuItem*"
QT_MOC_LITERAL(31, 584, 2) // "it"

    },
    "SinglePopupEditor\0contextCut\0\0contextCopy\0"
    "contextRemove\0contextPasteBelow\0"
    "contextPasteAbove\0contextPasteInside\0"
    "contextNewSeparatorBelow\0"
    "contextNewSeparatorAbove\0"
    "contextNewSeparatorInside\0contextNewItemBelow\0"
    "contextNewItemAbove\0contextNewItemInside\0"
    "contextNewMenuBelow\0contextNewMenuAbove\0"
    "contextNewMenuInside\0contextNewExtMenuBelow\0"
    "contextNewExtMenuAbove\0contextNewExtMenuInside\0"
    "contextNewLabelBelow\0contextNewLabelAbove\0"
    "contextNewLabelInside\0contextNewPrologue\0"
    "contextNewEpilogue\0selectionChanged\0"
    "customContextMenuRequested\0pnt\0testPopup\0"
    "testModeMenuItemClicked\0KviKvsPopupMenuItem*\0"
    "it"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SinglePopupEditor[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      27,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  149,    2, 0x09 /* Protected */,
       3,    0,  150,    2, 0x09 /* Protected */,
       4,    0,  151,    2, 0x09 /* Protected */,
       5,    0,  152,    2, 0x09 /* Protected */,
       6,    0,  153,    2, 0x09 /* Protected */,
       7,    0,  154,    2, 0x09 /* Protected */,
       8,    0,  155,    2, 0x09 /* Protected */,
       9,    0,  156,    2, 0x09 /* Protected */,
      10,    0,  157,    2, 0x09 /* Protected */,
      11,    0,  158,    2, 0x09 /* Protected */,
      12,    0,  159,    2, 0x09 /* Protected */,
      13,    0,  160,    2, 0x09 /* Protected */,
      14,    0,  161,    2, 0x09 /* Protected */,
      15,    0,  162,    2, 0x09 /* Protected */,
      16,    0,  163,    2, 0x09 /* Protected */,
      17,    0,  164,    2, 0x09 /* Protected */,
      18,    0,  165,    2, 0x09 /* Protected */,
      19,    0,  166,    2, 0x09 /* Protected */,
      20,    0,  167,    2, 0x09 /* Protected */,
      21,    0,  168,    2, 0x09 /* Protected */,
      22,    0,  169,    2, 0x09 /* Protected */,
      23,    0,  170,    2, 0x09 /* Protected */,
      24,    0,  171,    2, 0x09 /* Protected */,
      25,    0,  172,    2, 0x09 /* Protected */,
      26,    1,  173,    2, 0x09 /* Protected */,
      28,    0,  176,    2, 0x09 /* Protected */,
      29,    1,  177,    2, 0x09 /* Protected */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QPoint,   27,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 30,   31,

       0        // eod
};

void SinglePopupEditor::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<SinglePopupEditor *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->contextCut(); break;
        case 1: _t->contextCopy(); break;
        case 2: _t->contextRemove(); break;
        case 3: _t->contextPasteBelow(); break;
        case 4: _t->contextPasteAbove(); break;
        case 5: _t->contextPasteInside(); break;
        case 6: _t->contextNewSeparatorBelow(); break;
        case 7: _t->contextNewSeparatorAbove(); break;
        case 8: _t->contextNewSeparatorInside(); break;
        case 9: _t->contextNewItemBelow(); break;
        case 10: _t->contextNewItemAbove(); break;
        case 11: _t->contextNewItemInside(); break;
        case 12: _t->contextNewMenuBelow(); break;
        case 13: _t->contextNewMenuAbove(); break;
        case 14: _t->contextNewMenuInside(); break;
        case 15: _t->contextNewExtMenuBelow(); break;
        case 16: _t->contextNewExtMenuAbove(); break;
        case 17: _t->contextNewExtMenuInside(); break;
        case 18: _t->contextNewLabelBelow(); break;
        case 19: _t->contextNewLabelAbove(); break;
        case 20: _t->contextNewLabelInside(); break;
        case 21: _t->contextNewPrologue(); break;
        case 22: _t->contextNewEpilogue(); break;
        case 23: _t->selectionChanged(); break;
        case 24: _t->customContextMenuRequested((*reinterpret_cast< const QPoint(*)>(_a[1]))); break;
        case 25: _t->testPopup(); break;
        case 26: _t->testModeMenuItemClicked((*reinterpret_cast< KviKvsPopupMenuItem*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject SinglePopupEditor::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_SinglePopupEditor.data,
    qt_meta_data_SinglePopupEditor,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *SinglePopupEditor::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SinglePopupEditor::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SinglePopupEditor.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int SinglePopupEditor::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 27)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 27;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 27)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 27;
    }
    return _id;
}
struct qt_meta_stringdata_PopupEditorWidget_t {
    QByteArrayData data[15];
    char stringdata0[186];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PopupEditorWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PopupEditorWidget_t qt_meta_stringdata_PopupEditorWidget = {
    {
QT_MOC_LITERAL(0, 0, 17), // "PopupEditorWidget"
QT_MOC_LITERAL(1, 18, 18), // "currentItemChanged"
QT_MOC_LITERAL(2, 37, 0), // ""
QT_MOC_LITERAL(3, 38, 16), // "QTreeWidgetItem*"
QT_MOC_LITERAL(4, 55, 2), // "it"
QT_MOC_LITERAL(5, 58, 4), // "prev"
QT_MOC_LITERAL(6, 63, 26), // "customContextMenuRequested"
QT_MOC_LITERAL(7, 90, 3), // "pnt"
QT_MOC_LITERAL(8, 94, 8), // "newPopup"
QT_MOC_LITERAL(9, 103, 9), // "exportAll"
QT_MOC_LITERAL(10, 113, 14), // "exportSelected"
QT_MOC_LITERAL(11, 128, 18), // "exportCurrentPopup"
QT_MOC_LITERAL(12, 147, 18), // "removeCurrentPopup"
QT_MOC_LITERAL(13, 166, 12), // "popupRefresh"
QT_MOC_LITERAL(14, 179, 6) // "szName"

    },
    "PopupEditorWidget\0currentItemChanged\0"
    "\0QTreeWidgetItem*\0it\0prev\0"
    "customContextMenuRequested\0pnt\0newPopup\0"
    "exportAll\0exportSelected\0exportCurrentPopup\0"
    "removeCurrentPopup\0popupRefresh\0szName"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PopupEditorWidget[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    2,   54,    2, 0x09 /* Protected */,
       6,    1,   59,    2, 0x09 /* Protected */,
       8,    0,   62,    2, 0x09 /* Protected */,
       9,    0,   63,    2, 0x09 /* Protected */,
      10,    0,   64,    2, 0x09 /* Protected */,
      11,    0,   65,    2, 0x09 /* Protected */,
      12,    0,   66,    2, 0x09 /* Protected */,
      13,    1,   67,    2, 0x09 /* Protected */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3, 0x80000000 | 3,    4,    5,
    QMetaType::Void, QMetaType::QPoint,    7,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   14,

       0        // eod
};

void PopupEditorWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<PopupEditorWidget *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->currentItemChanged((*reinterpret_cast< QTreeWidgetItem*(*)>(_a[1])),(*reinterpret_cast< QTreeWidgetItem*(*)>(_a[2]))); break;
        case 1: _t->customContextMenuRequested((*reinterpret_cast< const QPoint(*)>(_a[1]))); break;
        case 2: _t->newPopup(); break;
        case 3: _t->exportAll(); break;
        case 4: _t->exportSelected(); break;
        case 5: _t->exportCurrentPopup(); break;
        case 6: _t->removeCurrentPopup(); break;
        case 7: _t->popupRefresh((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject PopupEditorWidget::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_PopupEditorWidget.data,
    qt_meta_data_PopupEditorWidget,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *PopupEditorWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PopupEditorWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PopupEditorWidget.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int PopupEditorWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
    return _id;
}
struct qt_meta_stringdata_PopupEditorWindow_t {
    QByteArrayData data[5];
    char stringdata0[56];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PopupEditorWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PopupEditorWindow_t qt_meta_stringdata_PopupEditorWindow = {
    {
QT_MOC_LITERAL(0, 0, 17), // "PopupEditorWindow"
QT_MOC_LITERAL(1, 18, 13), // "cancelClicked"
QT_MOC_LITERAL(2, 32, 0), // ""
QT_MOC_LITERAL(3, 33, 9), // "okClicked"
QT_MOC_LITERAL(4, 43, 12) // "applyClicked"

    },
    "PopupEditorWindow\0cancelClicked\0\0"
    "okClicked\0applyClicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PopupEditorWindow[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   29,    2, 0x08 /* Private */,
       3,    0,   30,    2, 0x08 /* Private */,
       4,    0,   31,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void PopupEditorWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<PopupEditorWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->cancelClicked(); break;
        case 1: _t->okClicked(); break;
        case 2: _t->applyClicked(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject PopupEditorWindow::staticMetaObject = { {
    &KviWindow::staticMetaObject,
    qt_meta_stringdata_PopupEditorWindow.data,
    qt_meta_data_PopupEditorWindow,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *PopupEditorWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PopupEditorWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PopupEditorWindow.stringdata0))
        return static_cast<void*>(this);
    return KviWindow::qt_metacast(_clname);
}

int PopupEditorWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KviWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
