/****************************************************************************
** Meta object code from reading C++ file 'ChannelsJoinDialog.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../ChannelsJoinDialog.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ChannelsJoinDialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_ChannelsJoinDialogTreeWidget_t {
    QByteArrayData data[1];
    char stringdata0[29];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ChannelsJoinDialogTreeWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ChannelsJoinDialogTreeWidget_t qt_meta_stringdata_ChannelsJoinDialogTreeWidget = {
    {
QT_MOC_LITERAL(0, 0, 28) // "ChannelsJoinDialogTreeWidget"

    },
    "ChannelsJoinDialogTreeWidget"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ChannelsJoinDialogTreeWidget[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void ChannelsJoinDialogTreeWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject ChannelsJoinDialogTreeWidget::staticMetaObject = { {
    &QTreeWidget::staticMetaObject,
    qt_meta_stringdata_ChannelsJoinDialogTreeWidget.data,
    qt_meta_data_ChannelsJoinDialogTreeWidget,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *ChannelsJoinDialogTreeWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ChannelsJoinDialogTreeWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_ChannelsJoinDialogTreeWidget.stringdata0))
        return static_cast<void*>(this);
    return QTreeWidget::qt_metacast(_clname);
}

int ChannelsJoinDialogTreeWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QTreeWidget::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_ChannelsJoinDialog_t {
    QByteArrayData data[9];
    char stringdata0[118];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ChannelsJoinDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ChannelsJoinDialog_t qt_meta_stringdata_ChannelsJoinDialog = {
    {
QT_MOC_LITERAL(0, 0, 18), // "ChannelsJoinDialog"
QT_MOC_LITERAL(1, 19, 15), // "editTextChanged"
QT_MOC_LITERAL(2, 35, 0), // ""
QT_MOC_LITERAL(3, 36, 17), // "editReturnPressed"
QT_MOC_LITERAL(4, 54, 13), // "cancelClicked"
QT_MOC_LITERAL(5, 68, 11), // "joinClicked"
QT_MOC_LITERAL(6, 80, 13), // "deleteClicked"
QT_MOC_LITERAL(7, 94, 10), // "regClicked"
QT_MOC_LITERAL(8, 105, 12) // "clearClicked"

    },
    "ChannelsJoinDialog\0editTextChanged\0\0"
    "editReturnPressed\0cancelClicked\0"
    "joinClicked\0deleteClicked\0regClicked\0"
    "clearClicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ChannelsJoinDialog[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   49,    2, 0x09 /* Protected */,
       3,    0,   52,    2, 0x09 /* Protected */,
       4,    0,   53,    2, 0x09 /* Protected */,
       5,    0,   54,    2, 0x09 /* Protected */,
       6,    0,   55,    2, 0x09 /* Protected */,
       7,    0,   56,    2, 0x09 /* Protected */,
       8,    0,   57,    2, 0x09 /* Protected */,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void ChannelsJoinDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<ChannelsJoinDialog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->editTextChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->editReturnPressed(); break;
        case 2: _t->cancelClicked(); break;
        case 3: _t->joinClicked(); break;
        case 4: _t->deleteClicked(); break;
        case 5: _t->regClicked(); break;
        case 6: _t->clearClicked(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject ChannelsJoinDialog::staticMetaObject = { {
    &QDialog::staticMetaObject,
    qt_meta_stringdata_ChannelsJoinDialog.data,
    qt_meta_data_ChannelsJoinDialog,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *ChannelsJoinDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ChannelsJoinDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_ChannelsJoinDialog.stringdata0))
        return static_cast<void*>(this);
    return QDialog::qt_metacast(_clname);
}

int ChannelsJoinDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
